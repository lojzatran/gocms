/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class BootStrap {

     def init = { servletContext ->
  		if(IWConfig.count()==0){
  		    log.info("Creating default configuration...");
	 	    def conf = new IWConfig();
 	    	conf.mailHost = "mail.mydomain.com";
 	    	conf.mailFrom = "no-reply@mydomain.com";
 	    	conf.webmasterEmail = "webmaster@mydomain.com";
 	    	conf.contactEmail = "contact@mydomain.com";
 	    	conf.mailAuth = false;
 	    	conf.mailUsr = "";
 	    	conf.mailPwd = "";
 	    	//mime types supported in MediaController
 	    	conf.mediaAllowedMimeTypes= "image/gif,image/jpeg";
 	    	conf.siteDescription = "A great site powered by goCMS";
	    	conf.siteAuthor = "ImaginaWorks Software Factory, S.L.U";
	    	conf.siteOwner = "goCMS";
	    	conf.siteKeywords = "gocms groovy grails content management system";
	    	conf.siteTitle = "goCMS go!";
	    	conf.userCookieName = com.imaginaworks.util.TXTUtils.getRandomString(10);
	    	if(conf.save()){
	    	    log.info("Default configuration was successfully created. \n\t\tgoCMS go!");
	    	}
	    	else{
	    	    log.error("Unable to create default configuration!!");
	    	    conf.errors.each{
	    	        log.error(it)
	    	    }
	    	}
 	    	
 		}
     }
     def destroy = {
     }
} 