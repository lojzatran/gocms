/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

// log4j configuration
//log4j {
//    appender.stdout = "org.apache.log4j.ConsoleAppender"
//    appender.'stdout.layout' = "org.apache.log4j.PatternLayout"
//    appender.'stdout.layout.ConversionPattern' = '[%r] %c{2} %m%n'
//    rootLogger = "error,stdout"
//    logger {
//        grails = "info,stdout"
//        org {
//            codehaus.groovy.grails.web.servlet = "info,stdout"  //  controllers
//            codehaus.groovy.grails.web.pages = "info,stdout" //  GSP
//            codehaus.groovy.grails.web.sitemesh = "info,stdout" //  layouts
//            codehaus.groovy.grails."web.mapping.filter" = "info,stdout" // URL mapping
//            codehaus.groovy.grails."web.mapping" = "info,stdout" // URL mapping
//            codehaus.groovy.grails.commons = "info,stdout" // core / classloading
//            codehaus.groovy.grails.plugins = "info,stdout" // plugins
//            codehaus.groovy.grails.orm.hibernate = "info,stdout" // hibernate integration
//            springframework = "off,stdout"
//            hibernate = "off,stdout"
//        }
//    }
//    additivity.'default' = false
//    additivity {
//        grails = false
//        org {
//            codehaus.groovy.grails = false
//            springframework = false
//            hibernate = false
//        }
//    }
//}

log4j = {
    appenders {
        environments {
            console name: "stdout",
                    layout: pattern(conversionPattern: '[%r] %c{2} %m%n')
        }
    }

    root {
        error 'stdout', 'stacktraceFile' // logování v aplikaci pomocí log.info
    }

    environments {
        production {
            info stacktraceFile: "grails.app"
        }
        development {
            info stacktraceFile: "grails.app"
        }
        test {
            info "stdout", "grails.app"
        }
    }

    error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
            'org.codehaus.groovy.grails.web.pages', //  GSP
            'org.codehaus.groovy.grails.web.sitemesh', //  layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping', // URL mapping
            'org.codehaus.groovy.grails.commons', // core / classloading
            'org.codehaus.groovy.grails.plugins', // plugins
            'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate'
}

grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
