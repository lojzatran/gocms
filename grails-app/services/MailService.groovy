/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import org.apache.commons.logging.LogFactory
import com.imaginaworks.srvc.email.Email


class MailService {

	boolean transactional = false
	
	def sendTXT(String smtp, String from, String toaddress, String content, String msgSubject) {
	    log.info("Enviando correo desde ${from} a ${toaddress}. Asunto: ${msgSubject}, Cuerpo:\n${content}")
	    
	    Email.sendEmail(
	            smtp,
	            null,
	            null,
	            from,
	            toaddress,
	            msgSubject,
	            content
	    );
	    
	    
	}

	def sendHTML(String smtp, String from, String toaddress, String content, String msgSubject) {
	    Email.sendHTMLEmail(
	            smtp,
	            null,
	            null,
	            from,
	            toaddress,
	            msgSubject,
	            wrapWithHtml(content)
	    );
	}

	def wrapWithHtml(msg) {
		def wrapped = """
		<html>
    	<head>
    		<style>
    		body {
    			color: #333;
    			font-family: Tahoma, Geneva, Arial, Helvetica, sans-serif;
    			font-size: 0.8em;
    			line-height: 1.8em;
    			text-align: left;
    			background-off: #E6E6E6;
    			}

    			a {
    			color: #005880;
    			}

    			a:hover {
    			color: #006B95;
    			}

    			a:visited {
    			color: #006B95;
    			}

    			a:visited:hover {
    			color: #2C91B2;
    			}

    			h1{
    			font-family: "Arial Narrow",Tahoma, Geneva, Arial, Helvetica, sans-serif;
    			font-size: 1.6em;
    			color: #006B95;
    			margin: 15px 0 50px 0;
    			padding-left: 15px;
    			}

    			p {
    			margin: 10px 15px 5px 15px;
    			}

    		</style>
    	</head>
    	<body>
    	${msg}
		</body>
		</html>
		"""
		return wrapped
	}
}

