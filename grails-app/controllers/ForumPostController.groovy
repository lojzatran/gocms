/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
            
class ForumPostController extends SecureController{
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ forumPostList: ForumPost.list( params ) ]
    }

    def show = {
        [ forumPost : ForumPost.get( params.id ) ]
    }

    def delete = {
        def forumPost = ForumPost.get( params.id )
        if(forumPost) {
            forumPost.delete()
            flash.message = "ForumPost ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "ForumPost not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def forumPost = ForumPost.get( params.id )

        if(!forumPost) {
                flash.message = "ForumPost not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ forumPost : forumPost ]
        }
    }

    def update = {
        def forumPost = ForumPost.get( params.id )
        if(forumPost) {
             forumPost.properties = params
            if(forumPost.save()) {
                redirect(action:show,id:forumPost.id)
            }
            else {
                render(view:'edit',model:[forumPost:forumPost])
            }
        }
        else {
            flash.message = "ForumPost not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def forumPost = new ForumPost()
        forumPost.properties = params
        return ['forumPost':forumPost]
    }

    def save = {
        def forumPost = new ForumPost()
        forumPost.properties = params
        if(forumPost.save()) {
            redirect(action:show,id:forumPost.id)
        }
        else {
            render(view:'create',model:[forumPost:forumPost])
        }
    }
    
	def pendingPosts = {
	        def posts = ForumPost.findAllByApproved(false,[sort:'id',order:'desc'])
	        [posts:posts]
	}
	
	def approvePost = {        
	        if(params.id){
	            def post = ForumPost.get(params.id)
	            post.approved = true;
	            if(post.save()){
	               flash.message = "Post aprobado." 
	            }
	            else{
	                flash.message = "No se pudo aprobar el post... Ha ocurrido un error"
	            }
	        }
	        else{
	            flash.message = "No se seleccion&oacute; ning&uacute;n post..." 
	        }
	        redirect(action:'pendingPosts')
	}
	
	def deletePost = {
	        if(params.id){
	            def post = ForumPost.get(params.id)    
	            if(post.author){
	                post.author.removeFromPosts(post);
	            }
	            if(post.parent){
	                post.parent.removeFromReplies(post);
	            }
	            if (post.forum){
	                post.forum.removeFromThreads(post)
	            }
	            if(post.delete()){
	                flash.message = "Post eliminado."
	            }
	            else {
	                flash.message = "No se pudo eliminar..."
	            }
	            
	             
	        }
	        else{
	            flash.message = "No se seleccion&oacute; ning&uacute;n post..." 
	        }
	        redirect(action:'pendingPosts')	        
	}
}