/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import com.sun.syndication.feed.synd.*
import com.sun.syndication.io.SyndFeedOutput

import com.imaginaworks.util.DateUtils


class FeedController {

	def index = {}
	
	def supportedFormats = [ "rss_0.90", "rss_0.91", "rss_0.92", "rss_0.93", "rss_0.94", "rss_1.0", "rss_2.0", "atom_0.3"]

	
    def rssStories = {
    		def sto = Story.list(max: 20, sort: "id", order: "desc")
     		def issues = []    		
    		sto.each {
    		    issues << new Expando(
    		            id:it.id,
    		            intro:it.intro,
    		            titulo:it.titulo,
    		            enlace:"${request.scheme}://${request.serverName}:${request.serverPort}${request.contextPath}/home/story/${it.id}"
    		            )
    		}   		
    		render(text: getFeed(issues,"rss_2.0"), contentType:"text/xml", encoding:"UTF-8")
   	
    }
	
	def rssEvents = {
	        def hoy = DateUtils.getCleanDate();
	        
    		def evt = Event.findByDateBetween(hoy, hoy+1, [sort: 'date'])
    		def issues = []    		
    		evt.each {
    		    issues << new Expando(
    		            id:it.id,
    		            intro:it.description,
    		            titulo:it.title,
    		            enlace:"${request.scheme}://${request.serverName}:${request.serverPort}${request.contextPath}/events/show/${it.id}"
    		            )
    		}
    		render(text: getFeed(issues,"rss_2.0"), contentType:"text/xml", encoding:"UTF-8")	        
	}
    
    def atom = {
    		
    		render(text: getFeed("atom_1.0"), contentType:"text/xml", encoding:"UTF-8")
    	   			
    }

	// or specify your own feed type
    def all = {
			def format = params.id
			if (supportedFormats.contains(format)) {
	    		render(text: getFeed(format), contentType:"text/xml", encoding:"UTF-8")
			} else {
				response.sendError(response.SC_FORBIDDEN);
			}				
	}
	
	
    def getFeed(issues,feedType) {    	
          	
        def entries = []
        issues.each { issue ->
        	def desc = new SyndContentImpl(type: "text/plain", value: issue.intro);
	        def entry = new SyndEntryImpl(
	                title: issue.titulo, 
	        		link: issue.enlace,
	        		publishedDate: issue.fechaAlta, description: desc);
	        entries.add(entry);

        }
        SyndFeed feed = new SyndFeedImpl(
                feedType: feedType, 
                title: IWConfig.list()[0].siteTitle,
        		link: "${request.scheme}://${request.serverName}:${request.serverPort}${request.contextPath}", 
        		description: IWConfig.list()[0].siteDescription,
        		entries: entries);
        
        StringWriter writer = new StringWriter();
        SyndFeedOutput output = new SyndFeedOutput();
        output.output(feed,writer);
        writer.close();
        
        return writer.toString();

    	
    }	
}

