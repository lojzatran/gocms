/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import java.text.SimpleDateFormat;

class MediaController extends SecureController {
    static final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
    static final String FILES_PATH = "/files";
	def index = {redirect(action:'upload'); }
	
	def upload = {
	        
	}
	
	def showContents = {
	        def folders = [];
	        def files = [];
	        File folder = new File(servletContext.getRealPath("${FILES_PATH}"));
	        //carpetas:
	        folder.eachDir{
	            folders<<"${FILES_PATH}/${it.name}";
	        }
	        //archivos:
	        if(params.folder){
	            new File(servletContext.getRealPath(params.folder)).eachFile{
	                files<<"${FILES_PATH}/${params.folder}/${it.name}";
	            }
	        }
	        render(template:'folderContent',model:['files':files,'folders':folders]);
	}
	
	def save = {
	        def i = 1;
	        def file = null;
	        def config = IWConfig.list()[0];
	        String message = "";
	        String path = null;
	        while((file=request.getFile("file_${i++}")) && !file.isEmpty()){
	            if(config.mediaAllowedMimeTypes.contains(file.getContentType())){
	                path = saveFile(file);	 
	                message = "${message}<br />${file.originalFilename} procesado: ${path}";
	            }
	            else{
	                message = "${message}<br />El fichero ${file.originalFilename} no es de un tipo permitido. Solo se admiten los tipos ${config.mediaAllowedMimeTypes}";
	            }	            
	        }
	        flash.message = message;
	        redirect(action:upload);
	}
	
	private String saveFile(file){
	    def hoy = sdf.format(new Date());
	    File folder = new File(servletContext.getRealPath("${FILES_PATH}/${hoy}"));
	    folder.mkdirs();
	    
	    def ext = file.originalFilename;
	    ext = ext.substring(ext.lastIndexOf('.'));
	    
	    def newName = System.currentTimeMillis() + ext;
	    
	    file.transferTo(new File(folder,newName));	
	    
	    def applicationUri = grailsAttributes.getApplicationUri(request);
	    return "${applicationUri}${FILES_PATH}/${hoy}/${newName}";
	}
}

