/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import com.imaginaworks.util.TXTUtils

class DocumentCategoryController  extends SecureController{
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ documentCategoryList: DocumentCategory.list( params ) ]
    }

    def show = {
        [ documentCategory : DocumentCategory.get( params.id ) ]
    }

    def delete = {
        def documentCategory = DocumentCategory.get( params.id )
        if(documentCategory) {
            documentCategory.delete()
            flash.message = "DocumentCategory ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "DocumentCategory not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def documentCategory = DocumentCategory.get( params.id )

        if(!documentCategory) {
                flash.message = "DocumentCategory not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ documentCategory : documentCategory ]
        }
    }

    def update = {
        def documentCategory = DocumentCategory.get( params.id )
        if(documentCategory) {
             documentCategory.properties = params
             documentCategory.normalized = TXTUtils.normalizeString(documentCategory.title) 
            if(documentCategory.save()) {
                redirect(action:show,id:documentCategory.id)
            }
            else {
                render(view:'edit',model:[documentCategory:documentCategory])
            }
        }
        else {
            flash.message = "DocumentCategory not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def documentCategory = new DocumentCategory()
        documentCategory.properties = params
        return ['documentCategory':documentCategory]
    }

    def save = {
        def documentCategory = new DocumentCategory()
        documentCategory.properties = params
        documentCategory.normalized = TXTUtils.normalizeString(documentCategory.title) 
        if(documentCategory.save()) {
            redirect(action:show,id:documentCategory.id)
        }
        else {
            render(view:'create',model:[documentCategory:documentCategory])
        }
    }

}