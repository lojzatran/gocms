/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
            
class BlockController  extends SecureController {
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ blockList: Block.list( params ) ]
    }

    def show = {
        [ block : Block.get( params.id ) ]
    }

    def delete = {
        def block = Block.get( params.id )
        if(block) {
            block.delete()
            flash.message = "Block ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "Block not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def block = Block.get( params.id )

        if(!block) {
                flash.message = "Block not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ block : block ]
        }
    }

    def update = {
        def block = Block.get( params.id )
        if(block) {
             block.properties = params
            if(block.save()) {
                redirect(action:show,id:block.id)
            }
            else {
                render(view:'edit',model:[block:block])
            }
        }
        else {
            flash.message = "Block not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def block = new Block()
        block.properties = params
        return ['block':block]
    }

    def save = {
        def block = new Block()
        block.properties = params
        if(block.save()) {
            redirect(action:show,id:block.id)
        }
        else {
            render(view:'create',model:[block:block])
        }
    }

}