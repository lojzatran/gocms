/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */


class ForumController extends SecureController{
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ forumList: Forum.list( params ) ]
    }

    def show = {
        [ forum : Forum.get( params.id ) ]
    }

    def delete = {
        def forum = Forum.get( params.id )
        if(forum) {
            forum.delete()
            flash.message = "Forum ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "Forum not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def forum = Forum.get( params.id )

        if(!forum) {
                flash.message = "Forum not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ forum : forum ]
        }
    }

    def update = {
        def forum = Forum.get( params.id )
        if(forum) {
             forum.properties = params
            if(forum.save()) {
                redirect(action:show,id:forum.id)
            }
            else {
                render(view:'edit',model:[forum:forum])
            }
        }
        else {
            flash.message = "Forum not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def forum = new Forum()
        forum.properties = params
        return ['forum':forum]
    }

    def save = {
        def forum = new Forum()
        forum.properties = params
        if(forum.save()) {
            redirect(action:show,id:forum.id)
        }
        else {
            render(view:'create',model:[forum:forum])
        }
    }

}