/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import com.imaginaworks.util.TXTUtils
import com.imaginaworks.util.IOUtils

class DocumentController  extends SecureController{
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ documentList: Document.list( params ) ]
    }

    def show = {
        [ document : Document.get( params.id ) ]
    }

    def delete = {
        def document = Document.get( params.id )
        if(document) {
            document.delete()
            flash.message = "Document ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "Document not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def document = Document.get( params.id )

        if(!document) {
                flash.message = "Document not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ document : document ]
        }
    }

    def update = {
        def document = Document.get( params.id )
        if(document) {
             document.properties = params
             document.normalized = TXTUtils.normalizeString(document.title)
            if(document.save()) {
                def file = request.getFile("archivo")
                if(file && !file.isEmpty()){
                    def name = file.originalFilename.toLowerCase()
                    File carpeta = new File(servletContext.getRealPath("/files/document/${document.category.normalized}"))
                    if(!carpeta.exists()) carpeta.mkdirs()
                    File destino = new File(carpeta,"${document.normalized}-${document.id}${name.substring(name.lastIndexOf('.'))}")
                    if(destino.exists()) destino.delete()
                    file.transferTo(destino)
                    document.fileName = destino.name
                	document.mimeType = IOUtils.getMimeType(destino)
                	document.bytes = destino.length()
                	document.save()                    
                }
                redirect(action:show,id:document.id)
            }
            else {
                render(view:'edit',model:[document:document])
            }
        }
        else {
            flash.message = "Document not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def document = new Document()
        document.properties = params        
        return ['document':document]
    }

    def save = {
        def document = new Document()
        document.properties = params
        document.normalized = TXTUtils.normalizeString(document.title)
        def file = request.getFile("archivo")
        if(!file || file.isEmpty()){            
         	flash.message="No se recibi&oacute; ningun archivo!"
         	render(view:'create',model:[document:document])
        }
        else{            
            if(document.save()) {
                def name = file.originalFilename.toLowerCase()
                File carpeta = new File(servletContext.getRealPath("/files/document/${document.category.normalized}"))
                if(!carpeta.exists()) carpeta.mkdirs()
                File destino = new File(carpeta,"${document.normalized}-${document.id}${name.substring(name.lastIndexOf('.'))}")
                file.transferTo(destino)
                document.mimeType = IOUtils.getMimeType(destino)
                document.fileName = destino.name
                document.bytes = destino.length()
                document.save()
                redirect(action:show,id:document.id)
            }
            else {
                render(view:'create',model:[document:document])
            }
        }
        
    }

}