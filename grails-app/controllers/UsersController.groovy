/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class UsersController {
    MailService mailService;

    def index = {}

    def profile = {
        if (!session.user) redirect(controller: 'home', action: 'index')
        [user: session.user]
    }

    def saveProfile = {
        def user = User.get(session.user.id)
        user.properties = params
        if (user.save()) {
            session.user = user
            flash.message = "Tu perfil se guard&oacute; correctamente"
            redirect(action: 'profile')
        }
        else {
            render(view: 'profile', model: [user: user])
        }

    }

    def contactForm = {

    }

    def sendContactRequest = {

        def smtp = IWConfig.list()[0].mailHost
        def from = IWConfig.list()[0].mailFrom
        def contact = IWConfig.list()[0].contactEmail
        def nombre = params.nombre
        def telef = params.telefono
        def email = params.email

        def mensaje = """
Nombre: ${nombre}
Telefono: ${telef}
Email: ${email}
Mensaje:
${params.mensaje}            
            """

        mailService.sendTXT(smtp, from, contact, mensaje, "Nuevo mensaje desde ${request.serverName} de ${params.nombre}")
    }

    def passwordReminder = {
        if (params.email) {
            def user = User.findByEmail(params.email)
            if (user) {
                def msg = """
Estos son tus datos para acceder a ${request.serverName}:

Nombre de usuario: ${user.nombre}
Clave de acceso: ${user.clave}

Saludos,
${request.serverName}
                """
                def smtp = IWConfig.list()[0].mailHost
                def from = IWConfig.list()[0].mailFrom
                mailService.sendTXT(smtp, from, user.email, msg, "Recordatorio de clave de acceso a ${request.serverName}")
                flash.message = "Hemos enviado tu contrase&ntilde;a a la direcci&oacute;n de correo que figura en tu perfil."
            }
            else {
                flash.message = "No existe ning&uacute;n usuario con esa direcci&oacute;n de correo."
            }
        }
        else {
            flash.message = "No se recibi&oacute; ning&uacute;n email."
        }
        redirect(action: 'passwordReminderForm')
    }

    def passwordReminderForm = {

    }
}

