/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
            
class BlockGroupController  extends SecureController {
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ blockGroupList: BlockGroup.list( params ) ]
    }

    def show = {
        [ blockGroup : BlockGroup.get( params.id ) ]
    }

    def delete = {
        def blockGroup = BlockGroup.get( params.id )
        if(blockGroup) {
            blockGroup.delete()
            flash.message = "BlockGroup ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "BlockGroup not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def blockGroup = BlockGroup.get( params.id )

        if(!blockGroup) {
                flash.message = "BlockGroup not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ blockGroup : blockGroup ]
        }
    }

    def update = {
        def blockGroup = BlockGroup.get( params.id )
        if(blockGroup) {
             blockGroup.properties = params
            if(blockGroup.save()) {
                redirect(action:show,id:blockGroup.id)
            }
            else {
                render(view:'edit',model:[blockGroup:blockGroup])
            }
        }
        else {
            flash.message = "BlockGroup not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def blockGroup = new BlockGroup()
        blockGroup.properties = params
        return ['blockGroup':blockGroup]
    }

    def save = {
        def blockGroup = new BlockGroup()
        blockGroup.properties = params
        if(blockGroup.save()) {
            redirect(action:show,id:blockGroup.id)
        }
        else {
            render(view:'create',model:[blockGroup:blockGroup])
        }
    }

}