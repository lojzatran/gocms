/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class ForumsController {
    MailService mailService;

	def index = {	  	        
	        [ forumList: Forum.list()]	        
	}
	
	def recentPosts = {
	        [threads: ForumPost.listOrderById(max:10, order:'desc')]
	}
	
	def show = {
	        if(!params.id){
	            redirect(action:'index');
	        }
	        def forum = Forum.get(params.id)
	        session.forum = forum
	        def lista;
	        if(forum.moderated){	            
	            lista = ForumPost.findAllByForumAndApproved(forum,true,[sort:'id',order:'desc'])
	        }
	        else{
	            lista = ForumPost.findAllByForum(forum,[sort:'id',order:'desc'])
	        }	      
	        [threads:lista]
	}
	
	def thread = {
	        if(!params.id){
	            redirect(action:'index');
	        }
	        def thread = ForumPost.get(params.id);
	        thread.views++;
	        thread = thread.save();
	        def lista;
	        if(thread.forum.moderated){
	            lista = thread.replies.findAll{it.approved}.sort{it.id};
	        }
	        else{
	            lista = thread.replies.sort{it.id};
	        }
	        [thread:thread,replies:lista]
	        
	}
	
	def reply = {
	        if(!params.id){
	            redirect(action:'index');
	        }
	        def post = ForumPost.get(params.id);
	}
	
	def savePost = {
	        def smtp = IWConfig.list()[0].mailHost
	        def from = IWConfig.list()[0].mailFrom
	        def webmaster = IWConfig.list()[0].webmasterEmail

	        def title = params.title;
	        def text = params.text;
	        def user = session.user;
	        def forum = session.forum;
	        def post = new ForumPost(
	                forum:forum,
	                author:user,
	                title:title,
	                text:text,
	                created:new Date()
	                ).save();
	        
	        mailService.sendHTML(smtp,from,webmaster,text,"Nuevo Post de ${session.user.nombre} en el foro \"${forum.name}\": ${title}")
	        mailService.sendHTML(smtp,from,forum.owner.email,text,"Nuevo Post de ${session.user.nombre} en el foro \"${forum.name}\": ${title}")
	        flash.message = "Tu mensaje ser&aacute; publicado cuando lo autorice el moderador."
	        render(template:'/okAndReload');	        
	}
	
	def saveReply = {
	        def smtp = IWConfig.list()[0].mailHost
	        def from = IWConfig.list()[0].mailFrom
	        def webmaster = IWConfig.list()[0].webmasterEmail
	        
	        def parentID = params.parentID;
	        def parent = ForumPost.get(parentID);
	        def author = session.user;
	        def hoy = new Date();
	        def forum = parent.forum;
	        def title = params."title_${parentID}";
	        def text = params."text_${parentID}";
	        
	        def post = new ForumPost(
	                title: title,
	                text: text,
	                author: author,
	                created: hoy,
	                parent: parent
	                ).save();
	        
	        parent.replyCount++;
	        parent.save();

	        mailService.sendHTML(smtp,from,webmaster,text,"Nuevo Post de ${session.user.nombre} en el foro \"${forum.name}\": ${title}")
	        mailService.sendHTML(smtp,from,forum.owner.email,text,"Nuevo Post de ${session.user.nombre} en el foro \"${forum.name}\": ${title}")
	        
	        flash.message = "Tu mensaje ser&aacute; publicado cuando lo autorice el moderador."
	        render(template:'/okAndReload');
	}
	
	def replyForm = {
	        def post = ForumPost.get(params.parentID);
	        render(template:'replyForm',model:[parent:post]);
	}
	def newPostForm = {
	        render(template:'newPostForm');
	}

}

