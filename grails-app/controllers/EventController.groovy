/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import net.sf.classifier4J.summariser.*;
            
class EventController  extends SecureController{
    ISummariser summariser = new SimpleSummariser();
    final int SUMMARY_SENTENCES = 2;
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ eventList: Event.list( params ) ]
    }

    def show = {
        [ event : Event.get( params.id ) ]
    }

    def delete = {
        def event = Event.get( params.id )
        if(event) {
            event.delete()
            flash.message = "Event ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "Event not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def event = Event.get( params.id )

        if(!event) {
                flash.message = "Event not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ event : event ]
        }
    }

    def update = {
        def event = Event.get( params.id )
        if(event) {
             event.properties = params
            if(event.save()) {
                redirect(action:show,id:event.id)
            }
            else {
                render(view:'edit',model:[event:event])
            }
        }
        else {
            flash.message = "Event not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def event = new Event()
        event.properties = params
        return ['event':event]
    }

    def save = {
        def event = new Event()
        event.properties = params
        
        def intro = summariser.summarise(MarkupUtils.removeMarkup(event.description),SUMMARY_SENTENCES);
        if(intro.size()>250) intro = "${intro.substring(0,250)}..";
        event.intro = intro;
        
        if(event.save()) {
            redirect(action:show,id:event.id)
        }
        else {
            render(view:'create',model:[event:event])
        }
    }

}