/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class GalleriesController {

	def index = {
	        if(!params.max)params.max = 15
	        if(!params['sort'])params['sort'] = 'id';
	        if(!params.order)params.order = 'desc';
	        
	        def galleries = ImageGallery.list(params);
	        return [galleries:galleries]
	}
	
	def show = {
	        if(!params.id && !session.idGaleria){
	            flash.message = "No se ha seleccionado ninguna galeria"
	            redirect(action:index)
	        }
	        else{
	            if(params.id) session.idGaleria = params.id
		        if(!params.max)params.max = 25
		        if(!params['sort'])params['sort'] = 'id';		        
	            def galeria = ImageGallery.get(session.idGaleria)
	            def images = Image.findAllByGallery(galeria,params)
	            return [galeria:galeria,imagenes:images]
	        }	        
	}
	
	def viewImage = {
	        if(!params.id){
	            flash.message = "No se ha seleccionado ninguna foto"
	            redirect(action:index)
	        }
	        [foto:Image.get(params.id)]
	}
	
}

