/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
            
class ImageController extends SecureController  {
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ imageList: Image.list( params ) ]
    }

    def show = {
        [ image : Image.get( params.id ) ]
    }

    def delete = {
        def image = Image.get( params.id )
        if(image) {
            image.delete()
            flash.message = "Image ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "Image not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def image = Image.get( params.id )

        if(!image) {
                flash.message = "Image not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ image : image ]
        }
    }

    def update = {
        def image = Image.get( params.id )
        if(image) {
             image.properties = params
            if(image.save()) {
                redirect(action:show,id:image.id)
            }
            else {
                render(view:'edit',model:[image:image])
            }
        }
        else {
            flash.message = "Image not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def image = new Image()
        image.properties = params
        return ['image':image]
    }

    def save = {
        def image = new Image()
        image.properties = params
        if(image.save()) {
            redirect(action:show,id:image.id)
        }
        else {
            render(view:'create',model:[image:image])
        }
    }

}