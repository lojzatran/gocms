/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

abstract class SecureController {
    MailService mailService;

    /*
    CADA CONTROLLER TIENE UNOS ROLES ASOCIADOS.
    SOLO PODRN INVOCARLO LOS USUARIOS CON ALGUNO DE LOS ROLES ADMITIDOS.
    */
    static def authMap = [
                          block:'admin,editor',
                          blockGroup:'admin,editor',
                          event:'admin,editor',
                          eventCategory:'admin,editor',
                          forum:'admin',
                          forumPost:'admin',
                          media:'admin,editor',
                          section:'',
                          story:'admin,editor',
                          tag:'admin',
                          user:'admin',
                          image:'admin,editor',
                          imageGallery:'admin,editor',
                          documentCategory:'admin,editor',
                          document:'admin,editor',
                          comment:'admin,editor'
                          ];

	def beforeInterceptor = [action: this.&auth, 
	                         except:[
	                                 'loginForm',
	                                 'login',
	                                 'registerForm',
	                                 'register'
	                                 ]];
	
	def auth() {
	    if(!session.user){
	        flash.message="Tienes que iniciar sesi&#243;n para entrar ah&#237;...";
	        redirect(controller:'home',action:'index');
	        return false;
	    }
	    else{
	        def roles = session.user.roles;
	        def controller = this.controllerName;
	        def required = authMap[controller].split(',');
	        //por defecto es admin:
	        if(!required){
	            required='admin';
	        }
	        boolean ok = false;
	        for(rol in required){	            
	            if(roles.contains(rol)){
	                ok = true;
	                break;
	            }
	        }
	        if(!ok){
	            def msg = "INTENTO DE ACCESO NO PERMITIDO DE [${session.user}], CON ROLES [${roles}] A [${controller}]";
	            log.info(msg);
	            mailService.sendTXTtoWebmaster(msg,msg)
		        flash.message="No tienes privilegios para entrar ah&#237;...";
		        redirect(controller:'home',action:'index');
		        return false;
	        }
	    }
	}	                         
}

