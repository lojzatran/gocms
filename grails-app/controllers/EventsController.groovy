/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import com.imaginaworks.util.*

class EventsController {

	def index = {
	        if(!params.max)params.max = 25;
	        if(!params['sort'])params['sort'] = 'id';
	        if(!params.order)params.order = 'desc';
	        
	        [ eventList: Event.list( params ) ]	 	        
	}
	
	def show = {
	        if(!params.id){
	            redirect(controller:'home',action:'index')
	        }
	        [evento:Event.get(params.id)]
	}
	
	def today = {
	        if(!params['sort'])params['sort'] = 'date';
	        def hoy = new Date()
	        def list = Event.findAllByDate(hoy, params )
	        if(!list?.size) flash.message = "No se encontraron eventos para las fechas seleccionadas."
	        [ eventList: list,hoy:hoy ]
	}
	
	def thisWeek = {
 	        def lunes = DateUtils.getLastMonday()
	        def domingo = lunes + 6	        
	        if(!params['sort'])params['sort'] = 'date';
 	        
 	        def list = Event.findAllByDateBetween(lunes,domingo, params )
	        if(!list?.size) flash.message = "No se encontraron eventos para las fechas seleccionadas."
	        [ eventList: lsit,lunes:lunes,domingo:domingo ]	 	        
	       
	}
	def thisMonth = {
 	        def t0 = DateUtils.getFirstDayOfMonth()
	        def t1 = DateUtils.getLastDayOfMonth()	        
	        if(!params['sort'])params['sort'] = 'date';
 	        
 	        def list = Event.findAllByDateBetween(t0,t1, params ) 	        
	        if(!list?.size) flash.message = "No se encontraron eventos para las fechas seleccionadas."
	        [ eventList: list,mes:DateUtils.format(t0,'MMMM') ]	 	        	       
	}	
	
	def thisYear = {
 	        def t0 = DateUtils.firstDayOfYear
 	        def t1 = DateUtils.lastDayOfYear	       
 	        
	        if(!params['sort'])params['sort'] = 'date'; 	        
 	        def list = Event.findAllByDateBetween(t0,t1, params )
	        if(!list?.size) flash.message = "No se encontraron eventos para las fechas seleccionadas."
	        [ eventList: list,anyo:DateUtils.format(t0,'yyyy') ]		        
	}
	
	def search = {
	        def fechas = new Expando()
	        fechas.t0 = new Date()
	        fechas.t1 = fechas.t0 + 7
	     return[fechas:fechas]   
	}
	
	def findEvents = {
 	        def t0 = DateUtils.parse("${params.t0_day}/${params.t0_month}/${params.t0_year}","dd/MM/yyyy")
 	        def t1 = DateUtils.parse("${params.t1_day}/${params.t1_month}/${params.t1_year}","dd/MM/yyyy")
	        if(!params['sort'])params['sort'] = 'date';
 	        
 	        def list = Event.findAllByDateBetween(t0,t1, params )
	        if(!list?.size) flash.message = "No se encontraron eventos para las fechas seleccionadas."
	        [ eventList: list, desde:t0, hasta:t1 ]
	}
}

