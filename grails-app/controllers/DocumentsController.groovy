/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class DocumentsController {

	def index = { }
	
	def show = {
	        if(!params.id){
	            flash.message = "No se ha seleccionado ning&uacute;n documento..."
	            redirect(controller:'home',action:'index')
	        }
	        def doc = Document.get(params.id)
	        if(!doc){
	            flash.message = "El documento ${params.id} no existe..."
		        redirect(controller:'home',action:'index')	            
	        }
	        [document:doc]
	}
	
	
}

