/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class HomeController{
    MailService mailService;
    
    def beforeInterceptor = [action: this.&getUserFromCookie]
    
	def index = {
	        if(!params.max)params.max = 5;
	        if(!params['sort'])params['sort'] = 'id';
	        if(!params.order)params.order = 'desc';
	        def l = Story.list(params);
	        [storyList: l]
	}    
    
	def list = {
	         redirect(action:index,params:params)
	}
    
   def section = {
	        if(!params.id){
	            redirect(action:index);
	        }
	        if(!params.max)params.max = 10;
	        if(!params['sort'])params['sort'] = 'id';
	        if(!params.order)params.order = 'desc';	
	        def section = Section.get( params.id )
           [ 
             section : section,
             totalStories: Story.countBySeccion(section),
             stories: Story.findAllBySeccion(section,params),
             documents: Document.findAllBySection(section,[sort:'id',order:'desc'])
           ]
   }

   def story = {
	        if(!params.id){
	            redirect(action:index);
	        }

	        def story = Story.get(params.id);
            story.lecturas ++;
            story.save();
            session.story = story;
	        def comments;
	        if(IWConfig.list()[0].moderateComments){
	            comments = Comment.findAllByStoryAndApproved(session.story,true,[sort:'id',order:'asc']);
	        }
	        else{
	            comments = Comment.findAllByStory(session.story,[sort:'id',order:'asc']);
	        }
	        [comments:comments];
	}
    

	def login = {
	        def user = User.findByNombreAndClave(params.name,params.passwd);
	        if(user){
	            session.user = user;
	            if(params.recordar){
	                setCookie();
	            }
	            render "&#161;Hola ${user?.nombre?.encodeAsHTML()}! Un momento por favor...<script>window.location.reload();</script>";
	        }
	        else{
	            render(template:"/userBox",model:[msg:"Usuario o contrase&#241;a incorrectos..."]);
	        }
	}
    
	private unsetCookie(){
	    def cookieName = IWConfig.list()[0].userCookieName;
	    def cookieValue = '';
	    def cookie = new javax.servlet.http.Cookie(cookieName, cookieValue)
	    cookie.path = '/'
	    cookie.maxAge = 0
	    response.addCookie(cookie)
	}	
	private setCookie(){
	    def cookieName = IWConfig.list()[0].userCookieName;
	    def cookieValue = "${session.user.id}";
	    def cookie = new javax.servlet.http.Cookie(cookieName, cookieValue)
	    cookie.path = '/'
	    cookie.maxAge = 60*60*24*365*5  // 5 years in seconds
	    response.addCookie(cookie)
	}
	
	private getUserFromCookie(){
	    if(!session.user){
		    def cookieName = IWConfig.list()[0].userCookieName
		    def cookie
		    request.cookies.each {
	            if(it.name == cookieName){
	                cookie = it.value
	            }
	        }
		    if(cookie){
		        try{
		            session.user = User.get(cookie);
		            log.info("Usuario reconocido: ${session.user}")
		        }
		        catch(Exception x){
		            log.info("No se pudo leer el usuario a partir de la cookie ${cookie}: ${x}")
		        }
		    }
	    }
	}

	def logout = {
	        unsetCookie();
	        session.invalidate();
	        redirect(action:"index");
	}
    def register = {
	        def user = new User()
	        user.properties = params
	        return ['user':user]
	}

	def saveUser = {
	        def user = new User()
	        user.properties = params
	        user.fechaAlta = new Date();
	        user.roles= "user";
	        if(!user.web){
	            user.web = "";
	        }
	        if(params.clave != params.clave2){
	            flash.message = "Las claves no coinciden...";
	            render(view:'register',model:[user:user]);
	        }
	        else if(user.save()) {
	            session.user=user;
	            redirect(action:'index')
	        }
	        else {
	            render(view:'register',model:[user:user])
	        }
	 }

	def remember = {

	 }
    def saveComment = {
	        def smtp = IWConfig.list()[0].mailHost
	        def from = IWConfig.list()[0].mailFrom
	        def webmaster = IWConfig.list()[0].webmasterEmail
	        
	        def comment = new Comment(
	        title:params.title,
	        text:params.text,
	        author:session.user,
	        created:new Date(),
	        story:session.story,
	        ).save();
	        
	        if(comment.save()) {
	            if(IWConfig.list()[0].moderateComments){
	                flash.message = "Tu comentario est&aacute; pendiente de aprobaci&oacute;n por un moderador."
	            }
	            redirect(controller:'home',action:'story',id:session.story.id);
	            mailService.sendTXT(smtp,from,webmaster,comment.text,"Nuevo Comentario de ${session.user.nombre}")
	        }
	        else {
	            render(view:'story',id:session.story.id,model:[comment:comment]);
	        }
	    }
	
}

