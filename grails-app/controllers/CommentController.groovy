/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
            
class CommentController  extends SecureController{
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ commentList: Comment.list( params ) ]
    }

    def show = {
        [ comment : Comment.get( params.id ) ]
    }

    def delete = {
        def comment = Comment.get( params.id )
        if(comment) {
            comment.delete()
            flash.message = "Comment ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "Comment not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def comment = Comment.get( params.id )

        if(!comment) {
                flash.message = "Comment not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ comment : comment ]
        }
    }

    def update = {
        def comment = Comment.get( params.id )
        if(comment) {
             comment.properties = params
            if(comment.save()) {
                redirect(action:show,id:comment.id)
            }
            else {
                render(view:'edit',model:[comment:comment])
            }
        }
        else {
            flash.message = "Comment not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def comment = new Comment()
        comment.properties = params
        return ['comment':comment]
    }

    def save = {
        def comment = new Comment()
        comment.properties = params
        comment.author = session.user;
        comment.created = new Date();
        comment.story = session.story;
        if(comment.save()) {
            redirect(controller:'home',action:'story')
        }
        else {
            render(view:'create',model:[comment:comment])
        }
    }
    
	def pendingComments = {
	        def comments = Comment.findAllByApproved(false,[sort:'id',order:'desc'])
	        [comments:comments]
	}
	
	def approveComment = {        
	        if(params.id){
	            def post = Comment.get(params.id)
	            post.approved = true;
	            if(post.save()){
	               flash.message = "Comentario aprobado." 
	            }
	            else{
	                flash.message = "No se pudo aprobar el comentario... Ha ocurrido un error"
	            }
	        }
	        else{
	            flash.message = "No se seleccion&oacute; ning&uacute;n comentario..." 
	        }
	        redirect(action:'pendingComments')
	}
	
	def deleteComment = {
	        if(params.id){
	            def post = Comment.get(params.id)    
	            if(post.author){
	                post.author.removeFromPosts(post);
	            }
	            if(post.delete()){
	                flash.message = "Comentario eliminado."
	            }
	            else {
	                flash.message = "No se pudo eliminar..."
	            }
	        }
	        else{
	            flash.message = "No se seleccion&oacute; ning&uacute;n comentario..." 
	        }
	        redirect(action:'pendingComments')	        
	}    

}