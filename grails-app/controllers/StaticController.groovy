/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class StaticController {

	def index = { }
	
	def legal = {}
	def localizacion = {}
	def callejero = {}
	
	def block = {
	        def block
	        if(!params.id){
	            redirect(controller:'home',action:'index')
	        }
	        else{
	            try{
	                block = Block.get(params.id)
	            }
	            catch(Exception x){
	                log.info(x)
	            }
	            /*
	            Si no encuentro el bloque por id, lo busco por titulo. De esta forma
	            podemos usar urls del tipo /static/block/<titulo>
	            */
	            if(!block){
	                def title = params.id.decodeURL()
	                log.info("Buscando bloque con titulo ${title}")
	                block = Block.findByName(title)
	            }
	        }
	        if(!block){
	            flash.message = "No se encontr&oacute; ning&uacute;n bloque con id '${params.id}'"
	            redirect(controller:'home', action:'index')
	        }
	        [block:block];
	}
}

