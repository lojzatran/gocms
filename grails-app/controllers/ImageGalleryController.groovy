/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import com.imaginaworks.util.TXTUtils
import com.imaginaworks.util.ImageUtils

class ImageGalleryController extends SecureController  {
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ imageGalleryList: ImageGallery.list( params ) ]
    }

    def show = {
        [ imageGallery : ImageGallery.get( params.id ) ]
    }

    def delete = {
        def imageGallery = ImageGallery.get( params.id )
        if(imageGallery) {
            imageGallery.delete()
            flash.message = "ImageGallery ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "ImageGallery not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def imageGallery = ImageGallery.get( params.id )

        if(!imageGallery) {
                flash.message = "ImageGallery not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ imageGallery : imageGallery ]
        }
    }

    def update = {
        def imageGallery = ImageGallery.get( params.id )
        if(imageGallery) {
             imageGallery.properties = params
            if(imageGallery.save()) {
                redirect(action:show,id:imageGallery.id)
            }
            else {
                render(view:'edit',model:[imageGallery:imageGallery])
            }
        }
        else {
            flash.message = "ImageGallery not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def imageGallery = new ImageGallery()
        imageGallery.properties = params
        return ['imageGallery':imageGallery]
    }

    def save = {
        def imageGallery = new ImageGallery()
        imageGallery.properties = params
        if(imageGallery.save()) {
            redirect(action:show,id:imageGallery.id)
        }
        else {
            render(view:'create',model:[imageGallery:imageGallery])
        }
    }
    
    def addImages = {
            if(!params.idGallery){
                flash.message = "No se recibio id de galeria"
                redirect(controller:home,action:index)
            }
            def galeria = ImageGallery.get(params.idGallery)
            def i = 1
            def procesadas = 0
            def file
            def tmpFile
            def fileName
            while((file=request.getFile("img_${i}"))){
                fileName = file.originalFilename.toLowerCase()
                tmpFile = File.createTempFile("goecms.image",".tmp")
                log.info("Procesando ${fileName}")
                if(!file.isEmpty() && (fileName.endsWith('.jpg') || fileName.endsWith('.jpeg'))){
                    file.transferTo(tmpFile)
                    addToGallery(tmpFile,params["title_${i}"],params["description_${i}"],galeria)
                    procesadas ++
                }
                else{
                    log.info("El archivo estaba vacio o no era jpeg (!)")
                }
                i++;
                tmpFile.delete()
            }
            flash.message="${procesadas} Im&aacute;genes procesadas correctamente."
            redirect(action:edit,id:galeria.id)
    }
    
    private addToGallery(imageFile,title,description,gallery){
        def quality = 75
        def gFolder = new File(servletContext.getRealPath("/files/gallery/${TXTUtils.normalizeString(gallery.name)}"))
        if(!gFolder.exists()) gFolder.mkdirs()
        
        def image = new Image(
                title:title,
                description:description,
                creationDate:new Date(),
                gallery:gallery
        ).save()
        
        log.info("Imagen ${image} creada. Procesando archivo...")
        try{
            def iFolder = new File(gFolder,"${image.id}")
            iFolder.mkdirs()
            def img1024 = new File(iFolder,"1024px.jpg")
            ImageUtils.createThumb(imageFile,img1024,quality,1024)
            def img500 = new File(iFolder,"500px.jpg")
            ImageUtils.createThumb(img1024,img500,quality,500)
            def img100 = new File(iFolder,"100px.jpg")
            ImageUtils.createThumb(img500,img100,quality,100)
            def img45 = new File(iFolder,"45px.jpg")
            ImageUtils.createThumb(img100,img45,quality,45)
            log.info("Imagen procesada correctamente.")
        }
        catch(Exception x){
            log.error("Ocurrio un error al procesar el archivo: ${x}. Borrando imagen")
            image.delete()
        }
    }

}