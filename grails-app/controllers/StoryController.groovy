/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import net.sf.classifier4J.summariser.*;
import com.imaginaworks.util.*;

class StoryController  extends SecureController{
    static final int THUMBNAIL_WIDTH = 100;
    static final int THUMBNAIL_QUALI = 75;
    static final String FILES_PATH = "/files/story";
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ storyList: Story.list( params ) ]
    }

    def show = {
        [ story : Story.get( params.id ) ]
    }

    def delete = {
        def story = Story.get( params.id )
        if(story) {
            story.delete()
            flash.message = "Story ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "Story not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def story = Story.get( params.id )
        if(!story) {
                flash.message = "Story not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ story : story ]
        }
    }
    
    def inlineEdit = {
            render(template:"storyEdit");
    }
    
    def inlineUpdate = {
            if(session.story){
                def story = Story.get(session.story.id);
                story.properties = params;
                story.intro = buildIntro(story);
                story.save();
                session.story = story;
                render(template:'/home/storyText');
            }
    }
    def update = {
        def story = Story.get( params.id )
        if(story) {
            story.properties = params
            story.intro = buildIntro(story);            
            if(story.save()) {
                redirect(action:show,id:story.id)
            }
            else {
                render(view:'edit',model:[story:story])
            }
        }
        else {
            flash.message = "Story not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def story = new Story()
        story.properties = params
        return ['story':story]
    }

    def save = {
        def story = new Story()
        story.properties = params
        story.fechaAlta = new Date();
        story.autor = session.user;
        
        story.intro = buildIntro(story);
        
        if(!params.enlace) story.enlace = new URL("${request.scheme}://${request.serverName}:${request.serverPort}/${request.contextPath}/stories")
        if(story.save()) {
            redirect(controller:'home',action:'story',id:story.id)
        }
        else {
            render(view:'create',model:[story:story])
        }
    }
    
    private buildIntro(story){
        def intro = MarkupUtils.removeMarkup(story.texto)
        if(intro.size()>250) intro = "${intro.substring(0,250)}..."
        if(findImage(story)){
            intro = """
            <img class='storyThumb' alt='${story.titulo}' border='0' src='${request.contextPath}${FILES_PATH}/${TXTUtils.normalizeString(story.titulo)}.jpg'/>${intro}
            """
        }
        return intro
    }
    
    private findImage(story){
        File folder = new File(servletContext.getRealPath("${FILES_PATH}"));
        if(!folder.exists()){
            folder.mkdirs();
        }
        log.info ("Buscando imagenes en \"${story}\"")
        def imgUrls = TXTUtils.getImageUrls(story.texto)
        log.info ("Imagenes encontradas: ${imgUrls}")
        //1. busco si hay alguna jpg
        def img
        def destino
        for(ruta in imgUrls) {
            if(ruta.endsWith(".jpg") || ruta.endsWith(".jpeg")){                
                img = ruta
                destino = new File(folder,"${TXTUtils.normalizeString(story.titulo)}.jpg")
                break
            }
        }

        if(img){            
            //2. si no es local me la bajo
            if(img.startsWith("http://") || img.startsWith("https://")){
                ImageUtils.createThumb(img,destino,THUMBNAIL_QUALI,THUMBNAIL_WIDTH);
            }
            else {
                if(request.contextPath.size() > 1){
                    img = img.substring(request.contextPath.size());
                }
                ImageUtils.createThumb(new File(servletContext.getRealPath(img)),destino,THUMBNAIL_QUALI,THUMBNAIL_WIDTH);
            }  
            return true
        }            
        return false
        
    }

}