    /* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
        
class TagController  extends SecureController{
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only
    // accept POST requests
    def allowedMethods = [delete:'POST',
                          save:'POST',
                          update:'POST']

    def list = {
        if(!params.max)params.max = 10
        [ tagList: Tag.list( params ) ]
    }

    def show = {
        [ tag : Tag.get( params.id ) ]
    }

    def delete = {
        def tag = Tag.get( params.id )
        if(tag) {
            tag.delete()
            flash.message = "Tag ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "Tag not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def tag = Tag.get( params.id )

        if(!tag) {
                flash.message = "Tag not found with id ${params.id}"
                redirect(action:list)
        }
        else {
            return [ tag : tag ]
        }
    }

    def update = {
        def tag = Tag.get( params.id )
        if(tag) {
             tag.properties = params
            if(tag.save()) {
                redirect(action:show,id:tag.id)
            }
            else {
                render(view:'edit',model:[tag:tag])
            }
        }
        else {
            flash.message = "Tag not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def tag = new Tag()
        tag.properties = params
        return ['tag':tag]
    }

    def save = {
        def tag = new Tag()
        tag.properties = params
        if(tag.save()) {
            redirect(action:show,id:tag.id)
        }
        else {
            render(view:'create',model:[tag:tag])
        }
    }

}