/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class IWConfig {
    /*
     Aqui habra que ir colocando los parametros de configuracion
     que vayan surgiendo a medida que evolucione el proyecto.
    */
    String mailHost;
    String mailFrom;
    boolean mailAuth;
    String mailUsr;
    String mailPwd;

    String webmasterEmail;
    String contactEmail;    

    String mediaAllowedMimeTypes;

    String siteDescription;
    String siteAuthor;
    String siteOwner;
    String siteKeywords;
    String siteTitle;
    String userCookieName;
    
    boolean moderateComments = true;
    
}
