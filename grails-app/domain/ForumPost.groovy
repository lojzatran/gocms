/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class ForumPost { 
    static hasMany = [replies:ForumPost];
    static belongsTo = [Forum, ForumPost];
    
    Forum forum;
    ForumPost parent;
    User author;
    
    String title;
    String text;
    Date created;
    
    boolean approved = false;
    int replyCount = 0;
    int views = 0;
    
    static constraints = {
        title(nullable:false)
        text(maxSize:999999)
        parent(nullable:true);
        forum(nullable:true);
    }
    
    String toString(){
        if(!title){
            return "Post sin titulo";
        }
        else if(title.size() < 100){
            return title;
        }
        else{
            return "${title.substring(0,97)}...";
        }        
    }
}	
