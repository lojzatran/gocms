/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class User {
    //Nacho: "user" is a reserved word in many databases...
    static withTable = "gocms_users"
    static hasMany = [comments:Comment, posts:ForumPost];
    
    Date fechaAlta;
    String nombre;
    String clave;
    String roles;
    String perfil;
    String email;
    String openID;
    String web;
    
    static constraints = {
        nombre(unique:true,size:5..15)
        clave(matches:/[\w\d]+/,size:6..12)
        email(email:true)
        perfil(blank:true,maxSize:1000)
        web(url:true,nullable:true)
        openID(url:true,nullable:true)
        roles(blank:true,nullable:true)
    }

    String toString(){
        nombre?nombre:"";
    }
}
