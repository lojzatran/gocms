/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class Story { 
    static searchable = [only: ['titulo', 'intro']]    
    static hasMany = [comments:Comment];
    def belongsTo = Section;
    
    User autor;
    Section seccion;
    //SEO:
    String normalizado;
    String keywords;
    String descripcion;
    
    String titulo;
    String intro;
    String texto;
    URL enlace;
    Date fechaAlta;
    int lecturas = 0;
        
    static constraints = {
        titulo(blank:false)
        texto(maxSize:100000)
        intro(maxSize:1000)
        enlace(url:true)
    }
    
    String toString(){
        titulo;
    }
}	
