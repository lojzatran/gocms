/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class Event { 
    static searchable = [only: ['intro', 'title']]
    static belongsTo=[EventCategory]
    
    EventCategory category
    Date date
    String title
    String description
    String intro
    
    static constraints = {
        date(nullable:false)
        title(nullable:false)
        category(nullable:false)
        description(maxSize:999999)
    }
    
    String toString(){
        title;
    }
}	
