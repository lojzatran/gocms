/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class Document { 
    static searchable = [only: ['title', 'description']]    
    static belongsTo = [DocumentCategory,Section]
    
    DocumentCategory category
    Section section
    
    String title
    String mimeType = ""
    String normalized
    String description
    String fileName = ""
    String keywords
    
    int bytes = 0
    Date creationDate = new Date()
    
    static constraints = {
        category(nullable:false)
        title(nullable:false)
        description(maxSize:999999)
        keywords(nullable:true)
    }
    
    String toString(){
        title
    }
}	
