/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import com.imaginaworks.util.DateUtils

/**
 * A  tag lib that provides tags for working with Yahoo calendar controls inside forms
 *
 * @author Peter Kelley
 * @since 15-September-2006
 */


class YahooCalendarTagLib {
	 /**
	  * Tag to output the correct javascript utility functions to support the calendar tags.
	  * This tag takes no parameters. This tag should be placed in the page header once if
	  * the yuiCalendarHeader and yuiCalendarBody tags are to be used. Only a single instance
	  * of this tag is ever required.
	  */
	 def yuiCalendarUtils = {attrs, body ->
	 	out << "<script language='javascript'>\n"
	 	out << "YAHOO.namespace('com.imaginaworks.goecms');\n"
	 	out << "function showYUICalendar(calendarName, calendarInstance) {\n"
	 	out << "    var linkName = calendarName + 'Link';\n"
	 	out << "    var link = document.getElementById(linkName);\n"
//	 	out << "    var pos = YAHOO.util.Dom.getXY(link);\n"
	 	out << "    calendarInstance.oDomContainer.style.display='block';\n"
//	 	out << "    YAHOO.util.Dom.setXY(calendarInstance.oDomContainer, [pos[0]+link.offsetWidth+5,pos[1]]);\n"
	 	out << "}\n"
	 	out << "function setCalendarDateValue(calendarName, calendarInstance, displayFieldName) {\n"
	 	out << "    var selected = calendarInstance.getSelectedDates()[0];\n"
	 	//out << "    calendarInstance.oDomContainer.style.display='none';\n"
	 	out << "    var submitField = document.getElementById(calendarName + '_day');\n"
	 	out << "    submitField.value = selected.format('dd');\n"
	 	out << "    submitField = document.getElementById(calendarName + '_month');\n"
	 	out << "    submitField.value = selected.format('MM');\n"
	 	out << "    submitField = document.getElementById(calendarName + '_year');\n"
	 	out << "    submitField.value = selected.format('yyyy');\n"
	 	out << "    submitField = document.getElementById(calendarName + '_hour');\n"
	 	out << "    submitField.value = selected.format('HH');\n"
	 	out << "    submitField = document.getElementById(calendarName + '_minute');\n"
	 	out << "    submitField.value = selected.format('mm');\n"
	 	out << "    var displayField = document.getElementById(displayFieldName);\n"
	 	out << "    displayField.innerHTML = selected.format('E d MMM yyyy');\n"
	 	out << "}\n"
	 	out << "</script>\n"
	 }

	 /**
	  * Tag to provide initialization code for a single Yahoo calendar instance. This tag
	  * should be used once for each calendar instance used on the page. This tag
	  * should be placed in the page header and paired with a yuiCalendarBody tag in the body with
	  * the same value for the calendarName parameter.
	  * @param name the name of the calendar instance
	  * @param value the date value to set the calendar to
	  */
	 def yuiCalendarHeader = {attrs, body ->
	 	def calendarName = attrs.name
	 	if (calendarName == null) {
	 		throw new IllegalArgumentException("name parameter must be specified")
	 	}
	 	Date dateValue = attrs.value
	 	out << "<script language='javascript'>\n"
	 	out << 'YAHOO.namespace("com.imaginaworks.goecms");\n'
	 	out << "function init${calendarName}() {\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName} = new YAHOO.widget.Calendar('YAHOO.com.imaginaworks.goecms.${calendarName}', '${calendarName}Container');\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.title = 'Select ${calendarName}';\n"
	 	//out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.onSelect = set${calendarName};\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.selectEvent.subscribe(set${calendarName});\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('DATE_FIELD_DELIMITER', '/');\n"

	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('MDY_DAY_POSITION', 1);\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('MDY_MONTH_POSITION', 2);\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('MDY_YEAR_POSITION', 3);\n"

	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('MD_DAY_POSITION', 1);\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('MD_MONTH_POSITION', 2);\n"

	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('MONTHS_SHORT',   ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']);\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('MONTHS_LONG',    ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']);\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('WEEKDAYS_1CHAR', ['D', 'L', 'M', 'X', 'J', 'V', 'S']);\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('WEEKDAYS_SHORT', ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa']);\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('WEEKDAYS_MEDIUM',['Dom','Lun','Mar','Mi&eacute;','Jue','Vie','S&aacute;b']);\n"
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.cfg.setProperty('WEEKDAYS_LONG',  ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado']);\n"
	 	
	 	out << "    YAHOO.com.imaginaworks.goecms.${calendarName}.render();\n"
	 	out << "}\n"
	 	out << "function set${calendarName}() {\n"
	 	out << "    var calendarInstance = YAHOO.com.imaginaworks.goecms.${calendarName};\n"
	 	out << "    setCalendarDateValue('${calendarName}', calendarInstance, '${calendarName}Value');\n"
	 	out << "}\n"
	 	out << "function show${calendarName}() {\n"
	 	out << "    var calendarInstance = YAHOO.com.imaginaworks.goecms.${calendarName};\n"
	 	out << "    showYUICalendar(\"${calendarName}\", calendarInstance);\n"
	 	out << "}\n"
	 	out << "YAHOO.util.Event.addListener(window, 'load', init${calendarName});\n"
	 	out << "</script>\n"
	 }

	 /**
	  * Tag to provide field code for a single Yahoo calendar instance. This tag
	  * should be used once for each calendar instance used on the page. This tag
	  * should be placed in the page body and paired with a yuiCalendarHeader tag in the header with
	  * the same value for the calendarName parameter.
	  * @param name the name of the calendar instance
	  * @param bean the bean containing the date parameter to set the display and hidden fields to
	  */
	 def yuiCalendarBody = { attrs, body ->
	 	def calendarName = attrs.name
	 	def bean = attrs.bean
	 	if (calendarName == null) {
	 		throw new IllegalArgumentException("name parameter must be specified")
	 	}
	 	if (bean == null) {
	 		throw new IllegalArgumentException("bean parameter must be specified")
	 	}

	 	def day = ""
	 	def month = ""
	 	def year = ""
	 	def hour = ""
	 	def minute = ""

	 	def dateDisplay = ""

	 	if (bean[calendarName] != null) {
	 		//Set the various date values to use in the output
	 		def calendar = new GregorianCalendar()
	 		calendar.setTime(bean[calendarName])

		 	day = calendar.get(Calendar.DATE)
			month = calendar.get(Calendar.MONTH) + 1 // Stupid Java months start at 0
			year = calendar.get(Calendar.YEAR)
			hour = calendar.get(Calendar.HOUR_OF_DAY) // 24 hour clock
			minute = calendar.get(Calendar.MINUTE)

//	        def formatter = new SimpleDateFormat("E d MMMM yyyy")
	 		dateDisplay = DateUtils.format(bean[calendarName],'E, dd MMMM yyyy')
	 	}
	 	def applicationUri = grailsAttributes.getApplicationUri(request)
	 	out << "<span class='value ${hasErrors('bean':bean,field:calendarName,'errors')}'>\n"
	 	out << "<span id='${calendarName}Value'>${dateDisplay}</span>\n"
//	 	out << "<a id='choose${calendarName}' onclick='show${calendarName}()' href='javascript:void(null)' >\n"
//	 	out << "    <img style='border:none;' border='0' id='${calendarName}Link' style='margin: 5px; vertical-align: middle' src='${applicationUri}/images/office-calendar.png' />\n"
//	 	out << "</a></span>\n"
	 	out << "<div id='${calendarName}Container' class='form_calendar'></div>\n"
	 	out << "<input id='${calendarName}' type='hidden' name='${calendarName}' value='struct'/>\n"
	 	out << "<input id='${calendarName}_day' type='hidden' name='${calendarName}_day' value='${day}'/>\n"
		out << "<input id='${calendarName}_month' type='hidden' name='${calendarName}_month' value='${month}'/>\n"
	 	out << "<input id='${calendarName}_year' type='hidden' name='${calendarName}_year' value='${year}'/>\n"
	 	out << "<input id='${calendarName}_hour' type='hidden' name='${calendarName}_hour' value='${hour}'/>\n"
	 	out << "<input id='${calendarName}_minute' type='hidden' name='${calendarName}_minute' value='${minute}'/>\n"
 	}

}