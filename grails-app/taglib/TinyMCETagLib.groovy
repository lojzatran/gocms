/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import org.apache.commons.logging.LogFactory

class TinyMCETagLib {
    def tinyMCEHEader = {attrs ->
        def applicationUri = grailsAttributes.getApplicationUri(request)
    	out<<"<script language='javascript' type='text/javascript' src='${applicationUri}/js/tiny_mce/tiny_mce.js'></script>";
        out<<"""
        <script language='javascript' type='text/javascript'>
			tinyMCE.init({
			mode : "textareas",
			theme : "advanced",
			theme_advanced_buttons1 : "bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,undo,redo,link,unlink",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			extended_valid_elements : "a[name|href|target|title|onclick],hr[class|width|size|noshade]"
			});
		</script>""";
    }

    def extendedMCEHEader = {attrs ->
    def applicationUri = grailsAttributes.getApplicationUri(request)
	out<<"<script language='javascript' type='text/javascript' src='${applicationUri}/js/tiny_mce/tiny_mce.js'></script>";
    out<<"""
    <script language='javascript' type='text/javascript'>
		tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "style",
		theme_advanced_buttons1 : "bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,undo,redo,link,unlink,image,styleprops,code,removeformat",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|style],hr[class|width|size|noshade],div[style,class,id],script[src,type]"
		});
	</script>""";
}


    def activateEditor = {attrs ->
    	out<<"""
		<script language='javascript' type='text/javascript'>
    	tinyMCE.execCommand("mceAddControl", false, "${attrs.fieldID}");
		</script>
    	""";
    }
}