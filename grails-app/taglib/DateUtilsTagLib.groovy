/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

import com.imaginaworks.util.DateUtils;

class DateUtilsTagLib {
    //static final SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MM/yyyy");
    //static final SimpleDateFormat weekDay = new SimpleDateFormat("EEEE",new Locale("es","ES"));
    //static final SimpleDateFormat time = new SimpleDateFormat("HH:mm");
    def format = { attrs ->
            def value = attrs['value'] ? attrs['value'] : new Date();
            out << DateUtils.format(value,'EEEE dd/MM/yyyy');
    }

    def howMuchTimeAgo = { attrs ->
            def value = attrs['value'];
            if(value){
                long millis = System.currentTimeMillis() - value.getTime();
                // Get difference in seconds
                long diffSecs = millis/(1000);
                // Get difference in minutes
                long diffMins = millis/(60*1000);
                // Get difference in hours
                long diffHours = millis/(60*60*1000);
                // Get difference in days
                long diffDays = millis/(24*60*60*1000);
                // Get difference in weeks
                long diffWeeks = millis/(7*24*60*60*1000);
                //si es de hace ms de 3 semanas pongo la fecha.
                if(diffWeeks>3){
                    out<<sdf.format(value);
                }
                else if(diffWeeks > 0){
                    if(diffWeeks == 1){
                        out<<"Publicado el ${DateUtils.format(value,'EEEE')} de la semana pasada.";
                    }
                    else {
                        out<<"Publicado hace ${diffWeeks} semanas.";
                    }
                }
                else if(diffDays > 0){
                    if(diffDays == 1){
                        out<<"Publicado ayer a las ${DateUtils.format(value,'HH:mm')}.";
                    }
                    else {
                        out <<"Publicado hace ${diffDays} d&#237;as.";
                    }
                }
                else if(diffHours > 0){
                    out<<"Publicado hace ${diffHours} horas ${diffMins - diffHours*60} minutos.";
                }
                else if(diffMins > 0){
                    out<<"Publicado hace ${diffMins} minutos.";
                }
                else{
                    out<<"Publicado hace ${diffSecs} segundos.";
                }
            }
            else{
                out << "Fecha desconocida.";
            }
    }
}