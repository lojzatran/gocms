/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class EmailTagLib {
    MailService mailService;

    def sendMail = {attrs, body ->
    def smtp = IWConfig.list()[0].mailHost
    def from = IWConfig.list()[0].mailFrom
    if(!attrs.to) attrs.to = IWConfig.list()[0].webmasterEmail
    try{
        mailService.sendHTML(smtp, from, attrs.to, body(), attrs.subject);        
    }
    catch(Exception x){
        log.error("No se pudo enviar le correo: ${x}")
    }
	
}
}