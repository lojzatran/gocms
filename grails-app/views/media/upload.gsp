<html>
 <head>
  <meta name="layout" content="main" />
  <title>Create Story</title>
 </head>
 <body>
  <div class="body">
   <h1>Subir contenidos</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:form action="save" method="post" enctype="multipart/form-data">
     <input type="hidden" name="fileCount" value="5"/>
     <div class="formButton centered"><input type="file" name="file_1" /></div>
     <div class="formButton centered"><input type="file" name="file_2" /></div>
     <div class="formButton centered"><input type="file" name="file_3" /></div>
     <div class="formButton centered"><input type="file" name="file_4" /></div>
     <div class="formButton centered"><input type="file" name="file_4" /></div>
    <div class="buttons">
     <span class="formButton">
      <input type="submit" value="Guardar" />
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>