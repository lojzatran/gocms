  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>TagReference List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="create">New TagReference</g:link></span>
        </div>
        <div class="body">
           <h1>TagReference List</h1>
            <g:if test="${flash.message}">
                 <div class="message">
                       ${flash.message}
                 </div>
            </g:if>
           <table>
             <thead>
               <tr>
                   
                                      
                        <th>Id</th>
                                      
                        <th>Story</th>
                                      
                        <th>Tag</th>
                                      
                        <th>User</th>
                   
                   <th></th>
               </tr>
             </thead>
             <tbody>
               <g:each in="${tagReferenceList}">
                    <tr>
                       
                            <td>${it.id?.encodeAsHTML()}</td>
                       
                            <td>${it.story?.encodeAsHTML()}</td>
                       
                            <td>${it.tag?.encodeAsHTML()}</td>
                       
                            <td>${it.user?.encodeAsHTML()}</td>
                       
                       <td class="actionButtons">
                            <span class="actionButton"><g:link action="show" id="${it.id}">Show</g:link></span>
                       </td>
                    </tr>
               </g:each>
             </tbody>
           </table>
               <div class="paginateButtons">
                   <g:paginate total="${TagReference.count()}" />
               </div>
        </div>
    </body>
</html>
