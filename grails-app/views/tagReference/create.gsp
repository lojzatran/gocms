  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Create TagReference</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">TagReference List</g:link></span>
        </div>
        <div class="body">
           <h1>Create TagReference</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <g:hasErrors bean="${tagReference}">
                <div class="errors">
                    <g:renderErrors bean="${tagReference}" as="list" />
                </div>
           </g:hasErrors>
           <g:form action="save" method="post" >
               <div class="dialog">
                <table>
                    <tbody>

                       
                       
                                  <tr class='prop'><td class='name'><label for='story'>Story:</label></td><td class='value ${hasErrors(bean:tagReference,field:'story','errors')}'><g:select optionKey="id" from="${Story.list()}" name='story.id' value="${tagReference?.story?.id}"></g:select></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='tag'>Tag:</label></td><td class='value ${hasErrors(bean:tagReference,field:'tag','errors')}'><g:select optionKey="id" from="${Tag.list()}" name='tag.id' value="${tagReference?.tag?.id}"></g:select></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='user'>User:</label></td><td class='value ${hasErrors(bean:tagReference,field:'user','errors')}'><g:select optionKey="id" from="${User.list()}" name='user.id' value="${tagReference?.user?.id}"></g:select></td></tr>
                       
                    </tbody>
               </table>
               </div>
               <div class="buttons">
                     <span class="formButton">
                        <input type="submit" value="Create"></input>
                     </span>
               </div>
            </g:form>
        </div>
    </body>
</html>
