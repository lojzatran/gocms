  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
          <meta name="layout" content="main" />
         <title>Show TagReference</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">TagReference List</g:link></span>
            <span class="menuButton"><g:link action="create">New TagReference</g:link></span>
        </div>
        <div class="body">
           <h1>Show TagReference</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <div class="dialog">
                 <table>
                   
                   <tbody>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Id:</td>
                              
                                    <td valign="top" class="value">${tagReference.id}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Story:</td>
                              
                                    <td valign="top" class="value"><g:link controller="story" action="show" id="${tagReference?.story?.id}">${tagReference?.story}</g:link></td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Tag:</td>
                              
                                    <td valign="top" class="value"><g:link controller="tag" action="show" id="${tagReference?.tag?.id}">${tagReference?.tag}</g:link></td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">User:</td>
                              
                                    <td valign="top" class="value"><g:link controller="user" action="show" id="${tagReference?.user?.id}">${tagReference?.user}</g:link></td>
                              
                        </tr>
                   
                   </tbody>
                 </table>
           </div>
           <div class="buttons">
               <g:form controller="tagReference">
                 <input type="hidden" name="id" value="${tagReference?.id}" />
                 <span class="button"><g:actionSubmit value="Edit" /></span>
                 <span class="button"><g:actionSubmit value="Delete" /></span>
               </g:form>
           </div>
        </div>
    </body>
</html>
