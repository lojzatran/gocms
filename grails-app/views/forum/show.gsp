  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
          <meta name="layout" content="main" />
         <title>Show Forum</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">Forum List</g:link></span>
            <span class="menuButton"><g:link action="create">New Forum</g:link></span>
        </div>
        <div class="body">
           <h1>Show Forum</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <div class="dialog">
                 <table>
                   
                   <tbody>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Id:</td>
                              
                                    <td valign="top" class="value">${forum.id}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Name:</td>
                              
                                    <td valign="top" class="value">${forum.name}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Description:</td>
                              
                                    <td valign="top" class="value">${forum.description}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Owner:</td>
                              
                                    <td valign="top" class="value"><g:link controller="user" action="show" id="${forum?.owner?.id}">${forum?.owner}</g:link></td>
                              
                        </tr>
                        <tr class="prop">
                              <td valign="top" class="name">Moderated:</td>
                              
                                    <td valign="top" class="value">${forum?.moderated}</td>
                              
                        </tr>
                                                                   <tr class="prop">
                              <td valign="top" class="name">Threads:</td>
                              
                                     <td  valign="top" style="text-align:left;" class="value">
                                        <ul>
                                            <g:each var="t" in="${forum.threads}">
                                                <li><g:link controller="forumPost" action="show" id="${t.id}">${t}</g:link></li>
                                            </g:each>
                                        </ul>
                                     </td>
                              
                        </tr>
                   </tbody>
                 </table>
           </div>
           <div class="buttons">
               <g:form controller="forum">
                 <input type="hidden" name="id" value="${forum?.id}" />
                 <span class="button"><g:actionSubmit value="Edit" /></span>
                 <span class="button"><g:actionSubmit value="Delete" /></span>
               </g:form>
           </div>
        </div>
    </body>
</html>
