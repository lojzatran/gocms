<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Edit Forum</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <a href="${createLinkTo(dir:'')}">Home</a>
   </span>
   <span class="menuButton">
    <g:link action="list">Forum List</g:link>
   </span>
   <span class="menuButton">
    <g:link action="create">New Forum</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Edit Forum</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${forum}">
    <div class="errors">
     <g:renderErrors bean="${forum}" as="list" />
    </div>
   </g:hasErrors>
   <div class="prop">
    <span class="name">Id:</span>
    <span class="value">${forum?.id}</span>
   </div>
   <g:form controller="forum" method="post">
    <input type="hidden" name="id" value="${forum?.id}" />
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='name'>Name:</label>
        </td>
        <td class='value ${hasErrors(bean:forum,field:' name ',' errors ')}'>
         <input type="text" name='name' value="${forum?.name?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='description'>Description:</label>
        </td>
        <td class='value ${hasErrors(bean:forum,field:' description ',' errors ')}'>
         <textarea rows='5' cols='40' name='description'>${forum?.description?.encodeAsHTML()}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='owner'>Owner:</label>
        </td>
        <td class='value ${hasErrors(bean:forum,field:' owner ',' errors ')}'>
         <g:select optionKey="id" from="${User.list()}" name='owner.id' value="${forum?.owner?.id}"></g:select>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='moderated'>Moderated:</label>
        </td>
        <td class='value ${hasErrors(bean:forum,field:' moderated ',' errors ')}'>
         <g:checkBox name="moderated" value="${forum?.moderated}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='threads'>Threads:</label>
        </td>
        <td class='value ${hasErrors(bean:forum,field:' threads ',' errors ')}'>
         <ul>
          <g:link controller='forumPost' params='["forum.id":forum?.id]' action='create'>Add ForumPost</g:link>
          <g:each var='t' in='${forum?.threads?}'>
           <li>
            <g:link controller='forumPost' action='show' id='${t.id}'>${t}</g:link>
           </li>
          </g:each>
         </ul>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="button">
      <g:actionSubmit value="Update" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
