  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
          <meta name="layout" content="main" />
         <title>Show User</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">User List</g:link></span>
            <span class="menuButton"><g:link action="create">New User</g:link></span>
        </div>
        <div class="body">
           <h1>Show User</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <div class="dialog">
                 <table>
                   
                   <tbody>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Id:</td>
                              
                                    <td valign="top" class="value">${user.id}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Nombre:</td>
                              
                                    <td valign="top" class="value">${user.nombre}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Clave:</td>
                              
                                    <td valign="top" class="value">${user.clave}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Email:</td>
                              
                                    <td valign="top" class="value">${user.email}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Perfil:</td>
                              
                                    <td valign="top" class="value">${user.perfil}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Web:</td>
                              
                                    <td valign="top" class="value">${user.web}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Open ID:</td>
                              
                                    <td valign="top" class="value">${user.openID}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Roles:</td>
                              
                                    <td valign="top" class="value">${user.roles}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Comments:</td>
                              
                                     <td  valign="top" style="text-align:left;" class="value">
                                        <ul>
                                            <g:each var="c" in="${user.comments}">
                                                <li><g:link controller="comment" action="show" id="${c.id}">${c}</g:link></li>
                                            </g:each>
                                        </ul>
                                     </td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Fecha Alta:</td>
                              
                                    <td valign="top" class="value">${user.fechaAlta}</td>
                              
                        </tr>
                   
                   </tbody>
                 </table>
           </div>
           <div class="buttons">
               <g:form controller="user">
                 <input type="hidden" name="id" value="${user?.id}" />
                 <span class="button"><g:actionSubmit value="Edit" /></span>
                 <span class="button"><g:actionSubmit value="Delete" /></span>
               </g:form>
           </div>
        </div>
    </body>
</html>
