  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Create User</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">User List</g:link></span>
        </div>
        <div class="body">
           <h1>Create User</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <g:hasErrors bean="${user}">
                <div class="errors">
                    <g:renderErrors bean="${user}" as="list" />
                </div>
           </g:hasErrors>
           <g:form action="save" method="post" >
               <div class="dialog">
                <table>
                    <tbody>

                       
                       
                                  <tr class='prop'><td class='name'><label for='nombre'>Nombre:</label></td><td class='value ${hasErrors(bean:user,field:'nombre','errors')}'><input type="text" maxlength='15' name='nombre' value="${user?.nombre?.encodeAsHTML()}"/></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='clave'>Clave:</label></td><td class='value ${hasErrors(bean:user,field:'clave','errors')}'><input type="text" maxlength='12' name='clave' value="${user?.clave?.encodeAsHTML()}"/></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='email'>Email:</label></td><td class='value ${hasErrors(bean:user,field:'email','errors')}'><input type="text" name='email' value="${user?.email?.encodeAsHTML()}"/></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='perfil'>Perfil:</label></td><td class='value ${hasErrors(bean:user,field:'perfil','errors')}'><textarea rows='5' cols='40' name='perfil'>${user?.perfil?.encodeAsHTML()}</textarea></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='web'>Web:</label></td><td class='value ${hasErrors(bean:user,field:'web','errors')}'><input type="text" name='web' value="${user?.web?.encodeAsHTML()}"/></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='openID'>Open ID:</label></td><td class='value ${hasErrors(bean:user,field:'openID','errors')}'><input type="text" name='openID' value="${user?.openID?.encodeAsHTML()}"/></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='roles'>Roles:</label></td><td class='value ${hasErrors(bean:user,field:'roles','errors')}'><input type="text" name='roles' value="${user?.roles?.encodeAsHTML()}"/></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='fechaAlta'>Fecha Alta:</label></td><td class='value ${hasErrors(bean:user,field:'fechaAlta','errors')}'><g:datePicker name='fechaAlta' value="${user?.fechaAlta}"></g:datePicker></td></tr>
                       
                    </tbody>
               </table>
               </div>
               <div class="buttons">
                     <span class="formButton">
                        <input type="submit" value="Create"></input>
                     </span>
               </div>
            </g:form>
        </div>
    </body>
</html>
