<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <g:extendedMCEHEader></g:extendedMCEHEader>  
  <title>Registrar Evento</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <a href="${createLinkTo(dir:'')}">Home</a>
   </span>
   <span class="menuButton">
    <g:link action="list">Event List</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Create Event</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${event}">
    <div class="errors">
     <g:renderErrors bean="${event}" as="list" />
    </div>
   </g:hasErrors>
   <g:form action="save" method="post">
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='date'>Date:</label>
        </td>
        <td class='value ${hasErrors(bean:event,field:' date ',' errors ')}'>
         <g:datePicker name='date' value="${event?.date}"></g:datePicker>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='title'>Title:</label>
        </td>
        <td class='value ${hasErrors(bean:event,field:' title ',' errors ')}'>
         <input type="text" name='title' value="${event?.title?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='category'>Category:</label>
        </td>
        <td class='value ${hasErrors(bean:event,field:' category ',' errors ')}'>
         <g:select optionKey="id" from="${EventCategory.list()}" name='category.id' value="${event?.category?.id}"></g:select>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='description'>Description:</label>
        </td>
        <td class='value ${hasErrors(bean:event,field:' description ',' errors ')}'>
         <textarea rows='5' cols='40' name='description'>${event?.description?.encodeAsHTML()}</textarea>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="formButton">
      <input type="submit" value="Create"></input>
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
