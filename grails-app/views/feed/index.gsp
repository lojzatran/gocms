<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Story List</title>
 </head>
 <body>
  <h1>Sindicaci&oacute;n de noticias</h1>
  <p>
  La sindicaci&#243;n de noticias permite acceder al contenido de esta web por medios distintos a un navegador. Por ejemplo, puedes integrar el contenido de la p&#225;gina en tu propia web, o en tu escritorio si tu sistema operativo lo permite.
  </p>
  <p>
  Elije el canal al que quieres suscribirte:
  <ul>
  <li><g:link controller="feed" action="rssStories">Notas de prensa</g:link></li>
  <li><g:link controller="feed" action="rssEvents">Eventos del d&iacute;a</g:link></li>
  </ul>
  </p>
 </body>
</html>
