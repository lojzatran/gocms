<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Story List</title>
 </head>
 <body>
   <h1>Notas de Prensa</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:each in="${storyList}" var="story">
    <g:render template="/home/storyIntro" bean="${story}" />
   </g:each>
   <div class="paginateButtons">
    <g:paginate total="${Story.count()}" controller="stories" action="index" />
   </div>
 </body>
</html>
