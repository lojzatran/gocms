<%@ page import="org.springframework.util.ClassUtils" %>
<%@ page import="org.codehaus.groovy.grails.plugins.searchable.SearchableUtils" %>
<%@ page import="org.codehaus.groovy.grails.plugins.searchable.lucene.LuceneUtils" %>
<html>
  <head>
    <title></title>
    <meta name="layout" content="main" />
 
  </head>
  <body>
    <g:set var="haveQuery" value="${params.q?.trim()}" />
    <g:set var="haveResults" value="${searchResult?.results}" />
        <g:if test="${haveQuery && haveResults}">
        <h1>
          Resultados <strong>${searchResult.offset + 1}</strong> a <strong>${searchResult.results.size() + searchResult.offset}</strong> de un total de <strong>${searchResult.total}</strong>
          para "<strong>${params.q}</strong>"
        </h1>
        </g:if>

    <g:if test="${parseException}">
      <p>Disculpa, no entiendo tu consulta "<strong>${params.q}</strong>" .</p>
      <p>Sugerencias:</p>
      <ul>
        <g:if test="${LuceneUtils.queryHasSpecialCharacters(params.q)}">
        <g:set var="cleanQuery" value="${LuceneUtils.cleanQuery(params.q)}"/>
        <g:set var="escapedQuery" value="${LuceneUtils.escapeQuery(params.q)}"/>
          <li>Elimina caracteres especiales, como <strong>" - [ ]</strong>, por ejemplo: <em><strong>${cleanQuery}</strong></em><br />
              <em>Si quieres buscar resultados para "${cleanQuery}" <g:link controller="search" action="index" params="[q: cleanQuery]">haz click aqu&iacute;</g:link></em>
          </li>
          <li>Sustituye caracteres especiales como <strong>" - [ ]</strong> por su secuencia de escape con <strong>\</strong>, por ejemplo, <em><strong>${escapedQuery}</strong></em><br />
              <em>Si quieres buscar resultados para "${escapedQuery}" <g:link controller="search" action="index" params="[q: escapedQuery]">haz click aqu&iacute;</g:link></em>
          </li>
        </g:if>
      </ul>
    </g:if>
    <g:elseif test="${haveQuery && !haveResults}">
      <p>No se encontraron resultados para "<strong>${params.q}</strong>"</p>
    </g:elseif>
    <g:elseif test="${haveResults}">
      <div class="results">
        <g:each var="result" in="${searchResult.results}" status="index">
          <div class="result">
            <g:set var="className" value="${ClassUtils.getShortName(result.getClass())}" />
            <g:if test="${className == 'Story'}">
            <g:render template="/home/storyIntro" bean="${Story.get(result.id)}"/>            
            </g:if>
            <g:elseif test="${className == 'Event'}">
            <g:render template="/events/eventIntro" bean="${Event.get(result.id)}"/>
            </g:elseif>
            <g:elseif test="${className == 'Document'}">
            <g:render template="/documents/documentIntro" bean="${Document.get(result.id)}"/>
            </g:elseif>            
          </div>
        </g:each>
      </div>

      <div>
        <div class="paging">
          <g:if test="${haveResults}">
              P&aacute;g:
              <g:set var="totalPages" value="${Math.ceil(searchResult.total / searchResult.max)}" />
              <g:if test="${totalPages == 1}"><span class="currentStep">1</span></g:if>
              <g:else><g:paginate controller="searchable" action="index" params="[q: params.q]" total="${searchResult.total}" prev="&lt; Anterior" next="Siguiente &gt;"/></g:else>
          </g:if>
        </div>
      </div>
    </g:elseif>
  </body>
</html>