<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Show ForumPost</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <a href="${createLinkTo(dir:'')}">Home</a>
   </span>
   <span class="menuButton">
    <g:link action="list">ForumPost List</g:link>
   </span>
   <span class="menuButton">
    <g:link action="create">New ForumPost</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Show ForumPost</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <div class="dialog">
    <table>
     <tbody>
      <tr class="prop">
       <td valign="top" class="name">Id:</td>
       <td valign="top" class="value">${forumPost.id}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Title:</td>
       <td valign="top" class="value">${forumPost.title}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Text:</td>
       <td valign="top" class="value">${forumPost.text}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Created:</td>
       <td valign="top" class="value">${forumPost.created}</td>
      </tr>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Approved:</td>
       <td valign="top" class="value">${forumPost.approved}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Replies:</td>
       <td valign="top" style="text-align:left;" class="value">
        <ul>
         <g:each var="r" in="${forumPost.replies}">
          <li>
           <g:link controller="forumPost" action="show" id="${r.id}">${r}</g:link>
          </li>
         </g:each>
        </ul>
       </td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Forum:</td>
       <td valign="top" class="value">
        <g:link controller="forum" action="show" id="${forumPost?.forum?.id}">${forumPost?.forum}</g:link>
       </td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Parent:</td>
       <td valign="top" class="value">
        <g:link controller="forumPost" action="show" id="${forumPost?.parent?.id}">${forumPost?.parent}</g:link>
       </td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Author:</td>
       <td valign="top" class="value">
        <g:link controller="user" action="show" id="${forumPost?.author?.id}">${forumPost?.author}</g:link>
       </td>
      </tr>
     </tbody>
    </table>
   </div>
   <div class="buttons">
    <g:form controller="forumPost">
     <input type="hidden" name="id" value="${forumPost?.id}" />
     <span class="button">
      <g:actionSubmit value="Edit" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </g:form>
   </div>
  </div>
 </body>
</html>
