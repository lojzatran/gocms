<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="layout" content="main" />
		<title>Forum List</title>
	</head>
	<body>
		<div class="body">
            <g:if test="${flash.message}">
                 <div class="message">
                       ${flash.message}
                 </div>
            </g:if>
			<h1>Mensajes pendientes de aprobaci&oacute;n</h1> 
         <g:if test="${posts.size()}">
            <p>
            Los siguientes mensajes pertenecen a foros moderados y requieren la aprobaci&#243;n de un supervisor antes de aparecer en la web. Para cada mensaje, deber&#225; pulsar uno de los dos botones, "Aprobar" para que el mensaje pase a ser visible en la web, o "Eliminar" para borrarlo definitivamente.
            </p>
			<g:each in="${posts}" var="c">                
				<div class="comment">
					<div class="commentTitle">
						<h2>${c.title}</h2>
                        <g:if test="${c.forum}">
                        <h3>Foro: "${c.forum.name}"</h3>
                        </g:if>
                        <g:elseif test="${c.parent?.forum}">
                        <h3>Foro: "${c.parent.forum.name}"</h3>
                        </g:elseif>                        
						${c.author?.nombre.encodeAsHTML()} -
						<g:howMuchTimeAgo value="${c.created}" />
					</div>
					${c.text}
				</div>
                <div class="dialog">
                <g:form name="aprobar_${c.id}" action="approvePost" style="float:left; margin-right: 1em;" onsubmit="return confirm('Va a AUTORIZAR este mensaje. Esta seguro?');">
                <input type="hidden" name="id" value="${c.id}"/>
                <input style="padding-left:18px;background-position:left;background-repeat:no-repeat;background-image:url('${createLinkTo(dir:'images',file:'ok.gif')}')" type="submit" name="s" value="Aprobar"/>
                </g:form>
                <g:form name="eliminar_${c.id}" action="deletePost" onsubmit="return confirm('Va a ELIMINAR este mensaje. Esta seguro?');">
                <input type="hidden" name="id" value="${c.id}"/>
                <input style="padding-left:18px;background-position:left;background-repeat:no-repeat;background-image:url('${createLinkTo(dir:'images',file:'ko.gif')}')" type="submit" name="s" value="Eliminar"/>
                </g:form>
                </div>
			</g:each>
          </g:if>
          <g:else>
          <p>No hay mensajes pendientes de aprobaci&oacute;n.</p>
          </g:else>
		</div>
	</body>
</html>
