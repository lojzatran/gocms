<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Edit ForumPost</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <a href="${createLinkTo(dir:'')}">Home</a>
   </span>
   <span class="menuButton">
    <g:link action="list">ForumPost List</g:link>
   </span>
   <span class="menuButton">
    <g:link action="create">New ForumPost</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Edit ForumPost</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${forumPost}">
    <div class="errors">
     <g:renderErrors bean="${forumPost}" as="list" />
    </div>
   </g:hasErrors>
   <div class="prop">
    <span class="name">Id:</span>
    <span class="value">${forumPost?.id}</span>
   </div>
   <g:form controller="forumPost" method="post">
    <input type="hidden" name="id" value="${forumPost?.id}" />
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='title'>Title:</label>
        </td>
        <td class='value ${hasErrors(bean:forumPost,field:' title ',' errors ')}'>
         <input type="text" name='title' value="${forumPost?.title?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='text'>Text:</label>
        </td>
        <td class='value ${hasErrors(bean:forumPost,field:' text ',' errors ')}'>
         <textarea rows='5' cols='40' name='text'>${forumPost?.text?.encodeAsHTML()}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='created'>Created:</label>
        </td>
        <td class='value ${hasErrors(bean:forumPost,field:' created ',' errors ')}'>
         <g:datePicker name='created' value="${forumPost?.created}"></g:datePicker>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='approved'>Approved:</label>
        </td>
        <td class='value ${hasErrors(bean:forumPost,field:' approved ',' errors ')}'>
         <g:checkBox name="approved" value="${forumPost?.approved}" />
        </td>
       </tr>
       
       
       <tr class='prop'>
        <td class='name'>
         <label for='replies'>Replies:</label>
        </td>
        <td class='value ${hasErrors(bean:forumPost,field:' replies ',' errors ')}'>
         <ul>
          <g:each var='r' in='${forumPost?.replies?}'>
           <li>
            <g:link controller='forumPost' action='show' id='${r.id}'>${r}</g:link>
           </li>
          </g:each>
         </ul>
         <g:link controller='forumPost' params='["forumPost.id":forumPost?.id]' action='create'>Add ForumPost</g:link>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='forum'>Forum:</label>
        </td>
        <td class='value ${hasErrors(bean:forumPost,field:' forum ',' errors ')}'>
         <g:select optionKey="id" from="${Forum.list()}" name='forum.id' value="${forumPost?.forum?.id}"></g:select>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='parent'>Parent:</label>
        </td>
        <td class='value ${hasErrors(bean:forumPost,field:' parent ',' errors ')}'>
         <g:select optionKey="id" from="${ForumPost.list()}" name='parent.id' value="${forumPost?.parent?.id}"></g:select>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='author'>Author:</label>
        </td>
        <td class='value ${hasErrors(bean:forumPost,field:' author ',' errors ')}'>
         <g:select optionKey="id" from="${User.list()}" name='author.id' value="${forumPost?.author?.id}"></g:select>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="button">
      <g:actionSubmit value="Update" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
