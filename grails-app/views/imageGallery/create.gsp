<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Create ImageGallery</title>
  <g:extendedMCEHEader></g:extendedMCEHEader>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <g:link action="list">Listado de Galer&iacute;as</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Crear Galer&iacute;a</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${imageGallery}">
    <div class="errors">
     <g:renderErrors bean="${imageGallery}" as="list" />
    </div>
   </g:hasErrors>
   <g:form action="save" method="post">
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='name'>Nombre:</label>
        </td>
        <td class='value ${hasErrors(bean:imageGallery,field:' name ',' errors ')}'>
         <input type="text" name='name' value="${imageGallery?.name?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='description'>Descripci&oacute;n:</label>
        </td>
        <td class='value ${hasErrors(bean:imageGallery,field:' description ',' errors ')}'>
         <textarea rows='5' cols='40' name='description'>${imageGallery?.description?.encodeAsHTML()}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='creationDate'>Fecha:</label>
        </td>
        <td class='value ${hasErrors(bean:imageGallery,field:' creationDate ',' errors ')}'>
         <g:datePicker name='creationDate' value="${imageGallery?.creationDate}"></g:datePicker>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="formButton">
      <input type="submit" value="Create"></input>
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
