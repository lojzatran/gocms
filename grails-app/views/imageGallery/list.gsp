<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>ImageGallery List</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <g:link action="create">Crear Galer&iacute;a</g:link>
   </span>
  </div>
  <div class="body">
   <h1>ImageGallery List</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <table>
    <thead>
     <tr>
      <g:sortableColumn property="id" title="Id" />
      <g:sortableColumn property="name" title="Nombre" />
      <g:sortableColumn property="description" title="Descripci&oacute;n" />
      <g:sortableColumn property="creationDate" title="Fecha" />
      <th></th>
     </tr>
    </thead>
    <tbody>
     <g:each in="${imageGalleryList}">
      <tr>
       <td>${it.id?.encodeAsHTML()}</td>
       <td>${it.name?.encodeAsHTML()}</td>
       <td>${it.description?.encodeAsHTML()}</td>
       <td>${it.creationDate?.encodeAsHTML()}</td>
       <td class="actionButtons">
        <span class="actionButton">
         <g:link action="show" id="${it.id}">Ver</g:link>
        </span>
       </td>
      </tr>
     </g:each>
    </tbody>
   </table>
   <div class="paginateButtons">
    <g:paginate total="${ImageGallery.count()}" />
   </div>
  </div>
 </body>
</html>
