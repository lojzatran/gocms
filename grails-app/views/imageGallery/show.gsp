<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Show ImageGallery</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <a href="${createLinkTo(dir:'')}">Home</a>
   </span>
   <span class="menuButton">
    <g:link action="list">Listado de Galer&iacute;as</g:link>
   </span>
   <span class="menuButton">
    <g:link action="create">Crear Galer&iacute;a</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Galer&iacute;a</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <div class="dialog">
    <table>
     <tbody>
      <tr class="prop">
       <td valign="top" class="name">Id:</td>
       <td valign="top" class="value">${imageGallery.id}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Nombre:</td>
       <td valign="top" class="value">${imageGallery.name}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Descripci&oacute;n:</td>
       <td valign="top" class="value">${imageGallery.description}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Fecha:</td>
       <td valign="top" class="value">${imageGallery.creationDate}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Im&aacute;genes:</td>
       <td valign="top" style="text-align:left;" class="value">
        <ul>
         <g:each var="i" in="${imageGallery.images}">
          <li>
           <g:link controller="image" action="show" id="${i.id}">${i}</g:link>
          </li>
         </g:each>
        </ul>
       </td>
      </tr>
     </tbody>
    </table>
   </div>
   <div class="buttons">
    <g:form controller="imageGallery">
     <input type="hidden" name="id" value="${imageGallery?.id}" />
     <span class="button">
      <g:actionSubmit value="Edit" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </g:form>
   </div>
  </div>
 </body>
</html>
