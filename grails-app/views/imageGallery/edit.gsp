<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Edit ImageGallery</title>
  <g:extendedMCEHEader></g:extendedMCEHEader>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <g:link action="list">
     Listado de Galer&iacute;as
    </g:link>
   </span>
   <span class="menuButton">
    <g:link action="create">
     Crear Galer&iacute;a
    </g:link>
   </span>
  </div>
  <div class="body">
   <h1>
    Editar Galer&iacute;a
   </h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${imageGallery}">
    <div class="errors">
     <g:renderErrors bean="${imageGallery}" as="list" />
    </div>
   </g:hasErrors>
   <div class="prop">
    <span class="name">Id:</span>
    <span class="value">${imageGallery?.id}</span>
   </div>
   <g:form controller="imageGallery" method="post">
    <input type="hidden" name="id" value="${imageGallery?.id}" />
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='name'>Nombre:</label>
        </td>
        <td class='value ${hasErrors(bean:imageGallery,field:' name ',' errors ')}'>
         <input type="text" name='name' value="${imageGallery?.name?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='description'>
          Descripci&oacute;n:
         </label>
        </td>
        <td class='value ${hasErrors(bean:imageGallery,field:' description ',' errors ')}'>
         <textarea rows='5' cols='40' name='description'>${imageGallery?.description?.encodeAsHTML()}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='creationDate'>Fecha:</label>
        </td>
        <td class='value ${hasErrors(bean:imageGallery,field:' creationDate ',' errors ')}'>
         <g:datePicker name='creationDate' value="${imageGallery?.creationDate}"></g:datePicker>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='images'>
          Im&aacute;genes:
         </label>
        </td>
        <td class='value ${hasErrors(bean:imageGallery,field:' images ',' errors ')}'>
        <div style="height: 90px;overflow:auto;">
         <ul>
          <g:each var='i' in='${imageGallery?.images?}'>
           <li>
            <g:link controller='image' action='show' id='${i.id}'>${i}</g:link>
           </li>
          </g:each>
         </ul>
         </div>
         <g:link controller='image' params='["imageGallery.id":imageGallery?.id]' action='create'>Add Image</g:link>
        </td>
       </tr>

      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="button">
      <g:actionSubmit value="Update" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </div>
   </g:form>
   <h1>A&ntilde;adir im&aacute;genes a la galer&iacute;a</h1>
   <div id="message"></div>
   <script type="text/javascript">function showSpinner(){Effect.Appear('spinner_1',{duration:0.5,queue:'end'});Effect.Appear('spinner_2',{duration:0.5,queue:'end'});document.getElementById('s1').disabled=true;document.getElementById('s2').disabled=true;}</script>
   <script type="text/javascript">function hideSpinner(){Effect.Fade('spinner_1',{duration:0.5,queue:'end'});Effect.Fade('spinner_2',{duration:0.5,queue:'end'});}</script>

   <g:form name="addImages" url="[controller:'imageGallery',action:'addImages']" onComplete="hideSpinner();" update="message" enctype="multipart/form-data" method="post">
   <input type="hidden" name="idGallery" value="${imageGallery.id}"/>
   
    <div class="buttons"><input type="submit" id="s1" name="s1" value="Subir fotos" onclick="showSpinner();"/></div>
    <div style="display:none;" id="spinner_1"><img src="${createLinkTo(dir:'images',file:'spinner.gif')}" /> Subiendo archivos. Un momento por favor... </div>
    <g:each in="${(1..10)}" var="num">
     <h2>Imagen ${num}</h2>
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='title_${num}'>
          T&iacute;tulo:
         </label>
        </td>
        <td class='value'>
         <input type="text" name='title_${num}' />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='description_${num}'>
          Descripci&oacute;n:
         </label>
        </td>
        <td class='value'>
         <textarea rows='5' cols='40' name='description_${num}'></textarea>
        </td>
       </tr>
       <tr class="prop">
        <td class='name'>
         <label for='img_${num}'>
          Archivo (s&oacute;lo fotos JPEG!):
         </label>
        </td>
        <td class='value'>
         <input type="file" name="img_${num}" />
        </td>
       </tr>
      </tbody>
     </table>
    </g:each>
    <div class="buttons"><input id="s2" type="submit" name="s2" value="Subir fotos" onclick="showSpinner()"/></div>
    <div style="display:none;" id="spinner_2"><img src="${createLinkTo(dir:'images',file:'spinner.gif')}" /> Subiendo archivos. Un momento por favor... </div>    
   </g:form>
  </div>
 </body>
</html>
