<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Story List</title>
 </head>
 <body>
  <h1>Eventos para hoy [<g:format value="${hoy}"/>]</h1>
  <g:if test="${flash.message}">
   <div class="message">${flash.message}</div>
  </g:if>
  <g:each in="${eventList}" var="event">
   <h2>
    <a href="${createLink(controller:'events',action:'show',id:event.id)}">${event.title}</a>
   </h2>
   <h3>
    <g:format value="${event.date}" />
   </h3>
   ${event.intro}
  </g:each>
 </body>
</html>
