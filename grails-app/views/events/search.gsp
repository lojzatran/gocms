<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Story List</title>
  <script type="text/javascript" src="${createLinkTo(dir:'js/yui-cal',file:'yahoo-dom-event.js')}"></script>
  <script type="text/javascript" src="${createLinkTo(dir:'js/yui-cal',file:'calendar-min.js')}"></script>
  <script type="text/javascript" src="${createLinkTo(dir:'js/yui-cal',file:'date.js')}"></script>
  <link rel="stylesheet" type="text/css" href="${createLinkTo(dir:'js/yui-cal',file:'calendar.css')}" />
  <g:yuiCalendarUtils />
  <g:yuiCalendarHeader name="t0" />
  <g:yuiCalendarHeader name="t1" />
 </head>
 <body>
  <h1>
   B&uacute;squedas r&aacute;pidas:
  </h1>
  <ul>
   <li>
    <g:link controller="events" action="today">Eventos para hoy</g:link>
   </li>
   <li>
    <g:link controller="events" action="thisWeek">Eventos para esta semana</g:link>
   </li>
   <li>
    <g:link controller="events" action="thisMonth">Eventos para este mes</g:link>
   </li>
   <li>
    <g:link controller="events" action="thisYear">
     Eventos para este a&ntilde;o
    </g:link>
   </li>
  </ul>
  <h2>Buscar eventos por fecha</h2>
  <p>Elije las fechas entre las que quieres consultar los eventos:</p>
   <div class="yui-skin-sam">
    <g:form action="findEvents" method="post" name="findEvents">
     <div class="centered">
      <label for='fechasValue'>Desde:</label>
      <g:yuiCalendarBody name="t0" bean="${fechas}" />
     </div>
     <div class="centered">
      <label for='fechasValue'>Hasta:</label>
      <g:yuiCalendarBody name="t1" bean="${fechas}" />
     </div>
     <div class="centered">
      <input type="submit" value="Buscar eventos"></input>
     </div>
    </g:form>
   </div>
 </body>
</html>
