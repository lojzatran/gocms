<html>
 <head>
  <meta name="layout" content="main" />
   <title>Story List</title>
 </head>
 <body>
  <div class="body">
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <div class="story" id="story">
    <div class="storyTitle">
     <h2>${evento.title}</h2>
     <h3><g:format value="${evento.date}"/></h3>
    </div>
    ${evento.description}
   </div>
  </div>
 </body>
</html>
