<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Create Story</title>
  <g:extendedMCEHEader></g:extendedMCEHEader>
 </head>
 <body>
  <div class="body">
   <h1>Publicar Nota de Prensa</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${story}">
    <div class="errors">
     <g:renderErrors bean="${story}" as="list" />
    </div>
   </g:hasErrors>
   <g:form action="save" method="post">
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='titulo'>Titulo:</label>
        </td>
        <td class="value ${hasErrors(bean:story,field:' titulo ',' errors ')}">
        <textarea rows='2' name='titulo'>${story?.titulo}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='texto'>Texto:</label>
        </td>
        <td class="value ${hasErrors(bean:story,field:' texto ',' errors ')}">
         <textarea rows='20' name='texto'>${story?.texto}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='enlace'>Enlace:</label>
        </td>
        <td class="value ${hasErrors(bean:story,field:' enlace ',' errors ')}">
         <input type="text" class="textField"  name='enlace' value="${story?.enlace?.encodeAsHTML()}"></input>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='seccion'>Seccion:</label>
        </td>
        <td class="value ${hasErrors(bean:story,field:' seccion ',' errors ')}">
         <g:select optionKey="id" from="${Section.listOrderByNombre()}" name='seccion.id' value="${story?.seccion?.id}"></g:select>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="formButton">
      <input type="submit" value="Guardar"></input>
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
