  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Story List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="create">New Story</g:link></span>
        </div>
        <div class="body">
           <h1>Story List</h1>
            <g:if test="${flash.message}">
                 <div class="message">
                       ${flash.message}
                 </div>
            </g:if>
           <table>
             <thead>
               <tr>
                   
                                      
                        <th>Id</th>
                                      
                        <th>Titulo</th>
                                      
                        <th>Texto</th>
                                      
                        <th>Enlace</th>
                                      
                        <th>Autor</th>
                                      
                        <th>Fecha Alta</th>
                   
                   <th></th>
               </tr>
             </thead>
             <tbody>
               <g:each in="${storyList}">
                    <tr>
                       
                            <td>${it.id?.encodeAsHTML()}</td>
                       
                            <td>${it.titulo?.encodeAsHTML()}</td>
                       
                            <td>${it.texto?.encodeAsHTML()}</td>
                       
                            <td>${it.enlace?.encodeAsHTML()}</td>
                       
                            <td>${it.autor?.encodeAsHTML()}</td>
                       
                            <td>${it.fechaAlta?.encodeAsHTML()}</td>
                       
                       <td class="actionButtons">
                            <span class="actionButton"><g:link action="show" id="${it.id}">Show</g:link></span>
                       </td>
                    </tr>
               </g:each>
             </tbody>
           </table>
               <div class="paginateButtons">
                   <g:paginate total="${Story.count()}" />
               </div>
        </div>
    </body>
</html>
