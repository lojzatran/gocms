<g:formRemote url="[controller:'story',action:'inlineUpdate']" method="post" name="editStory" update="story" before="javascript:tinyMCE.triggerSave();">
 <div class="dialog">
  <table>
   <tbody>
    <tr class='prop'>
     <td class='name'>
      <label for='titulo'>Titulo:</label>
     </td>
     <td class="value ${hasErrors(bean:story,field:' titulo ',' errors ')}">
      <input type="text" class="textField" name='titulo' value="${session.story?.titulo?.encodeAsHTML()}"></input>
     </td>
    </tr>
    <tr class='prop'>
     <td class='name'>
      <label for='texto'>Texto:</label>
     </td>
     <td class="value ${hasErrors(bean:story,field:' texto ',' errors ')}">
      <textarea id="content_${session.story?.id}" rows='40' name='texto'>${session.story?.texto}</textarea>
     </td>
    </tr>
    <tr class='prop'>
     <td class='name'>
      <label for='enlace'>Enlace:</label>
     </td>
     <td class="value ${hasErrors(bean:story,field:' enlace ',' errors ')}">
      <input type="text" class="textField" name='enlace' value="${session.story?.enlace?.encodeAsHTML()}"></input>
     </td>
    </tr>
    <tr class='prop'>
     <td class='name'>
      <label for='seccion'>Seccion:</label>
     </td>
     <td class="value ${hasErrors(bean:story,field:' seccion ',' errors ')}">
      <g:select optionKey="id" from="${Section.listOrderByNombre()}" name='seccion.id' value="${session.story?.seccion?.id}"></g:select>
     </td>
    </tr>
   </tbody>
  </table>
 </div>
 <div class="buttons">
  <span class="formButton">
   <input type="submit" value="Guardar" />
  </span>
 </div>
 <g:activateEditor fieldID="content_${session.story?.id}"/>
</g:formRemote>