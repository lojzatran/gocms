  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Edit Story</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">Story List</g:link></span>
            <span class="menuButton"><g:link action="create">New Story</g:link></span>
        </div>
        <div class="body">
           <h1>Edit Story</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <g:hasErrors bean="${story}">
                <div class="errors">
                    <g:renderErrors bean="${story}" as="list" />
                </div>
           </g:hasErrors>
           <div class="prop">
	      <span class="name">Id:</span>
	      <span class="value">${story?.id}</span>
           </div>           
           <g:form controller="story" method="post" >
               <input type="hidden" name="id" value="${story?.id}" />
               <div class="dialog">
                <table>
                    <tbody>

                       
                       
				<tr class='prop'><td class='name'><label for='titulo'>Titulo:</label></td><td class='value ${hasErrors(bean:story,field:'titulo','errors')}'><input type="text" name='titulo' value="${story?.titulo?.encodeAsHTML()}"></input></td></tr>
                       
				<tr class='prop'><td class='name'><label for='texto'>Texto:</label></td><td class='value ${hasErrors(bean:story,field:'texto','errors')}'><textarea rows='5' cols='40' name='texto'>${story?.texto?.encodeAsHTML()}</textarea></td></tr>
                       
				<tr class='prop'><td class='name'><label for='enlace'>Enlace:</label></td><td class='value ${hasErrors(bean:story,field:'enlace','errors')}'><input type="text" name='enlace' value="${story?.enlace?.encodeAsHTML()}"></input></td></tr>
                       
				<tr class='prop'><td class='name'><label for='tags'>Tags:</label></td><td class='value ${hasErrors(bean:story,field:'tags','errors')}'><ul>
    <g:each var='t' in='${story?.tags?}'>
        <li><g:link controller='tagReference' action='show' id='${t.id}'>${t}</g:link></li>
    </g:each>
</ul>
<g:link controller='tagReference' params='["story.id":story?.id]' action='create'>Add TagReference</g:link>
</td></tr>
                       
				<tr class='prop'><td class='name'><label for='autor'>Autor:</label></td><td class='value ${hasErrors(bean:story,field:'autor','errors')}'><g:select optionKey="id" from="${User.list()}" name='autor.id' value="${story?.autor?.id}"></g:select></td></tr>
                       
				<tr class='prop'><td class='name'><label for='fechaAlta'>Fecha Alta:</label></td><td class='value ${hasErrors(bean:story,field:'fechaAlta','errors')}'><g:datePicker name='fechaAlta' value="${story?.fechaAlta}"></g:datePicker></td></tr>
                       
				<tr class='prop'><td class='name'><label for='seccion'>Seccion:</label></td><td class='value ${hasErrors(bean:story,field:'seccion','errors')}'><g:select optionKey="id" from="${Section.list()}" name='seccion.id' value="${story?.seccion?.id}"></g:select></td></tr>
                       
				<tr class='prop'><td class='name'><label for='intro'>Intro:</label></td><td class='value ${hasErrors(bean:story,field:'intro','errors')}'><input type="text" name='intro' value="${story?.intro?.encodeAsHTML()}"></input></td></tr>
                       
				<tr class='prop'><td class='name'><label for='comments'>Comments:</label></td><td class='value ${hasErrors(bean:story,field:'comments','errors')}'><ul>
    <g:each var='c' in='${story?.comments?}'>
        <li><g:link controller='comment' action='show' id='${c.id}'>${c}</g:link></li>
    </g:each>
</ul>
<g:link controller='comment' params='["story.id":story?.id]' action='create'>Add Comment</g:link>
</td></tr>
                       
                    </tbody>
                </table>
               </div>

               <div class="buttons">
                     <span class="button"><g:actionSubmit value="Update" /></span>
                     <span class="button"><g:actionSubmit value="Delete" /></span>
               </div>
            </g:form>
        </div>
    </body>
</html>
