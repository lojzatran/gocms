  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
          <meta name="layout" content="main" />
         <title>Show Story</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">Story List</g:link></span>
            <span class="menuButton"><g:link action="create">New Story</g:link></span>
        </div>
        <div class="body">
           <h1>Show Story</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <div class="dialog">
                 <table>
                   
                   <tbody>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Id:</td>
                              
                                    <td valign="top" class="value">${story.id}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Titulo:</td>
                              
                                    <td valign="top" class="value">${story.titulo}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Texto:</td>
                              
                                    <td valign="top" class="value">${story.texto}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Enlace:</td>
                              
                                    <td valign="top" class="value">${story.enlace}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Tags:</td>
                              
                                     <td  valign="top" style="text-align:left;" class="value">
                                        <ul>
                                            <g:each var="t" in="${story.tags}">
                                                <li><g:link controller="tagReference" action="show" id="${t.id}">${t}</g:link></li>
                                            </g:each>
                                        </ul>
                                     </td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Autor:</td>
                              
                                    <td valign="top" class="value"><g:link controller="user" action="show" id="${story?.autor?.id}">${story?.autor}</g:link></td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Fecha Alta:</td>
                              
                                    <td valign="top" class="value">${story.fechaAlta}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Seccion:</td>
                              
                                    <td valign="top" class="value"><g:link controller="section" action="show" id="${story?.seccion?.id}">${story?.seccion}</g:link></td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Intro:</td>
                              
                                    <td valign="top" class="value">${story.intro}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Comments:</td>
                              
                                     <td  valign="top" style="text-align:left;" class="value">
                                        <ul>
                                            <g:each var="c" in="${story.comments}">
                                                <li><g:link controller="comment" action="show" id="${c.id}">${c}</g:link></li>
                                            </g:each>
                                        </ul>
                                     </td>
                              
                        </tr>
                   
                   </tbody>
                 </table>
           </div>
           <div class="buttons">
               <g:form controller="story">
                 <input type="hidden" name="id" value="${story?.id}" />
                 <span class="button"><g:actionSubmit value="Edit" /></span>
                 <span class="button"><g:actionSubmit value="Delete" /></span>
               </g:form>
           </div>
        </div>
    </body>
</html>
