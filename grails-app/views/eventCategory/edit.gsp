  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Edit EventCategory</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">EventCategory List</g:link></span>
            <span class="menuButton"><g:link action="create">New EventCategory</g:link></span>
        </div>
        <div class="body">
           <h1>Edit EventCategory</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <g:hasErrors bean="${eventCategory}">
                <div class="errors">
                    <g:renderErrors bean="${eventCategory}" as="list" />
                </div>
           </g:hasErrors>
           <div class="prop">
	      <span class="name">Id:</span>
	      <span class="value">${eventCategory?.id}</span>
           </div>           
           <g:form controller="eventCategory" method="post" >
               <input type="hidden" name="id" value="${eventCategory?.id}" />
               <div class="dialog">
                <table>
                    <tbody>

                       
                       
				<tr class='prop'><td class='name'><label for='events'>Events:</label></td><td class='value ${hasErrors(bean:eventCategory,field:'events','errors')}'><ul>
    <g:each var='e' in='${eventCategory?.events?}'>
        <li><g:link controller='event' action='show' id='${e.id}'>${e}</g:link></li>
    </g:each>
</ul>
<g:link controller='event' params='["eventCategory.id":eventCategory?.id]' action='create'>Add Event</g:link>
</td></tr>
                       
				<tr class='prop'><td class='name'><label for='name'>Name:</label></td><td class='value ${hasErrors(bean:eventCategory,field:'name','errors')}'><input type="text" name='name' value="${eventCategory?.name?.encodeAsHTML()}"/></td></tr>
                       
                    </tbody>
                </table>
               </div>

               <div class="buttons">
                     <span class="button"><g:actionSubmit value="Update" /></span>
                     <span class="button"><g:actionSubmit value="Delete" /></span>
               </div>
            </g:form>
        </div>
    </body>
</html>
