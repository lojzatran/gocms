<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="
            http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/09/sitemap.xsd">
<url>
  <loc>http://groovy.org.es/</loc>
  <priority>1</priority>
  <changefreq>hourly</changefreq>
</url>
<g:each in="${storyList}" var="item">
<url>
  <loc>http://groovy.org.es/home/story/${item.id}</loc>
  <priority>0.5</priority>
  <changefreq>daily</changefreq>
</url>
</g:each>
</urlset>