<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Create Section</title>
 </head>
 <body>
  <div class="body">
   <div class="storyTitle">
    <h2>${block.name}</h2>
   </div>
   <g:if test="${session.user && session.user.roles.contains('editor')}">
   <a href="${createLink(controller:'block',action:'edit',id:block.id)}">Editar</a>
   </g:if>
   ${block.contents}
  </div>
 </body>
</html>
