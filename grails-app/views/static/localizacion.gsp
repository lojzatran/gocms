  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Create Section</title>  
<!-- YAHOO MAPS -->
<script type="text/javascript"
src="http://api.maps.yahoo.com/ajaxymap?v=3.7&appid=OW9F7N3V34HK3mI0cKZHpsAU9Zi2xs_Jz9UTICj3cNh_dpVXaQ0OK.6.10KdMkhZIaA-">
</script>
<!-- FIN YAHOO MAPS -->
    </head>
    <body>
        <div class="body">
        <h1>Situaci&oacute;n Geogr&aacute;fica</h1>
<p>Serranillos del Valle se encuentra localizado al sur oeste de la Comunidad de Madrid. Se enmarca dentro de la Mancomunidad del Sur Oeste, una entidad supramunicipal donde se integran los municipios de Arroyomolinos, Casarrubuelos, Cubas de la Sagra, Gri&#241;&#243;n, Humanes de Madrid, Moraleja de Enmedio, Serranillos del Valle, Torrej&#243;n de la Calzada y Torrej&#243;n de Velasco.</p>
<p>La Mancomunidad ofrece a todos sus vecinos, entre otros, los servicios de protecci&#243;n civil y extinci&#243;n de incendios, servicios sociales y reinserci&#243;n social, recogida y tratamiento de residuos urbanos e industriales, limpieza viaria, actividades culturales, deportivas y de tiempo libre, mantenimiento de parques y jardines y asesoramiento jur&#237;dico y de consumo.</p>
<p>La distancia desde Serranillos del Valle a la Puerta del Sol, se puede estimar en unos 32 Km. Se encuentra a 2 Km del final de la Comunidad de Madrid, lindando con las poblaciones de Gri&#241;&#243;n, Cubas, Batres y Carranque (este &#250;ltimo municipio pertenece ya a Castilla la Mancha)</p>

<h2>Accesos por carretera</h2>
<p>Se puede acceder por la A-42 (Madrid - Toledo), desde Torrej&#243;n de la Calzada, por la N-V (Madrid &#8211; Badajoz) desde Navalcarnero o por la M- 405 desde Humanes- Fuenlabrada.</p>
<p>Otras alternativas pasan por la M- 407 desde Loranca o desde la M-50.</p>
<p>La nueva carretera de peaje AP- 41 (Madrid- Toledo), tiene una salida en Serranillos del Valle, y permite enlazar con la M- 50 en todas sus direcciones a la altura de M&#243;stoles o enlazar con la M- 40 con todas sus opciones de acceso a Madrid Centro o a las propias de la M- 40.</p>

<h2>Transporte P&#250;blico</h2>

Cuenta con dos l&#237;neas de autob&#250;s de transporte p&#250;blico:
<ul>
<li><strong>468 (Getafe- Serranillos del Valle):</strong> Ofrece dos posibilidades, desde Getafe a Serranillos del Valle y desde Getafe a Los Llanos. Esta &#250;ltima, da lugar a una l&#237;nea interurbana que une el centro del pueblo con otras zonas mas cercanas a la M- 415.
La 468 adem&#225;s, une Serranillos del Valle con Fuenlabrada &#8211;Tren de Cercan&#237;as y Metrosur.
Adem&#225;s, esta l&#237;nea, los viernes y s&#225;bado noche y las v&#237;speras de festivos ofrece servicio nocturno desde Fuenlabrada hasta Serranillos del Valle</li>
<li><strong>460 (Madrid- Parla- Batres):</strong> Une Serranillos del Valle con la capital y permite acceder a Parla- Tren de Cercan&#237;as.</li>

<p>Se pueden consultar los horarios de esta l&#237;neas desde el apartado Transportes de este misma web.</p>  

<h2>Serranillos en el Mapa</h2>
<p>(El mapa permite arrastrar y soltar con el rat&oacute;n)</p>
<div class="centered">
<div id="map" style="width: 650px; height: 550px; border: 1px solid #ccc;"></div>
<script type="text/javascript">
    var direccion = "Plaza de España, Serranillos del Valle, Madrid, Spain";
	var map = new YMap(document.getElementById('map'));
	map.setMapType(YAHOO_MAP_REG);
	
	map.drawZoomAndCenter(direccion, 7);
	var punto = new YGeoPoint( 40.209557, -3.884354 );
	map.addMarker(punto);
</script>
</div>      
        </div>
    </body>
</html>
