  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Create Section</title>         
    </head>
    <body>
        <div class="body">
<h1>Aviso Legal.</h1>
<p>
Groovy.org.es o los representantes legales de la misma informa por medio de este escrito a los usuarios de los servicios prestados (en adelante "los usuarios"), ya sea por medio de su sitio web o por otros que puedan venir en el futuro  acerca de las cuestiones legales referentes a Groovy.org.es.
</p><p>
Al leer este documento, el usuario esta aceptándolo como contrato que rige la relación entre el mismo y Groovy.org.es. Navegar por el sitio de Groovy.org.es sin haberlo leido es responsabilidad del usuario y no exime de su cumplimiento.
</p>

<h1>Exención de responsabilidad por los contenidos y servicios prestados.</h1>
<p>
Groovy.org.es ofrece sus servicios de forma gratuita y no podrá ser requerido a seguir haciéndolo en un futuro, ya sea por cuestiones técnicas, personales o empresariales. Así mismo se crearan y suprimirán servicios a lo largo del tiempo, según los estime Groovy.org.es, sin que el usuario pueda requerir lo contrario siempre que se ajuste a la legalidad vigente.
</p><p>

Groovy.org.es incluye en su Website una serie de contenido electrónico. Este contenido no es propiedad ni responsabilidad de Groovy.org.es, sino de los firmantes de la misma, siendo aplicable la licencia que le hayan otrogado los mismos.
</p><p>
Groovy.org.es se reserva el derecho de suprimir todo aquel contenido considerado dañido, difamatorio o ilegal sin que el usuario tenga derecho a reclamaciones legales.
</p>

<h1>Propiedad intelectual.</h1>
<p>
Los textos (informaciones, conceptos, opiniones y otros análogos) y elementos gráficos (diseño, logos, código fuente y otros análogos) que constituyen Groovy.org.es son propiedad de los autores del mismo, y mantienen la licencia designada por ellos.
</p><p>
El formato, diseño, logo, y demás componentes del sitio Web son propiedad de Groovy.org.es, y pueden ser usados libremente, aunque Groovy.org.es se reserva a exigir el abandono de su uso si lo considera pertinente.
</p><p>
Se permite expresamente los enlaces desde el exterior al sitio de Groovy.org.es, pero en caso de usar una imagen o logo para el mismo, se deberá almacenar dicha imagen en el servidor del sitio que mantienen el enlace, no pudiendo usar directamente la imagen almacenada en el servidor de Groovy.org.es.
</p><p>
Los derechos no expresamente concedidos anteriormente quedan reservados a Groovy.org.es.
</p><p>
Todos los nombres comerciales, marcas o signos distintivos, logotipos, símbolos, marcas mixtas, figurativas o nominativas que aparecen en Groovy.org.es son propiedad de sus respectivos propietarios.

</p>
        </div>
    </body>
</html>
