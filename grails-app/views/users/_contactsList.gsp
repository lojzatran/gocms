<div>
<g:if test="${session.contactsCache && session.contactsCache.nicks.size()}">
 <p>
  Estos son los contactos que hemos obtenido. Puedes desactivar aquellos a los que no quieras enviar la recomendaci&oacute;n:
 </p>
 <div class="buttons">
  <span class="formButton" id="submitButton">
   <input type="submit" name="s" value="Enviar Recomendaci&oacute;n" />
  </span>
 </div><!--  checked="checked" -->
 <g:each in="${session.contactsCache.nicks}" var="nick">
  <input type="checkbox" name="nick" value="${nick}" />
  ${nick}
  <br />
 </g:each>
 <div class="buttons">
  <span class="formButton" id="submitButton">
   <input type="submit" name="s" value="Enviar Recomendaci&oacute;n" />
  </span>
 </div>
 </g:if>
 <g:else>
 <div class="message">
 No hemos encontrado ning&uacute;n contacto en tu cuenta.
 </div>
 </g:else>
</div>