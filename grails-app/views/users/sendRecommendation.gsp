<html>
 <head>
  <meta name="layout" content="main" />
   <title>Importa contactos de IM</title>
 </head>
 <body>
  <div class="body">
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
    <h1>Recomienda esta historia a tus contactos.</h1>
    <p>
     <img src="${createLinkTo(dir:'images',file:'contacts_big.gif')}" alt="Recomienda esta historia a tus contactos." title="Recomienda esta historia a tus contactos." style="float:left; margin: 4px;" />
     Hemos enviado la historia a tus contactos seleccionados. Muchas gracias por usar el servicio.
     <div class="centered">
     <a href="${createLink(controller:'home',action:'story',id:session.recommended.id)}" title="Volver a ${session.recommended.titulo}">Volver a "${session.recommended.titulo}"</a>
     </p>
  </div>
 </body>
</html>