<html>
 <head>
  <meta name="layout" content="main" />
   <title>Recordatorio de clave</title>
 </head>
 <body>
  <div class="body">
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
    <h1>Recordatorio de contrase&ntilde;a</h1>
    <p>Si has olvidado tu clave de acceso, introduce aqu&#237; tu direcci&#243;n de correo electr&#243;nico para que podamos envi&#225;rtela:</p>
    <g:form action="passwordReminder" name="r" class="centered">
    E-mail: <br /><input type="text" name="email" value=""/> <br/>
    <input type="submit" name="s" value="Enviar clave por e-mail"/>
    </g:form>
  </div>
 </body>
</html>