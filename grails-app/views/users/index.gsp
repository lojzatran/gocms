<html>
 <head>
  <meta name="layout" content="main" />
   <title>Importa contactos de IM</title>
 </head>
 <body>
  <div class="body">
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:form action="imcontacts">
   <h1>Recomienda esta historia a tus contactos. </h1>
    <p>Introduce tu email y contrase&#241;a de MSN Messenger, GMail, Yahoo Messenger, AOL, etc y enviaremos una recomendaci&#243;n
    de "${session.recommended.title}" a tus contactos de tu parte. No te preocupes, podr&#225;s seleccionar a cuales quieres enviarselo y a cuales no.</p>
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='nombre'>Email:</label>
        </td>
        <td class='value'>
         <input type="text" name='eml' />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='nombre'>Clave:</label>
        </td>
        <td class='value'>
         <input type="text" name='pwd' />
        </td>
       </tr>
      </tbody>
     </table>
     <script type="text/javascript">function showSpinner(){Effect.Appear('spinner',{duration:0.5,queue:'end'});}</script>
     <script type="text/javascript">function hiddeSpinner(){Effect.Fade('spinner',{duration:0.5,queue:'end'});}</script>
     <div class="buttons">
      <span class="formButton" id="submitButton">
       <g:submitToRemote action="imcontacts" value="Aceptar" before="showSpinner();" onComplete="hiddeSpinner();" update="contactsList" />
      </span>
     </div>
     <div style="display:none;" id="spinner">
      <img src="${createLinkTo(dir:'images',file:'spinner.gif')}" />
      Cargando tu lista de contactos. Un momento...
     </div>
     <p><b>Aviso:</b> No somos spammers. Tus datos de acceso no ser&#225;n almacenados de ninguna manera, y por tanto se olvidar&#225;n una vez enviada tu recomendaci&#243;n. Groovy.org.es te garantiza que no utilizar&#225; tu email ni tu clave para nada m&#225;s, nunca.</p>
   </g:form>
   <div id="contactsList"></div>
  </div>
 </body>
</html>