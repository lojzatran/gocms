<html>
 <head>
  <meta name="layout" content="main" />
 </head>
 <body>
  <div class="body">
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <h1>Formulario de contacto</h1>
   <p>Escribe tu nombre y tus datos de contacto para que podamos contestarte lo antes posible.</p>
   <g:form action="sendContactRequest" method="post">
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='author'>Nombre:</label>
        </td>
        <td class="value">
         <input type="text" name="nombre" value="${session.user?.nombre}"/>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='author'>Tel&eacute;fono:</label>
        </td>
        <td class="value">
         <input type="text" name="telefono" value=""/>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='author'>Correo electr&oacute;nico:</label>
        </td>
        <td class="value">
         <input type="text" name="email" value="${session.user?.email}"/>
        </td>
       </tr>

       <tr class='prop'>
        <td class='name'>
         <label for='author'>Mensaje:</label>
        </td>
        <td class="value">
         <textarea name="mensaje" rows="15"></textarea>
        </td>
       </tr>       
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="formButton">
      <input type="submit" value="Enviar"></input>
     </span>
    </div>
    </g:form>
  </div>
 </body>
</html>