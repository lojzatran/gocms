<html>
 <head>
  <meta name="layout" content="main" />
 </head>
 <body>
  <div class="body">
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <h1>Formulario de contacto</h1>
   <p>El mensaje se ha enviado correctamente. En breve nos pondremos en contacto contigo.</p>
   <p>Gracias por usar este servicio</p>

  </div>
 </body>
</html>