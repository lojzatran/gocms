<html>
 <head>
  <meta name="layout" content="main" />  <title>Create User</title>
 </head>
 <body>
  <div class="body">
   <h1>Perfil de usuario</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${user}">
    <div class="errors">
     <g:renderErrors bean="${user}" as="list" />
    </div>
   </g:hasErrors>
   <g:form action="saveProfile" method="post">
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='nombre'>Nombre:</label>
        </td>
        <td class="value ${hasErrors(bean:user,field:' nombre ',' errors ')}">
         <input class="textField" type="text" maxlength='15' name='nombre' value="${user?.nombre?.encodeAsHTML()}"></input>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='clave'>Clave:</label>
        </td>
        <td class="value ${hasErrors(bean:user,field:' clave ',' errors ')}">
         <input class="textField" type="password" maxlength='12' name='clave' value="${user?.clave?.encodeAsHTML()}"></input>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='clave'>Confirma tu clave:</label>
        </td>
        <td class="value ${hasErrors(bean:user,field:' clave ',' errors ')}">
         <input class="textField" type="password" maxlength='12' name='clave2' value="${user?.clave?.encodeAsHTML()}"></input>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label class="textField" for='email'>Email:</label>
        </td>
        <td class="value ${hasErrors(bean:user,field:' email ',' errors ')}">
         <input class="textField" type="text" name='email' value="${user?.email?.encodeAsHTML()}"></input>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='web'>Web:</label>
        </td>
        <td class="value ${hasErrors(bean:user,field:' web ',' errors ')}">
         <input class="textField" type="text" name='web' value="${user?.web?.encodeAsHTML()}"></input>
        </td>
       </tr>       
       <tr class='prop'>
        <td class='name'>
         <label for='perfil'>Tu perfil :</label>
        </td>
        <td class="value ${hasErrors(bean:user,field:' perfil ',' errors ')}">
         <textarea rows='15' cols='40' name='perfil'>${user?.perfil?.encodeAsHTML()}</textarea>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="formButton">
      <input type="submit" value="Ok!"></input>
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
