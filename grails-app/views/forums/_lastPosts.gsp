<h2>&Uacute;ltimos mensajes
</h2>
<h3>mensajes del foro</h3>
<ul>
 <g:each in="${ForumPost.listOrderById(max:10, order:'desc')}" var="hilo">
  <g:if test="${hilo.forum}">
   <g:if test="${!hilo.forum.moderated || hilo.approved}">
    <li>
     <a href="${createLink(controller:'forums',action:'thread',id:hilo.id)}">${hilo} (${hilo.author})</a>
     (${hilo.forum.name})
    </li>
   </g:if>
  </g:if>
  <g:elseif test="${!hilo.parent.forum.moderated || hilo.approved}">
   <li>
    <a href="${createLink(controller:'forums',action:'thread',id:hilo.parent.id)}">${hilo} (${hilo.author})</a>
    (${hilo.parent.forum.name})
   </li>
  </g:elseif>
 </g:each>
</ul>
