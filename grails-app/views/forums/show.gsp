<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <g:tinyMCEHEader />
  
  <title>Forum List</title>
 </head>
 <body>
  <div class="body">
<!-- google_ad_section_start -->
   <h1>Foro: ${session.forum.name}</h1>
<!-- google_ad_section_end -->   
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <script type="text/javascript">function showSpinner(){Effect.Appear('spinner',{duration:0.5,queue:'end'});}</script>
   <script type="text/javascript">function hiddeSpinner(){Effect.Fade('spinner',{duration:0.5,queue:'end'});}</script>
   <div class="centered">
   <a href="javascript:void(0);" onclick="showSpinner();${remoteFunction(action:'newPostForm',update:'newPost',onComplete:'hiddeSpinner();')}" title="Nuevo Post">Publicar un post nuevo.</a>
   <div style="display:none;" id="spinner"><img src="${createLinkTo(dir:'images',file:'spinner.gif')}" /> Un momento... </div>
   </div>
   <div id="newPost"></div>
   <!-- google_ad_section_start -->
   <table>
   <th>T&iacute;tulo</th>
   <th>Visitas</th>
   <th>Respuestas</th>
   <th>Fecha</th>
   <g:each in="${threads}" var="hilo">
   <tr>
   <td width="50%" style="text-align:left;"><a href="${createLink(action:'thread',id:hilo.id)}">${hilo} (${hilo.author})</a></td>
   <td>${hilo.views}</td>
   <td>${hilo.replies.size()}</td>
   <td style="text-align:left;"> <g:howMuchTimeAgo value="${hilo.created}" /></td>
   </tr>
   </g:each>
   </table>
   <!-- google_ad_section_end -->
   </div>
 </body>
</html>
