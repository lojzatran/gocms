<g:if test="${session.user}">
 <div id="response"></div>
 <g:form action="savePost">
 <script type="text/javascript">function showLoading(){var E = document.getElementById('submitButton'); E.startWaiting();}</script> 
  <div class="dialog">
   <div class="value">
    T&#237;tulo:
    <textarea id='title' rows='2' name='title'></textarea>
   </div>
   <div class="value">
    Comentario:
    <textarea id='text' rows='10' name='text' id='text'></textarea>
   </div>
  </div>
  <div class="buttons">
   <span class="formButton" id="submitButton">
    <g:submitToRemote 
    action="savePost" 
    value="Enviar" 
    before="tinyMCE.triggerSave();" 
    update="response"/>
   </span>
  </div>
  <g:activateEditor fieldID="title" />
  <g:activateEditor fieldID="text" />
 </g:form>
</g:if>
<g:else>
 <div class="message">
  Tienes que
  <a href="${createLink(controller:'home',action:'register')}">estar registrado</a>
  para iniciar sesi&#243;n y poder publicar tus mensajes en el foro.
 </div>
</g:else>