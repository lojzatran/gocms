<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Forum List</title>
 </head>
 <body>
  <div class="body">
   <h1>Foros</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
Bienvenido a los foros del Ayuntamiento de Serranillos del Valle. En este espacio podr&#225;s realizar consultas y comentarios sobre el funcionamiento de esta web o sobre cualquier otra cuesti&#243;n relacionada con tu municipio. Te comunicamos que cualquier mensaje que quieras publicar ser&#225; sometido a aprobaci&#243;n por un moderador antes de ser visible por todos. 
   <table>
    <thead>
     <tr>
      <g:sortableColumn property="name" title="Nombre" />
      <th width="2%">Hilos</th>
     </tr>
    </thead>
    <tbody>
<!-- google_ad_section_start -->
     <g:each in="${forumList}" var="foro">
      <tr>
       <td style="text-align: left;">
         <g:link action="show" id="${foro.id}">${foro.name?.encodeAsHTML()}</g:link>
         <p>${foro.description}</p>
       </td>
       <td>
       <g:if test="${foro.moderated}">
       ${ForumPost.countByForumAndApproved(foro,true)}
       </g:if>
       <g:else>
       ${foro?.threads.size()}
       </g:else>       
       </td>
      </tr>
     </g:each>
<!-- google_ad_section_end -->     
    </tbody>
   </table>
   <div class="paginateButtons">
    <g:paginate total="${Forum.count()}" />
   </div>
  </div>
 </body>
</html>
