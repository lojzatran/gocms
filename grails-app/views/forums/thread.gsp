<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="layout" content="main" />
		<title>Forum List</title>
		<g:tinyMCEHEader />
	</head>
	<body>
		<div class="body">
        <g:if test="${flash.message}">
         <div class="message">${flash.message}</div>
        </g:if>  
			<h1>${thread.forum.name}</h1>
			<div class="storyTitle">
				<h2>${thread.title}</h2>
				${thread.author} - <g:howMuchTimeAgo value="${thread.created}" />
			</div>
			<div class="threadText">
			<!-- google_ad_section_start -->
			${thread.text}
			<!-- google_ad_section_end -->
			</div>
			<g:each in="${replies}" var="c">
				<div class="comment">
					<div class="storyTitle">
						<h2>${c.title}</h2>
						${c.author?.nombre.encodeAsHTML()} -
						<g:howMuchTimeAgo value="${c.created}" />
					</div>
                    <div class="threadText">
     <!-- google_ad_section_start -->
					${c.text}
     <!-- google_ad_section_end -->
                    </div>
				</div>
			</g:each>
			<script type="text/javascript">function showSpinner(){Effect.Appear('spinner_${thread.id}',{duration:0.5,queue:'end'});}</script>
			<script type="text/javascript">function hiddeSpinner(){Effect.Fade('spinner_${thread.id}',{duration:0.5,queue:'end'});}</script>
			<a href="javascript:void(0);" onclick="showSpinner();${remoteFunction(action:'replyForm',params:'\'parentID=' + thread.id + '\' ',update:'reply_to_' + thread.id,onComplete:'hiddeSpinner();')}" title="Responder">Responder al hilo</a>
            | <a href="${createLink(action:'show',id:thread.forum.id)}">Volver al foro "${thread.forum.name}"</a>
            | <a href="${createLink(action:'index')}">Volver a los foros</a>
			<div style="display:none;" id="spinner_${thread.id}"><img src="${createLinkTo(dir:'images',file:'spinner.gif')}" /> Un momento... </div>
			<div id="reply_to_${thread.id}"></div>
		</div>
	</body>
</html>
