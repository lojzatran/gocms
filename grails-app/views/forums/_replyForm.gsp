<g:if test="${session.user}">
 <h1>Responder al post</h1>
 <div id="response_${parent.id}"></div>
 <g:form action="saveReply">
 <script type="text/javascript">function showLoading_${parent.id}(){var E = document.getElementById('submitButton_${parent.id}'); E.startWaiting();}</script>
 
  <input type="hidden" name="parentID" value="${parent.id}" />
  <div class="dialog">
   <div class="value">
    T&#237;tulo:
    <textarea id='title_${parent.id}' rows='2' name='title_${parent.id}'>Re: ${parent.title}</textarea>
   </div>
   <div class="value">
    Comentario:
    <textarea id='text_${parent.id}' rows='10' name='text_${parent.id}' id='text'></textarea>
   </div>
  </div>
  <div class="buttons">
   <span class="formButton">
    <g:submitToRemote 
    action="saveReply" 
    value="Enviar" 
    id="submitButton_${parent.id}"
    before="tinyMCE.triggerSave();" 
    update="response_${parent.id}"/>
   </span>
  </div>
  <g:activateEditor fieldID="title_${parent.id}" />
  <g:activateEditor fieldID="text_${parent.id}" />
 </g:form>
</g:if>
<g:else>
 <div class="message">
  Tienes que
  <a href="${createLink(controller:'home',action:'register')}">estar registrado</a>
  para iniciar sesi&#243;n y poder publicar tus mensajes en el foro.
 </div>
</g:else>