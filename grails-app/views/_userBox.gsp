	<g:if test="${session.user}">
        <h1 class="first">&#161;Hola ${session.user?.nombre?.encodeAsHTML()}!</h1>
        <dl class="nav3-grid">
          <dt><a href="${createLink(controller:'home',action:'logout')}" title="Cerrar Sesion">Salir</a></dt>
          <dt><a href="${createLink(controller:'users',action:'profile')}" title="Cerrar Sesion">Mi perfil</a></dt>
        </dl>   
	</g:if>
	<g:else>
	    <h1>Entrar</h1><br />    
		<div class="loginform">
			<g:if test="${msg}">
				<div class="errors">${msg}</div>
			</g:if>
			<g:form action="login" method="post" name="loginForm">
				<div id="userBox">
					<fieldset>
						<p>
							<label for="name" class="top">Usuario:</label><br />
							<input type="text" name="name" id="name" tabindex="1" class="field" value="" />
						</p>
						<p>
							<label for="passwd" class="top">Contrase&#241;a:</label><br />
							<input type="password" name="passwd" id="passwd" tabindex="2" class="field" value="" />
						</p>
						<p>
							<input type="checkbox" name="recordar" id="recordar" value="si" class="checkbox" tabindex="3" size="1" />
							<label for="recordar" class="right">Recordarme en este equipo</label>
						</p>
						<p>
							<g:submitToRemote tabindex="4" action="login" controller="home" update="userBox" value="ENTRAR" class="button" />
						</p>
						<p>
							<a href="${createLink(controller:'home',action:'register')}">Registro</a> | 
							<a href="${createLink(controller:'users',action:'passwordReminderForm')}">Olvid&eacute; mi clave</a>
						</p>
					</fieldset>
				</div>
			</g:form>
		</div>
	</g:else>