<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="cache-control" content="no-cache" />
  <meta http-equiv="expires" content="3600" />
  <meta name="revisit-after" content="2 days" />
  <meta name="robots" content="index,follow" />
  <meta name="publisher" content="${IWConfig.list()[0].siteOwner}" />
  <meta name="copyright" content="${IWConfig.list()[0].siteOwner}" />
  <meta name="author" content="${IWConfig.list()[0].siteAuthor}. Basado en un diseño original de G. Wolfgang www.1-2-3-4.info" />
  <meta name="distribution" content="global" />
  <meta name="description" content="${IWConfig.list()[0].siteDescription}" />
  <meta name="keywords" content="${IWConfig.list()[0].siteKeywords}" />
  <link rel="alternate" type="application/rss+xml" title="Actualidad en ${IWConfig.list()[0].siteTitle}" href="${createLink(controller:'feed',action:'rssStories')}" />
  <link rel="stylesheet" type="text/css" media="screen,projection,print" href="${createLinkTo(dir:'css',file:'main.css')}" />
  <link rel="stylesheet" type="text/css" media="screen,projection,print" href="${createLinkTo(dir:'css',file:'style.css')}" />

  <link rel="icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" />
  <g:layoutHead />
  <g:javascript library="application" />
  <g:javascript library="scriptaculous" />
  <g:javascript library="prototype" />
  
  <title>${IWConfig.list()[0].siteTitle}</title>
</head>


<body onload="${pageProperty(name:'body.onload')}" onunload="${pageProperty(name:'body.onunload')}">
<div id="page-container"> 
<g:render template="/header"/>
<g:render template="/rightCol"/>
 
 <div id="content">
  <div class="padding">
  <g:layoutBody />    
  </div>
 </div>
 
 <div id="prefooter">
   <br />
   <p><a href="#">Green Grass in your browser...</a></p>
   <p><a href="#">My first CSS Template</a></p>
   <p><a href="#">Try sNews 1.4!</a></p>
 </div>
 
 <div id="footer">
  <a href="#">RSS Feed</a> | 
  <a href="#">Contact</a> | 
  <a href="#">Accessibility</a> | 
  <a href="#">Products</a> | 
  <a href="#">Disclaimer</a> |
  <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> and <a href="http://validator.w3.org/check?uri=referer">XHTML</a>
 <br />
 Copyright &copy; 2006 Green Grass - Design: <a href="http://www.free-css-templates.com">David Herreman</a>
 </div>
</div>
</body>
</html>