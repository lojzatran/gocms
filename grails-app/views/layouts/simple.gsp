<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="cache-control" content="no-cache" />
  <meta http-equiv="expires" content="3600" />
  <meta name="revisit-after" content="2 days" />
  <meta name="robots" content="index,follow" />
  <meta name="publisher" content="${IWConfig.list()[0].siteOwner}" />
  <meta name="copyright" content="${IWConfig.list()[0].siteOwner}" />
  <meta name="author" content="${IWConfig.list()[0].siteAuthor}. Basado en un diseño original de G. Wolfgang www.1-2-3-4.info" />
  <meta name="distribution" content="global" />
  <meta name="description" content="${IWConfig.list()[0].siteDescription}" />
  <meta name="keywords" content="${IWConfig.list()[0].siteKeywords}" />
  <link rel="alternate" type="application/rss+xml" title="Actualidad en ${IWConfig.list()[0].siteTitle}" href="${createLink(controller:'feed',action:'rssStories')}" />
  <link rel="icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.ico')}" />
  <g:layoutHead />
  
  <title>${IWConfig.list()[0].siteTitle}</title>
</head>

<!-- Global IE fix to avoid layout crash when single word size wider than column width -->
<!--[if IE]><style type="text/css"> body {word-wrap: break-word;}</style><![endif]-->

<body onload="${pageProperty(name:'body.onload')}" onunload="${pageProperty(name:'body.onunload')}">
  <div class="page-container">
    <g:render template="/header"/>
    <!-- B. MAIN -->
    <div class="main">
      <!-- B.1 MAIN CONTENT -->
      <div class="main-content">
      <g:layoutBody />                       
      </div>
      <g:render template="/rightCol"/>          
    </div>
    <!-- C. FOOTER AREA -->      
    <div class="footer">
      <p>Copyright &copy; Ayuntamiento de Serranillos del Valle | Todos los derechos reservados.</p>
      <p class="credits">
      Desarrollo: <a href="http://www.imaginaworks.com" title="ImaginaWorks Software Factory">ImaginaWorks Software Factory</a> |  
      <a href="http://validator.w3.org/check?uri=referer" title="XHTML Valido">W3C XHTML 1.0</a> | 
      <a href="http://jigsaw.w3.org/css-validator/" title="CSS Valido">W3C CSS 2.0</a> |
      El tiempo cortes&iacute;a de <a href="http://www.tutiempo.net">Tutiempo.net</a>
      </p>
    </div>      
  </div> 
</body>
</html>