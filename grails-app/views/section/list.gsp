<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="layout" content="main" />
		<title>Section List</title>
	</head>
	<body>
		<div class="nav">
			<span class="menuButton">
				<g:link action="create">
					Nueva Secci&oacute;n
				</g:link>
			</span>
		</div>
		<div class="body">
			<g:if test="${flash.message}">
				<div class="message">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
						<th>Id</th>
						<th>Nombre</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<g:each in="${sectionList}">
						<tr>
							<td>${it.id?.encodeAsHTML()}</td>
							<td>${it.nombre?.encodeAsHTML()}</td>
							<td class="actionButtons">
								<span class="actionButton">
									<g:link action="show" id="${it.id}">Ver</g:link>
								</span>
							</td>
						</tr>
					</g:each>
				</tbody>
			</table>
			<div class="paginateButtons">
				<g:paginate total="${Section.count()}" />
			</div>
		</div>
	</body>
</html>
