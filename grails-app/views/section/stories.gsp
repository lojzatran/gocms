<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Story List</title>
 </head>
 <body>
  <div class="body">
   <h1>${section.nombre}</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:each in="${Story.findAllBySeccion(section,[sort:'id',order:'desc'])}">
    <div class="story">
     <div class="storyTitle">
      <h2>${it.titulo?.encodeAsHTML()}</h2>
      ${it.autor?.encodeAsHTML()} - ${it.fechaAlta?.encodeAsHTML()}
     </div>
     ${it.texto} ${it.enlace?.encodeAsHTML()}
     <span class="actionButton">
      <g:link action="show" id="${it.id}">Show</g:link>
     </span>
    </div>
   </g:each>
   <div class="paginateButtons">
    <g:paginate total="${Story.count()}" />
   </div>
  </div>
 </body>
</html>