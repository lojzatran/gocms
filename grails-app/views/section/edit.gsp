<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Edit Section</title>
  <g:extendedMCEHEader></g:extendedMCEHEader>    
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <g:link action="list">Listado de Secciones</g:link>
   </span>
   |
   <span class="menuButton">
    <g:link action="create">
     Nueva Secci&oacute;n
    </g:link>
   </span>
  </div>
  <div class="body">
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${section}">
    <div class="errors">
     <g:renderErrors bean="${section}" as="list" />
    </div>
   </g:hasErrors>
   <div class="prop">
    <span class="name">Id:</span>
    <span class="value">${section?.id}</span>
   </div>
   <g:form controller="section" method="post">
    <input type="hidden" name="id" value="${section?.id}" />
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='nombre'>Nombre:</label>
        </td>
        <td class='value ${hasErrors(bean:section,field:' nombre ',' errors ')}'>
         <input type='text' name='nombre' value="${section?.nombre?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='description'>
          Intro:
         </label>
        </td>
        <td class='value ${hasErrors(bean:section,field:' description ',' errors ')}'>
         <textarea rows='15' cols='40' name='intro'>${section?.intro}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='stories'>Historias:</label>
        </td>
        <td class='value ${hasErrors(bean:section,field:' stories ',' errors ')}'>
         <ul>
          <g:each var='s' in='${section?.stories?}'>
           <li>
            <g:link controller='story' action='show' id='${s.id}'>${s}</g:link>
           </li>
          </g:each>
         </ul>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="formButton">
      <g:actionSubmit value="update" />
     </span>
     <span class="formButton">
      <g:actionSubmit value="delete" />
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
