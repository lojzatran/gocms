<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="layout" content="main" />
		<title>Show Section</title>
	</head>
	<body>
		<div class="nav">
			<span class="menuButton">
				<g:link action="list">Listado de Secciones</g:link>
			</span>
			<span class="menuButton">
				<g:link action="create">
					Nueva Secci&oacute;n
				</g:link>
			</span>
		</div>
		<div class="body">
			<g:if test="${flash.message}">
				<div class="message">${flash.message}</div>
			</g:if>
			<div class="dialog">
				<table>
					<tbody>
						<tr class="prop">
							<td valign="top" class="name">Id:</td>
							<td valign="top" class="value">${section.id}</td>
						</tr>
						<tr class="prop">
							<td valign="top" class="name">Nombre:</td>
							<td valign="top" class="value">${section.nombre}</td>
						</tr>
						<tr class="prop">
							<td valign="top" class="name">Stories:</td>
							<td valign="top" style="text-align:left;" class="value">
								<ul>
									<g:each var="s" in="${section.stories}">
										<li>
											<g:link controller="story" action="show" id="${s.id}">${s}</g:link>
										</li>
									</g:each>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="buttons">
				<g:form controller="section">
					<input type="hidden" name="id" value="${section?.id}" />
					<span class="formButton">
						<g:actionSubmit value="edit" />
					</span>
					<span class="formButton">
						<g:actionSubmit value="delete" />
					</span>
				</g:form>
			</div>
		</div>
	</body>
</html>
