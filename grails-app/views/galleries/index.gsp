<%@ page import="com.imaginaworks.util.TXTUtils" %>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title></title>
 </head>
 <body>
  <div class="body">
   <h1>Galer&iacute;as de Im&aacute;genes</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
Estas son las galer&#237;as de fotos registradas. Para ver las fotos de una de ellas, haz click sobre su nombre.
   <table>
    <thead>
     <tr>
      <th>Galer&iacute;a</th>
      <th width="10%">Im&aacute;genes</th>
     </tr>
    </thead>
    <tbody>
     <g:each in="${galleries}" var="item">
      <tr>
       <td>
         <g:link action="show" id="${item.id}">${item.name?.encodeAsHTML()}</g:link>
         <g:if test="${session.user && (session.user.roles.contains('editor') || session.user.roles.contains('admin'))}">
         | <g:link action="edit" controller="imageGallery" id="${item.id}">Modificar</g:link>
         </g:if>
         <p>${item.description}</p>
         <g:set var="galleryPath" value="files/gallery/${TXTUtils.normalizeString(item.name)}/"/>
         <div class="galleryThumbs">
         <g:each in="${item.images}" var="foto">
         <a href="${createLink(controller:'galleries',action:'viewImage',id:foto.id)}" title="Ver imagen en grande">
         <img src="${createLinkTo(dir:galleryPath + foto.id,file:'45px.jpg')}" alt="${foto}"/>
         </a>&nbsp;
         </g:each>
         </div>
       </td>
       <td style="text-align: center;">
       ${item.images.size()}
       </td>
      </tr>
     </g:each>
    </tbody>
   </table>
   <div class="paginateButtons">
    <g:paginate total="${ImageGallery.count()}" />
   </div>
  </div>
 </body>
</html>
