<%@ page import="com.imaginaworks.util.TXTUtils" %><html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title></title>
 </head>
 <body>
  <div class="body">
   <h1>
    Galer&iacute;as de Im&aacute;genes
   </h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <h1>${galeria.name}</h1>
   <p>Hay ${galeria.images.size()} fotos en esta galeria</p>
   <g:set var="galleryPath" value="files/gallery/${TXTUtils.normalizeString(galeria.name)}/"/>
   <g:each in="${imagenes}" var="foto">
    <a href="${createLink(controller:'galleries',action:'viewImage',id:foto.id)}" title="Ver imagen en grande">
     <img src="${createLinkTo(dir:galleryPath + foto.id,file:'100px.jpg')}" alt="${foto}" />
    </a>
    &nbsp;
   </g:each>
   <div class="paginateButtons">
    <g:paginate total="${galeria.images.size()}" />
   </div>
  </div>
 </body>
</html>
