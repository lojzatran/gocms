<%@ page import="com.imaginaworks.util.TXTUtils" %><html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title></title>
 </head>
 <body>
  <div class="body">
   <h1>
    Galer&iacute;as de Im&aacute;genes
   </h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <h1>${foto.gallery.name}</h1>
   <h2>${foto}</h2>
   <div class="centered">
    <g:set var="imagePath" value="files/gallery/${TXTUtils.normalizeString(foto.gallery.name)}/${foto.id}" />
    <a href="${createLinkTo(dir:imagePath,file:'1024px.jpg')}" target="_blank" title="Pulsar para ver la imagen a tama&ntilde;o completo">
     <img alt="${image}" src="${createLinkTo(dir:imagePath,file:'500px.jpg')}" />
    </a>
   </div>
   <p>${foto.description}</p>
   <g:link action="show" id="${foto.gallery.id}">
    Volver a la galer&iacute;a "${foto.gallery.name}"
   </g:link>
   <g:if test="${session.user && (session.user.roles.contains('editor') || session.user.roles.contains('admin'))}">
    | <g:link action="show" controller="image" id="${foto.id}">Modificar</g:link>
   </g:if>
  </div>
 </body>
</html>
