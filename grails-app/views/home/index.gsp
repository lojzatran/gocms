<html>
 <head>
  <meta name="layout" content="main" />
%{--<!-- YAHOO MAPS -->--}%
%{--<script type="text/javascript"--}%
%{--src="http://api.maps.yahoo.com/ajaxymap?v=3.7&appid=OW9F7N3V34HK3mI0cKZHpsAU9Zi2xs_Jz9UTICj3cNh_dpVXaQ0OK.6.10KdMkhZIaA-">--}%
%{--</script>--}%
%{--<!-- FIN YAHOO MAPS -->--}%
 </head>
 <body>
  <g:if test="${flash.message}">
   <div class="message">${flash.message}</div>
  </g:if>
  <!-- Pagetitle -->
  <h1 class="pagetitle">
   <div style="float:right;">
    <!-- www.tutiempo.net -->
    %{--<a href="http://www.tutiempo.net/tiempo/Madrid_Getafe/LEGT.htm">--}%
     %{--<img style="border:none;" src="http://www.tutiempo.net/imagenes_asociados/84x38/LEGT.png" width="84" height="38" alt="El Tiempo Madrid / Getafe" />--}%
    %{--</a>--}%
    <!-- www.tutiempo.net -->
   </div>
   Bienvenido a Serranillos
  </h1>
  <!-- Content unit - One column -->
  <div class="column1-unit">
   <g:each var="i" in="${(0..1)}">
    <g:render template="storyIntro" bean="${storyList[i]}" />
   </g:each>
   <p>
    <a href="${createLink(controller:'stories')}">Leer todas las notas de prensa.</a>
   </p>
  </div>
  <hr class="clear-contentunit" />
  <!-- Content unit - Two columns -->
  %{--<div class="column2-unit-left">--}%
   %{--<h1>--}%
    %{--D&oacute;nde est&aacute;--}%
   %{--</h1>--}%
   %{--<h3>Busca tu camino a Serranillos!</h3>--}%
   %{--<div id="map" style="width: 250px; height: 250px; border: 1px solid #333;margin-left: 25px;"></div>--}%
%{--<script type="text/javascript">--}%
    %{--var direccion = "Plaza de España, Serranillos del Valle, Madrid, Spain";--}%
	%{--var map = new YMap(document.getElementById('map'));--}%
	%{--map.setMapType(YAHOO_MAP_REG);--}%
	%{--map.addZoomLong();--}%
	%{--map.addPanControl();--}%
	%{----}%
	%{--map.drawZoomAndCenter(direccion, 3);--}%
%{--</script>--}%
   %{--<p>--}%
    %{--Serranillos del Valle se encuentra localizado al sur oeste de la Comunidad de Madrid. Se enmarca dentro de la Mancomunidad del Sur Oeste, una entidad supramunicipal donde se integran...--}%
    %{--<g:link controller="static" action="localizacion">Seguir leyendo</g:link>--}%
   %{--</p>--}%
  %{--</div>--}%
  <div class="column2-unit-right">
   <g:each var="i" in="${(2..3)}">
    <g:render template="storyIntro" bean="${storyList[i]}" />
   </g:each>
  </div>
  <hr class="clear-contentunit" />
  <!-- Content unit - Three columns -->
  <div class="column3-unit-left">
   <g:render template="/events/lastEvents" />
  </div>
  <div class="column3-unit-middle">
   <g:render template="/lastComments" />
  </div>
  <div class="column3-unit-right">
   <g:render template="/forums/lastPosts" />
  </div>
 </body>
</html>