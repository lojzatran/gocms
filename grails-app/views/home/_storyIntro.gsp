<g:if test="${it}">
	<h1>
		<a href="${createLink(controller:'home', action:'story',id:it.id)}" title="Leer m&#225;s">${it.titulo}</a>
	</h1>
	<h3>${it.seccion.nombre}</h3>
	<p>${it.intro}</p>
	<p class="details">
		<g:howMuchTimeAgo value="${it.fechaAlta}" /> | <g:link controller="home" action="story" id="${it.id}">Leer m&#225;s.</g:link>
	</p>
</g:if>
