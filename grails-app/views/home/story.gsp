<html>
 <head>
  <meta name="layout" content="main" />
   <title>Story List</title>
   <g:if test="${session.user && session.user.roles.contains('editor')}">
    <g:extendedMCEHEader></g:extendedMCEHEader>
   </g:if>
   <g:else>
    <g:tinyMCEHEader />
   </g:else>
 </head>
 <body>
  <div class="body">
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:set var="commentTitle" value="${session.story.titulo?.encodeAsHTML()}" />
   <div class="story" id="story">
    <g:render template="storyText" />
   </div>
   <div class="centered">
    <!-- g:render template="/publiGoogleReferencia460x60" /-->
   </div>
   <g:each in="${comments}" var="c">
    <div class="comment">
     <div class="commentTitle">
      <h2>${c.title}</h2>
      ${c.author?.nombre.encodeAsHTML()} -
      <g:howMuchTimeAgo value="${c.created}" />
     </div>
<!-- google_ad_section_start -->     
     ${c.text}
<!-- google_ad_section_end -->     
    </div>
    <g:set var="commentTitle" value="Re: ${c.title}" />
   </g:each>
   <g:if test="${session.user}">
    <h1>A&#241;ade tu comentario</h1>
    <g:form action="saveComment" controller="home" method="post">
    <input type="hidden" name="title" value="${commentTitle}"/>
       Comentario:
       <textarea rows='20' cols="60" name='text' id='text'></textarea>
     </div>
     <div class="buttons">
      <span class="formButton">
       <input type="submit" value="Enviar"></input>
      </span>
    </g:form>
   </g:if>
   <g:else>
    <div class="message">
     Tienes que
     <a href="${createLink(controller:'home',action:'register')}">estar registrado</a>
     para iniciar sesi&#243;n y poder publicar tus comentarios
    </div>
   </g:else>
  </div>
 </body>
</html>
