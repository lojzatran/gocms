<html>
 <head>
  <meta name="layout" content="main" />
  <!-- GOOGLE MAPS -->
  <g:if test="${request.serverName=='localhost'}">
   <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAhpFB3DPzaJ7zd3xN0wU0mRTrkeOGdEaIJ5eMzyWkWWf2ZFeOrhQoBZt3U0_eBZoVn2hRuseAhKVywg" type="text/javascript"></script>
  </g:if>
  <g:else>
   <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAhpFB3DPzaJ7zd3xN0wU0mRTrkeOGdEaIJ5eMzyWkWWf2ZFeOrhQoBZt3U0_eBZoVn2hRuseAhKVywg" type="text/javascript"></script>
  </g:else>
  <script type="text/javascript">
   //<![CDATA[

    function load() {
      if (GBrowserIsCompatible()){ 
        var map = new GMap2(document.getElementById("map"));
        var pnt = new GLatLng(40.209557,-3.884354);
        var mrk = new GMarker(pnt);
        map.setCenter(pnt, 10);
        map.addControl(new GSmallMapControl());
        map.addOverlay(mrk);
      }
    }
    //]]>
  </script>
  <!-- FIN GOOGLE MAPS -->
 </head>
 <body onload="load()" onunload="GUnload()">
  <g:if test="${flash.message}">
   <div class="message">${flash.message}</div>
  </g:if>
  <!-- Pagetitle -->
  <h1 class="pagetitle">
   <div style="float:right;">
    <!-- www.tutiempo.net -->
    <a href="http://www.tutiempo.net/tiempo/Madrid_Getafe/LEGT.htm">
     <img style="border:none;" src="http://www.tutiempo.net/imagenes_asociados/84x38/LEGT.png" width="84" height="38" alt="El Tiempo Madrid / Getafe" />
    </a>
    <!-- www.tutiempo.net -->
   </div>
   Bienvenido a Serranillos
  </h1>
  <!-- Content unit - One column -->
  <div class="column1-unit">
   <g:each var="i" in="${(0..1)}">
    <g:render template="storyIntro" bean="${storyList[i]}" />
   </g:each>
   <p>
    <a href="${createLink(controller:'stories')}">Leer todas las notas de prensa.</a>
   </p>
  </div>
  <hr class="clear-contentunit" />
  <!-- Content unit - Two columns -->
  <div class="column2-unit-left">
  <div class="centered">
    <a href="${createLink(controller:'static',action:'block',id:'Atencion al Ciudadano'.encodeAsURL())}">
   <img class="banner" src="${createLinkTo(dir:'images',file:'atCiudadano.gif')}" alt="Atencion al Ciudadano" />
  </a>
  <a href="${createLink(controller:'static',action:'block',id:'Pago de cuotas de deportes'.encodeAsURL())}">
   <img class="banner" src="${createLinkTo(dir:'images',file:'cuotasDeportes.gif')}" alt="Pago de cuotas de deportes" />
  </a>  
  </div>
   <h1>
    D&oacute;nde est&aacute;
   </h1>
   <h3>Busca tu camino a Serranillos!</h3>
   <div id="map" style="width: 250px; height: 250px; border: 1px solid #ccc;margin-left: 25px;"></div>
   <p>
    Serranillos del Valle se encuentra localizado al sur oeste de la Comunidad de Madrid. Se enmarca dentro de la Mancomunidad del Sur Oeste, una entidad supramunicipal donde se integran...
    <g:link controller="static" action="localizacion">Seguir leyendo</g:link>
   </p>
  </div>
  <div class="column2-unit-right">
   <g:each var="i" in="${(2..3)}">
    <g:render template="storyIntro" bean="${storyList[i]}" />
   </g:each>
  </div>
  <hr class="clear-contentunit" />
  <!-- Content unit - Three columns -->
  <div class="column3-unit-left">
   <g:render template="/events/lastEvents" />
  </div>
  <div class="column3-unit-middle">
   <g:render template="/lastComments" />
  </div>
  <div class="column3-unit-right">
   <g:render template="/forums/lastPosts" />
  </div>
 </body>
</html>