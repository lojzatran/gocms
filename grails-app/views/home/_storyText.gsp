<div class="storyTitle">
 <h2>${session.story.titulo}</h2>
 <g:howMuchTimeAgo value="${session.story.fechaAlta}" />
 <g:if test="${session.user && session.user.roles.contains('editor')}">
  <g:remoteLink action="inlineEdit" controller="story" update="story">[editar]</g:remoteLink>
 </g:if>
</div>
<!-- google_ad_section_start -->
${session.story.texto}
<!-- google_ad_section_end -->
<div class="storyFoot">
 M&#225;s informaci&#243;n:
 <a href="${session.story.enlace?.encodeAsHTML()}">${session.story.enlace}</a>
</div>