<html>
 <head>
  <meta name="layout" content="main" />
  <title>Story List</title>
 </head>
 <body>
  <div class="body">
   <h1>${section.nombre}</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <!-- Content unit - Two columns -->
   <div class="column2-unit-left">
      ${section.intro}
   </div>
   <div class="column2-unit-right">
    <h2>Descargas</h2>
    <ul>
    <g:each in="${documents}" var="item">    
    <li><g:render template="/document/docIntro" bean="${item}" /></li>
    </g:each>
    </ul>
   </div>
   <hr class="clear-contentunit" />
   <!-- Content unit - One column -->
   <div class="column1-unit">
   <h2>Actualidad</h2>
    <g:each var="item" in="${stories}">
     <g:render template="storyIntro" bean="${item}" />
    </g:each>
   <div class="paginateButtons">
    <g:paginate total="${totalStories}" controller="home" action="section" id="${section.id}" />
   </div>    
   </div>
  </div>
 </body>
</html>