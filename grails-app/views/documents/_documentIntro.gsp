<g:if test="${it}">
	<h1>
		<a href="${createLink(controller:'documents',action:'show',id:it.id)}" title="Ver documento">${it.title}</a>
	</h1>
	<h2>
		${it.section} (registrado el <g:format value="${it.creationDate}" /> en <b>${it.category}</b>)
	</h2>
	<a href="${createLink(controller:'documents',action:'show',id:it.id)}" title="Ver documento">Ver documento</a>
</g:if>