<html>
 <head>
  <meta name="layout" content="main" />
  <title>Story List</title>
 </head>
 <body>
  <div class="body">
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <h1>${document.title}</h1>
   <h2>
    ${document.section} (${document.category}, registrado el
    <g:format value="${document.creationDate}" />
    )
   </h2>
   ${document.description}
   <div class="centered" style="margin-top:1em;padding:5px;border: 1px solid #ccc;">
    <g:if test="${document.fileName.endsWith('.pdf')}">
     <g:set var="imgName" value="pdf.gif" />
    </g:if>
    <g:elseif test="${document.fileName.endsWith('.doc')}">
     <g:set var="imgName" value="doc.gif" />
    </g:elseif>
    <g:else>
     <g:set var="imgName" value="nodef.gif" />
    </g:else>
    <g:set var="categoryPath" value="files/document/${document.category.normalized}"/>
    <img src="${createLinkTo(dir:'images/mime',file:imgName)}" alt="Descargar ${document.fileName}" />
    <a href="${createLinkTo(dir:categoryPath,file:document.fileName)}" title="Descargar">
    Descargar ${document.fileName} (Tama&ntilde;o: ${document.bytes} bytes)
    </a>
   </div>
   <g:link controller="home" action="section" id="${document.section.id}">&lt;&lt; Volver a "${document.section}"</g:link>
  </div>
 </body>
</html>
