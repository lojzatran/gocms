<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Show DocumentCategory</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <g:link action="list">Listado de Categor&iacute;as</g:link>
   </span>
   <span class="menuButton">
    <g:link action="create">Nueva Categor&iacute;a</g:link>
   </span>
  </div>
  <div class="body">
   <h1>
    Categor&iacute;a: ${documentCategory}
   </h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <div class="dialog">
    <table>
     <tbody>
      <tr class="prop">
       <td valign="top" class="name">Id:</td>
       <td valign="top" class="value">${documentCategory.id}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">T&iacute;tulo:</td>
       <td valign="top" class="value">${documentCategory.title}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Descripci&oacute;n:</td>
       <td valign="top" class="value">${documentCategory.description}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Documentos:</td>
       <td valign="top" style="text-align:left;" class="value">
        <ul>
         <g:each var="d" in="${documentCategory.documents}">
          <li>
           <g:link controller="document" action="show" id="${d.id}">${d}</g:link>
          </li>
         </g:each>
        </ul>
       </td>
      </tr>
     </tbody>
    </table>
   </div>
   <div class="buttons">
    <g:form controller="documentCategory">
     <input type="hidden" name="id" value="${documentCategory?.id}" />
     <span class="button">
      <g:actionSubmit value="Edit" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </g:form>
   </div>
  </div>
 </body>
</html>
