<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Edit DocumentCategory</title>
  <g:extendedMCEHEader></g:extendedMCEHEader>  
  
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <g:link action="list">Listado de Categor&iacute;as</g:link>
   </span>
   <span class="menuButton">
    <g:link action="create">Nueva Categor&iacute;a</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Modificar Categor&iacute;a</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${documentCategory}">
    <div class="errors">
     <g:renderErrors bean="${documentCategory}" as="list" />
    </div>
   </g:hasErrors>
   <div class="prop">
    <span class="name">Id:</span>
    <span class="value">${documentCategory?.id}</span>
   </div>
   <g:form controller="documentCategory" method="post">
    <input type="hidden" name="id" value="${documentCategory?.id}" />
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='title'>T&iacute;tulo:</label>
        </td>
        <td class='value ${hasErrors(bean:documentCategory,field:' title ',' errors ')}'>
         <input type="text" name='title' value="${documentCategory?.title?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='description'>Descripci&oacute;n:</label>
        </td>
        <td class='value ${hasErrors(bean:documentCategory,field:' description ',' errors ')}'>
         <textarea rows='5' cols='40' name='description'>${documentCategory?.description?.encodeAsHTML()}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='documents'>Documentos:</label>
        </td>
        <td class='value ${hasErrors(bean:documentCategory,field:' documents ',' errors ')}'>
         <ul>
          <g:each var='d' in='${documentCategory?.documents?}'>
           <li>
            <g:link controller='document' action='show' id='${d.id}'>${d}</g:link>
           </li>
          </g:each>
         </ul>
         <g:link controller='document' params='["documentCategory.id":documentCategory?.id]' action='create'>A&ntilde;adir Documento</g:link>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="button">
      <g:actionSubmit value="Update" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
