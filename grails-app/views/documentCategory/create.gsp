<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Create DocumentCategory</title>
  <g:extendedMCEHEader></g:extendedMCEHEader>  
  
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <g:link action="list">Listado de Categor&iacute;as</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Crear Categor&iacute;a</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${documentCategory}">
    <div class="errors">
     <g:renderErrors bean="${documentCategory}" as="list" />
    </div>
   </g:hasErrors>
   <g:form action="save" method="post">
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='title'>T&iacute;tulo:</label>
        </td>
        <td class='value ${hasErrors(bean:documentCategory,field:' title ',' errors ')}'>
         <input type="text" name='title' value="${documentCategory?.title?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='description'>Descripci&oacute;n:</label>
        </td>
        <td class='value ${hasErrors(bean:documentCategory,field:' description ',' errors ')}'>
         <textarea rows='5' cols='40' name='description'>${documentCategory?.description?.encodeAsHTML()}</textarea>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="formButton">
      <input type="submit" value="Create"></input>
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
