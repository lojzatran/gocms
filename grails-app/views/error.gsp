<html>
 <head>
  <title>No disponible</title>
  <style type="text/css">body{text-align: center; font-family: Arial, sans-serif;} h2{color:red; padding: 4px;border: 1px solid red; background-color:#fcc; text-align: center;} .errorMsg{width:85%;padding:2em;margin: auto;text-align:left;}</style>
 </head>
 <body>
  <div class="errorMsg">
   <h2>No disponible</h2>
   <img src="${createLinkTo(dir:'images',file:'error.gif')}" title="No disponible" style="border:0; float: left;" />
   <a href="http://www.imaginaworks.com" target="_blank" title="ImaginaWorks Software Factory">
    <img src="${createLinkTo(dir:'images/patrocinadores',file:'imaginaworks.png')}" alt="ImaginaWorks Software Factory" style="border:0; float: right;"/>
   </a>
   <p>En este momento estamos trabajando para mejorar el servicio. Sentimos las molestias.</p>
   <p>
    ImaginaWorks:
    <a href="mailto:info@imaginaworks.com">info@imaginaworks.com</a>
   </p>
  </div>
  <g:sendMail to="nacho@imaginaworks.com" subject="Incidencia ${request.serverName} - ${exception.message}">
   <pre>
    URL: ${request.request.requestURL} Message: ${exception.message} Caused by: ${exception.cause?.message} Class: ${exception.className} At Line: [${exception.lineNumber}] Code Snippet:
    <g:each var="cs" in="${exception.codeSnippet}">${cs}</g:each>
    Stack Trace ${exception.stackTraceText}
   </pre>
  </g:sendMail>
 </body>
</html>
<!--
 Message: ${exception.message} 
 Caused by: ${exception.cause?.message} 
 Class: ${exception.className}  		  		
 At Line: [${exception.lineNumber}]  		
 Code Snippet:	
 <g:each var="cs" in="${exception.codeSnippet}"> 
 ${cs}  			
 </g:each>  	
 Stack Trace
 ${exception.stackTraceText}
-->