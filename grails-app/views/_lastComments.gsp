<h2>&Uacute;ltimos comentarios
</h2>
<h3>comentarios a noticias</h3>
<ul>
 <g:if test="${IWConfig.list()[0].moderateComments}">
  <g:set var="cList" value="${Comment.findAllByApproved(true,[max:10, offset:0, sort:'id', order:'desc'])}" />
 </g:if>
 <g:else>
  <g:set var="cList" value="${Comment.findAll(max:10, offset:0, sort:'id', order:'desc')}" />
 </g:else>
 <g:each in="${cList}" var="comment">
  <li>
   <a href="${createLink(controller:'home',action:'story',id:comment.story?.id)}">${comment.title}</a>
  </li>
 </g:each>
</ul>