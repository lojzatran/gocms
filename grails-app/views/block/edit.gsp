<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <g:extendedMCEHEader></g:extendedMCEHEader>
  <title>Edit Block</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <a href="${createLinkTo(dir:'')}">Home</a>
   </span>
   <span class="menuButton">
    <g:link action="list">Block List</g:link>
   </span>
   <span class="menuButton">
    <g:link action="create">New Block</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Edit Block</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${block}">
    <div class="errors">
     <g:renderErrors bean="${block}" as="list" />
    </div>
   </g:hasErrors>
   <div class="prop">
    <span class="name">Id:</span>
    <span class="value">${block?.id}</span>
   </div>
   <g:form controller="block" method="post">
    <input type="hidden" name="id" value="${block?.id}" />
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='name'>Nombre:</label>
        </td>
        <td class='value ${hasErrors(bean:block,field:' name ',' errors ')}'>
         <input type="text" name='name' value="${block?.name?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='contents'>Contenido:</label>
        </td>
        <td class='value ${hasErrors(bean:block,field:' contents ',' errors ')}'>
         <textarea rows='15' cols='40' name='contents'>${block?.contents?.encodeAsHTML()}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='name'>Estilo:</label>
        </td>
        <td class='value ${hasErrors(bean:block,field:' style ',' errors ')}'>
         <input type="text" name='style' value="${block?.style}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='group'>Grupo:</label>
        </td>
        <td class='value ${hasErrors(bean:block,field:' group ',' errors ')}'>
         <g:select optionKey="id" from="${BlockGroup.list()}" name='group.id' value="${block?.group?.id}"></g:select>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="button">
      <g:actionSubmit value="Update" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
