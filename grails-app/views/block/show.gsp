<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Show Block</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <a href="${createLinkTo(dir:'')}">Home</a>
   </span>
   <span class="menuButton">
    <g:link action="list">Block List</g:link>
   </span>
   <span class="menuButton">
    <g:link action="create">New Block</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Show Block</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <div class="dialog">
    <table>
     <tbody>
      <tr class="prop">
       <td valign="top" class="name">Id:</td>
       <td valign="top" class="value">${block.id}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Nombre:</td>
       <td valign="top" class="value">${block.name}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Contenido:</td>
       <td valign="top" class="value">${block.contents}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Estilo:</td>
       <td valign="top" class="value">${block.style}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Grupo:</td>
       <td valign="top" class="value">
        <g:link controller="blockGroup" action="show" id="${block?.group?.id}">${block?.group}</g:link>
       </td>
      </tr>
     </tbody>
    </table>
   </div>
   <div class="buttons">
    <g:form controller="block">
     <input type="hidden" name="id" value="${block?.id}" />
     <span class="button">
      <g:actionSubmit value="Edit" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </g:form>
   </div>
  </div>
 </body>
</html>
