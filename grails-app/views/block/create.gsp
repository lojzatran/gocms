<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <g:extendedMCEHEader></g:extendedMCEHEader>
  <title>Create Block</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <a href="${createLinkTo(dir:'')}">Home</a>
   </span>
   <span class="menuButton">
    <g:link action="list">Block List</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Create Block</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${block}">
    <div class="errors">
     <g:renderErrors bean="${block}" as="list" />
    </div>
   </g:hasErrors>
   <g:form action="save" method="post">
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='name'>Nombre:</label>
        </td>
        <td class='value ${hasErrors(bean:block,field:' name ',' errors ')}'>
         <input type="text" name='name' value="${block?.name?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name ${hasErrors(bean:block,field:' contents ',' errors ')}' colspan="2">
         <label for='contents'>Contenido:</label>
        <br />
         <textarea rows='15' cols='40' name='contents'>${block?.contents?.encodeAsHTML()}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='name'>Estilo:</label>
        </td>
        <td class='value ${hasErrors(bean:block,field:' style ',' errors ')}'>
         <input type="text" name='style' value="${block?.style}" />
        </td>
       </tr>       
       <tr class='prop'>
        <td class='name'>
         <label for='group'>Grupo:</label>
        </td>
        <td class='value ${hasErrors(bean:block,field:' group ',' errors ')}'>
         <g:select optionKey="id" from="${BlockGroup.list()}" name='group.id' value="${block?.group?.id}"></g:select>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="formButton">
      <input type="submit" value="Create"></input>
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
