  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Edit Comment</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">Comment List</g:link></span>
            <span class="menuButton"><g:link action="create">New Comment</g:link></span>
        </div>
        <div class="body">
           <h1>Edit Comment</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <g:hasErrors bean="${comment}">
                <div class="errors">
                    <g:renderErrors bean="${comment}" as="list" />
                </div>
           </g:hasErrors>
           <div class="prop">
	      <span class="name">Id:</span>
	      <span class="value">${comment?.id}</span>
           </div>           
           <g:form controller="comment" method="post" >
               <input type="hidden" name="id" value="${comment?.id}" />
               <div class="dialog">
                <table>
                    <tbody>

                       
                       
				<tr class='prop'><td class='name'><label for='author'>Author:</label></td><td class='value ${hasErrors(bean:comment,field:'author','errors')}'><g:select optionKey="id" from="${User.list()}" name='author.id' value="${comment?.author?.id}"></g:select></td></tr>
                       
				<tr class='prop'><td class='name'><label for='created'>Created:</label></td><td class='value ${hasErrors(bean:comment,field:'created','errors')}'><g:datePicker name='created' value="${comment?.created}"></g:datePicker></td></tr>
                       
				<tr class='prop'><td class='name'><label for='parent'>Parent:</label></td><td class='value ${hasErrors(bean:comment,field:'parent','errors')}'><g:select optionKey="id" from="${Comment.list()}" name='parent.id' value="${comment?.parent?.id}"></g:select></td></tr>
                       
				<tr class='prop'><td class='name'><label for='story'>Story:</label></td><td class='value ${hasErrors(bean:comment,field:'story','errors')}'><g:select optionKey="id" from="${Story.list()}" name='story.id' value="${comment?.story?.id}"></g:select></td></tr>
                       
				<tr class='prop'><td class='name'><label for='text'>Text:</label></td><td class='value ${hasErrors(bean:comment,field:'text','errors')}'><input type='text' name='text' value="${comment?.text?.encodeAsHTML()}" /></td></tr>
                       
				<tr class='prop'><td class='name'><label for='title'>Title:</label></td><td class='value ${hasErrors(bean:comment,field:'title','errors')}'><input type='text' name='title' value="${comment?.title?.encodeAsHTML()}" /></td></tr>
                       
                    </tbody>
                </table>
               </div>

               <div class="buttons">
                     <span class="button"><g:actionSubmit value="Update" /></span>
                     <span class="button"><g:actionSubmit value="Delete" /></span>
               </div>
            </g:form>
        </div>
    </body>
</html>
