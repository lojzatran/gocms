  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Comment List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="create">New Comment</g:link></span>
        </div>
        <div class="body">
           <h1>Comment List</h1>
            <g:if test="${flash.message}">
                 <div class="message">
                       ${flash.message}
                 </div>
            </g:if>
           <table>
             <thead>
               <tr>
                   
                                      
                        <th>Id</th>
                                      
                        <th>Author</th>
                                      
                        <th>Created</th>
                                      
                        <th>Parent</th>
                                      
                        <th>Story</th>
                                      
                        <th>Text</th>
                   
                   <th></th>
               </tr>
             </thead>
             <tbody>
               <g:each in="${commentList}">
                    <tr>
                       
                            <td>${it.id?.encodeAsHTML()}</td>
                       
                            <td>${it.author?.encodeAsHTML()}</td>
                       
                            <td>${it.created?.encodeAsHTML()}</td>
                       
                            <td>${it.parent?.encodeAsHTML()}</td>
                       
                            <td>${it.story?.encodeAsHTML()}</td>
                       
                            <td>${it.text?.encodeAsHTML()}</td>
                       
                       <td class="actionButtons">
                            <span class="actionButton"><g:link action="show" id="${it.id}">Show</g:link></span>
                       </td>
                    </tr>
               </g:each>
             </tbody>
           </table>
               <div class="paginateButtons">
                   <g:paginate total="${Comment.count()}" />
               </div>
        </div>
    </body>
</html>
