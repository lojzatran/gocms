  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
          <meta name="layout" content="main" />
         <title>Show Comment</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">Comment List</g:link></span>
            <span class="menuButton"><g:link action="create">New Comment</g:link></span>
        </div>
        <div class="body">
           <h1>Show Comment</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <div class="dialog">
                 <table>
                   
                   <tbody>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Id:</td>
                              
                                    <td valign="top" class="value">${comment.id}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Author:</td>
                              
                                    <td valign="top" class="value"><g:link controller="user" action="show" id="${comment?.author?.id}">${comment?.author}</g:link></td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Created:</td>
                              
                                    <td valign="top" class="value">${comment.created}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Parent:</td>
                              
                                    <td valign="top" class="value"><g:link controller="comment" action="show" id="${comment?.parent?.id}">${comment?.parent}</g:link></td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Story:</td>
                              
                                    <td valign="top" class="value"><g:link controller="story" action="show" id="${comment?.story?.id}">${comment?.story}</g:link></td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Text:</td>
                              
                                    <td valign="top" class="value">${comment.text}</td>
                              
                        </tr>
                   
                        <tr class="prop">
                              <td valign="top" class="name">Title:</td>
                              
                                    <td valign="top" class="value">${comment.title}</td>
                              
                        </tr>
                   
                   </tbody>
                 </table>
           </div>
           <div class="buttons">
               <g:form controller="comment">
                 <input type="hidden" name="id" value="${comment?.id}" />
                 <span class="button"><g:actionSubmit value="Edit" /></span>
                 <span class="button"><g:actionSubmit value="Delete" /></span>
               </g:form>
           </div>
        </div>
    </body>
</html>
