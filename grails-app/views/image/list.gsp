<%@ page import="com.imaginaworks.util.TXTUtils" %><html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Image List</title>
 </head>
 <body>
  <div class="body">
   <h1>
    Listado completo de Im&aacute;genes
   </h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <table>
    <thead>
     <tr>
      <th></th>
      <g:sortableColumn property="id" title="Id" />
      <g:sortableColumn property="title" title="T&iacute;tulo" />
      <g:sortableColumn property="description" title="Descripci&oacute;n" />
      <g:sortableColumn property="creationDate" title="Fecha de alta" />
      <th>
       Galer&iacute;a
      </th>
      <th></th>
     </tr>
    </thead>
    <tbody>
     <g:each in="${imageList}">
      <tr>
       <td style="text-align:center;">
        <g:set var="imagePath" value="files/gallery/${TXTUtils.normalizeString(it.gallery.name)}/${it.id}" />
        <a href="${createLinkTo(dir:imagePath,file:'1024px.jpg')}" target="_blank" title="Pulsar para ver la imagen a tama&ntilde;o completo">
         <img alt="${image}" src="${createLinkTo(dir:imagePath,file:'45px.jpg')}" />
        </a>
       </td>
       <td>${it.id?.encodeAsHTML()}</td>
       <td>${it.title?.encodeAsHTML()}</td>
       <td>${it.description?.encodeAsHTML()}</td>
       <td>${it.creationDate?.encodeAsHTML()}</td>
       <td>${it.gallery?.encodeAsHTML()}</td>
       <td class="actionButtons">
        <span class="actionButton">
         <g:link action="show" id="${it.id}">Show</g:link>
        </span>
       </td>
      </tr>
     </g:each>
    </tbody>
   </table>
   <div class="paginateButtons">
    <g:paginate total="${Image.count()}" />
   </div>
  </div>
 </body>
</html>
