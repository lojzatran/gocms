  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Create Image</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">Image List</g:link></span>
        </div>
        <div class="body">
           <h1>Create Image</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <g:hasErrors bean="${image}">
                <div class="errors">
                    <g:renderErrors bean="${image}" as="list" />
                </div>
           </g:hasErrors>
           <g:form action="save" method="post" >
               <div class="dialog">
                <table>
                    <tbody>

                       
                       
                                  <tr class='prop'><td class='name'><label for='title'>Title:</label></td><td class='value ${hasErrors(bean:image,field:'title','errors')}'><input type="text" name='title' value="${image?.title?.encodeAsHTML()}"/></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='description'>Description:</label></td><td class='value ${hasErrors(bean:image,field:'description','errors')}'><textarea rows='5' cols='40' name='description'>${image?.description?.encodeAsHTML()}</textarea></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='creationDate'>Creation Date:</label></td><td class='value ${hasErrors(bean:image,field:'creationDate','errors')}'><g:datePicker name='creationDate' value="${image?.creationDate}"></g:datePicker></td></tr>
                       
                                  <tr class='prop'><td class='name'><label for='gallery'>Gallery:</label></td><td class='value ${hasErrors(bean:image,field:'gallery','errors')}'><g:select optionKey="id" from="${ImageGallery.list()}" name='gallery.id' value="${image?.gallery?.id}"></g:select></td></tr>
                       
                    </tbody>
               </table>
               </div>
               <div class="buttons">
                     <span class="formButton">
                        <input type="submit" value="Create"></input>
                     </span>
               </div>
            </g:form>
        </div>
    </body>
</html>
