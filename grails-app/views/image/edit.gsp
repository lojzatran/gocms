<%@ page import="com.imaginaworks.util.TXTUtils" %>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Edit Image</title>
  <g:extendedMCEHEader></g:extendedMCEHEader>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <g:link action="list">Todas las im&aacute;genes</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Modificar Imagen</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${image}">
    <div class="errors">
     <g:renderErrors bean="${image}" as="list" />
    </div>
   </g:hasErrors>
   <div class="prop">
    <span class="name">Id:</span>
    <span class="value">${image?.id}</span>
   </div>
   <g:form controller="image" method="post">
    <input type="hidden" name="id" value="${image?.id}" />
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='title'>
          T&iacute;tulo:
         </label>
        </td>
        <td class='value ${hasErrors(bean:image,field:' title ',' errors ')}'>
         <input type="text" name='title' value="${image?.title?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='description'>
          Descripci&oacute;n:
         </label>
        </td>
        <td class='value ${hasErrors(bean:image,field:' description ',' errors ')}'>
         <textarea rows='5' cols='40' name='description'>${image?.description?.encodeAsHTML()}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='creationDate'>Fecha:</label>
        </td>
        <td class='value ${hasErrors(bean:image,field:' creationDate ',' errors ')}'>
         <g:datePicker name='creationDate' value="${image?.creationDate}"></g:datePicker>
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="button">
      <g:actionSubmit value="Update" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </div>
   </g:form>
   <g:set var="imagePath" value="files/gallery/${TXTUtils.normalizeString(image.gallery.name)}/${image.id}" />
   <div class="centered">
    <br />
    <a href="${createLinkTo(dir:imagePath,file:'1024px.jpg')}" target="_blank" title="Pulsar para ver la imagen a tama&ntilde;o completo">
     <img alt="${image}" src="${createLinkTo(dir:imagePath,file:'500px.jpg')}" />
    </a>
   </div>
  </div>
 </body>
</html>
