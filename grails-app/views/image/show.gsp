<%@ page import="com.imaginaworks.util.TXTUtils" %>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Show Image</title>
 </head>
 <body>
  <g:set var="imagePath" value="files/gallery/${TXTUtils.normalizeString(image.gallery.name)}/${image.id}" />
  <div class="nav">
   <span class="menuButton">
    <g:link action="list">Todas las im&aacute;genes</g:link>
   </span>
  </div>
  <div class="body">
   <h1>${image}</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <div class="dialog">
    <table>
     <tbody>
      <tr class="prop">
       <td valign="top" class="name">Id:</td>
       <td valign="top" class="value">${image.id}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Titulo:</td>
       <td valign="top" class="value">${image.title}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Descripci&oacute;n:</td>
       <td valign="top" class="value">${image.description}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Fecha:</td>
       <td valign="top" class="value">${image.creationDate}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Galer&iacute;a:</td>
       <td valign="top" class="value">${image.gallery}</td>
      </tr>      
     </tbody>
    </table>
   </div>
   <div class="buttons">
    <g:form controller="image">
     <input type="hidden" name="id" value="${image?.id}" />
     <span class="button">
      <g:actionSubmit value="Edit" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </g:form>
   </div>
   <div class="centered">
   <br />
    <a href="${createLinkTo(dir:imagePath,file:'1024px.jpg')}" target="_blank" title="Pulsar para ver la imagen a tama&ntilde;o completo">
     <img alt="${image}" src="${createLinkTo(dir:imagePath,file:'500px.jpg')}" />
    </a>   
   </div>
  </div>
 </body>
</html>
