  
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
         <meta name="layout" content="main" />
         <title>Edit BlockGroup</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link action="list">BlockGroup List</g:link></span>
            <span class="menuButton"><g:link action="create">New BlockGroup</g:link></span>
        </div>
        <div class="body">
           <h1>Edit BlockGroup</h1>
           <g:if test="${flash.message}">
                 <div class="message">${flash.message}</div>
           </g:if>
           <g:hasErrors bean="${blockGroup}">
                <div class="errors">
                    <g:renderErrors bean="${blockGroup}" as="list" />
                </div>
           </g:hasErrors>
           <div class="prop">
	      <span class="name">Id:</span>
	      <span class="value">${blockGroup?.id}</span>
           </div>           
           <g:form controller="blockGroup" method="post" >
               <input type="hidden" name="id" value="${blockGroup?.id}" />
               <div class="dialog">
                <table>
                    <tbody>

                       
                       
				<tr class='prop'><td class='name'><label for='blocks'>Blocks:</label></td><td class='value ${hasErrors(bean:blockGroup,field:'blocks','errors')}'><ul>
    <g:each var='b' in='${blockGroup?.blocks?}'>
        <li><g:link controller='block' action='show' id='${b.id}'>${b}</g:link></li>
    </g:each>
</ul>
<g:link controller='block' params='["blockGroup.id":blockGroup?.id]' action='create'>Add Block</g:link>
</td></tr>
                       
				<tr class='prop'><td class='name'><label for='name'>Name:</label></td><td class='value ${hasErrors(bean:blockGroup,field:'name','errors')}'><input type="text" name='name' value="${blockGroup?.name?.encodeAsHTML()}"/></td></tr>
                       
                    </tbody>
                </table>
               </div>

               <div class="buttons">
                     <span class="button"><g:actionSubmit value="Update" /></span>
                     <span class="button"><g:actionSubmit value="Delete" /></span>
               </div>
            </g:form>
        </div>
    </body>
</html>
