<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Show Document</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <g:link action="list">Listado de Documentos</g:link>
   </span>
   <span class="menuButton">
    <g:link action="list" controller="documentCategory">Listado de Categor&iacute;as</g:link>
   </span>   
   <span class="menuButton">
    <g:link action="create">Crear Documento</g:link>
   </span>
  </div>
  <div class="body">
   <h1>${document}</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <div class="dialog">
    <table>
     <tbody>
      <tr class="prop">
       <td valign="top" class="name">Id:</td>
       <td valign="top" class="value">${document.id}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Archivo:</td>
       <td valign="top" class="value">
       <g:set var="categoryPath" value="files/document/${document.category.normalized}"/>
       <a href="${createLinkTo(dir:categoryPath,file:document.fileName)}" title="Descargar documento">Descargar</a>
       </td>
      </tr>
      
      <tr class="prop">
       <td valign="top" class="name">Secci&oacute;n:</td>
       <td valign="top" class="value">
        <g:link controller="section" action="show" id="${document?.section?.id}">${document?.section}</g:link>
       </td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Catego&iacute;a:</td>
       <td valign="top" class="value">
        <g:link controller="documentCategory" action="show" id="${document?.category?.id}">${document?.category}</g:link>
       </td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">T&iacute;tulo:</td>
       <td valign="top" class="value">${document.title}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Description:</td>
       <td valign="top" class="value">${document.description}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Fecha de registro:</td>
       <td valign="top" class="value">${document.creationDate}</td>
      </tr>
      <tr class="prop">
       <td valign="top" class="name">Tipo:</td>
       <td valign="top" class="value">${document.mimeType}</td>
      </tr>      
      <tr class="prop">
       <td valign="top" class="name">Tama&ntilde;o:</td>
       <td valign="top" class="value">${document.bytes} bytes</td>
      </tr>
     </tbody>
    </table>
   </div>
   <div class="buttons">
    <g:form controller="document">
     <input type="hidden" name="id" value="${document?.id}" />
     <span class="button">
      <g:actionSubmit value="Edit" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </g:form>
   </div>
  </div>
 </body>
</html>
