<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Edit Document</title>
  <g:extendedMCEHEader></g:extendedMCEHEader>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <a href="${createLinkTo(dir:'')}">Home</a>
   </span>
   <span class="menuButton">
    <g:link action="list">Document List</g:link>
   </span>
   <span class="menuButton">
    <g:link action="create">New Document</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Edit Document</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <g:hasErrors bean="${document}">
    <div class="errors">
     <g:renderErrors bean="${document}" as="list" />
    </div>
   </g:hasErrors>
   <div class="prop">
    <span class="name">Id:</span>
    <span class="value">${document?.id}</span>
   </div>
   <g:form controller="document" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="${document?.id}" />
    <div class="dialog">
     <table>
      <tbody>
       <tr class='prop'>
        <td class='name'>
         <label for='category'>
          Secci&oacute;n:
         </label>
        </td>
        <td class='value ${hasErrors(bean:document,field:' section ',' errors ')}'>
         <g:select optionKey="id" from="${Section.list()}" name='section.id' value="${document?.section?.id}"></g:select>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='category'>
          Categor&iacute;a
         </label>
        </td>
        <td class='value ${hasErrors(bean:document,field:' category ',' errors ')}'>
         <g:select optionKey="id" from="${DocumentCategory.list()}" name='category.id' value="${document?.category?.id}"></g:select>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='title'>
          T&iacute;tulo:
         </label>
        </td>
        <td class='value ${hasErrors(bean:document,field:' title ',' errors ')}'>
         <input type="text" name='title' value="${document?.title?.encodeAsHTML()}" />
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='description'>
          Descripci&oacute;n:
         </label>
        </td>
        <td class='value ${hasErrors(bean:document,field:' description ',' errors ')}'>
         <textarea rows='5' cols='40' name='description'>${document?.description?.encodeAsHTML()}</textarea>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='creationDate'>Fecha de registro:</label>
        </td>
        <td class='value ${hasErrors(bean:document,field:' creationDate ',' errors ')}'>
         <g:datePicker name='creationDate' value="${document?.creationDate}"></g:datePicker>
        </td>
       </tr>
       <tr class='prop'>
        <td class='name'>
         <label for='creationDate'>Archivo:</label>
        </td>
        <td class='value'>
         <input type="file" name="archivo" />
        </td>
       </tr>
      </tbody>
     </table>
    </div>
    <div class="buttons">
     <span class="button">
      <g:actionSubmit value="Update" />
     </span>
     <span class="button">
      <g:actionSubmit value="Delete" />
     </span>
    </div>
   </g:form>
  </div>
 </body>
</html>
