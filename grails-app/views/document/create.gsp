<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="layout" content="main" />
		<title>Create Document</title>
		<g:extendedMCEHEader></g:extendedMCEHEader>
	</head>
	<body>
		<div class="nav">
			<span class="menuButton">
				<a href="${createLinkTo(dir:'')}">Home</a>
			</span>
			<span class="menuButton">
				<g:link action="list">Document List</g:link>
			</span>
		</div>
		<div class="body">
			<h1>Create Document</h1>
			<g:if test="${flash.message}">
				<div class="message">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${document}">
				<div class="errors">
					<g:renderErrors bean="${document}" as="list" />
				</div>
			</g:hasErrors>
			<g:form action="save" method="post" enctype="multipart/form-data">
				<div class="dialog">
					<table>
						<tbody>
							<tr class='prop'>
								<td class='name'>
									<label for='category'>
										Secci&oacute;n:
									</label>
								</td>
								<td class='value ${hasErrors(bean:document,field:' section ',' errors ')}'>
									<g:select optionKey="id" from="${Section.list()}" name='section.id' value="${document?.section?.id}"></g:select>
								</td>
							</tr>
							<tr class='prop'>
								<td class='name'>
									<label for='category'>
										Categor&iacute;a
									</label>
								</td>
								<td class='value ${hasErrors(bean:document,field:' category ',' errors ')}'>
									<g:select optionKey="id" from="${DocumentCategory.list()}" name='category.id' value="${document?.category?.id}"></g:select>
								</td>
							</tr>
							<tr class='prop'>
								<td class='name'>
									<label for='title'>
										T&iacute;tulo:
									</label>
								</td>
								<td class='value ${hasErrors(bean:document,field:' title ',' errors ')}'>
									<input type="text" name='title' value="${document?.title?.encodeAsHTML()}" />
								</td>
							</tr>
							<tr class='prop'>
								<td class='name'>
									<label for='description'>
										Descripci&oacute;n:
									</label>
								</td>
								<td class='value ${hasErrors(bean:document,field:' description ',' errors ')}'>
									<textarea rows='5' cols='40' name='description'>${document?.description?.encodeAsHTML()}</textarea>
								</td>
							</tr>
							<tr class='prop'>
								<td class='name'>
									<label for='creationDate'>Archivo:</label>
								</td>
								<td class='value'>
									<input type="file" name="archivo"/>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="buttons">
					<span class="formButton">
   <script type="text/javascript">function showSpinner(){Effect.Appear('spinner_1',{duration:0.5,queue:'end'});document.getElementById('s1').disabled=true;}</script>

						<input type="submit" value="Enviar" onclick="showSpinner();" id="s1"></input>
    <div style="display:none;" id="spinner_1"><img src="${createLinkTo(dir:'images',file:'spinner.gif')}" /> Subiendo archivos. Un momento por favor... </div>
      
					</span>
				</div>
			</g:form>
		</div>
	</body>
</html>
