<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="main" />
  <title>Document List</title>
 </head>
 <body>
  <div class="nav">
   <span class="menuButton">
    <g:link action="create">Crear Documento</g:link>
   </span>
  </div>
  <div class="body">
   <h1>Listado completo de documentos</h1>
   <g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
   </g:if>
   <table>
    <thead>
     <tr>
      <g:sortableColumn property="id" title="Id" />
      <g:sortableColumn property="title" title="T&iacute;tulo" />      
      <th>Categor&iacute;a</th>
      <th>Secci&oacute;n</th>
      <g:sortableColumn property="creationDate" title="Fecha de Registro" />
      <th></th>
     </tr>
    </thead>
    <tbody>
     <g:each in="${documentList}">
      <tr>
       <td>${it.id?.encodeAsHTML()}</td>
       <td>${it.title?.encodeAsHTML()}</td>
       <td>${it.category?.encodeAsHTML()}</td>
       <td>${it.section?.encodeAsHTML()}</td>
       <td>${it.creationDate?.encodeAsHTML()}</td>
       <td class="actionButtons">
        <span class="actionButton">
         <g:link action="show" id="${it.id}">Detalles</g:link> |
         <g:set var="categoryPath" value="files/document/${it.category.normalized}"/>
         <a href="${createLinkTo(dir:categoryPath,file:it.fileName)}" title="Descargar documento">Descargar</a>
        </span>
       </td>
      </tr>
     </g:each>
    </tbody>
   </table>
   <div class="paginateButtons">
    <g:paginate total="${Document.count()}" />
   </div>
  </div>
 </body>
</html>
