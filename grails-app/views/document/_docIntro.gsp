<g:set var="doc" value="${it}"/>
<g:if test="${doc.fileName.endsWith('.pdf')}">
 <g:set var="imgName" value="pdf.gif" />
</g:if>
<g:elseif test="${doc.fileName.endsWith('.doc')}">
 <g:set var="imgName" value="doc.gif" />
</g:elseif>
<g:else>
 <g:set var="imgName" value="nodef.gif" />
</g:else>
<img src="${createLinkTo(dir:'images/mime',file:imgName)}" alt="${doc.title}" />
<g:link controller="documents" action="show" id="${doc.id}">${doc.title}</g:link>