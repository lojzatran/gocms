/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
package com.imaginaworks.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.org.apache.xpath.internal.XPathAPI;

/**
 *
 * @author nacho
 *
 */
public class XMLUtils {
    private static final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    static {
        factory.setValidating(false);
    }

    /**
     *
     * @param xml
     * @return
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public static Document buildDocument(String xml) throws SAXException, IOException, ParserConfigurationException {
        ByteArrayInputStream bis = new ByteArrayInputStream(xml.getBytes());
        return factory.newDocumentBuilder().parse(bis);
    }

    /**
     *
     * @param is
     * @return
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public static Document buildDocument(InputStream is) throws SAXException, IOException, ParserConfigurationException {
        return factory.newDocumentBuilder().parse(is);
    }


    /**
     * Devuelve el contenido en del primer elemento que cumpla la consulta
     * xpath.
     *
     * @param node
     * @param query
     * @return
     * @throws TransformerException
     */
    public static String xpathFirstNodeValue(Node node, String query) {
        String s = "?";
        try {
            Iterator it = xpath(node, query);
            s = (it != null && it.hasNext()) ? ((Element) it.next()).getFirstChild().getNodeValue() : "";
        } catch (Exception x) {
            x.printStackTrace();
        }
        return s;
    }
    /**
    *
    *
    * @param xml
    * @param xpath
    * @return
    * @throws SAXException
    * @throws IOException
    * @throws ParserConfigurationException
    */
   public static String xpathFirstNodeValue(String xml, String xpath) throws SAXException, IOException, ParserConfigurationException {
       Document doc = buildDocument(xml);
       return xpathFirstNodeValue(doc, xpath);
   }
    /**
     *
     * @param node
     * @param query
     * @return
     * @throws TransformerException
     */
    public static Iterator xpath(Node node, String query) throws TransformerException {
        NodeList nodelist = XPathAPI.selectNodeList(node, query);
        return new NodeListIterator(nodelist);
    }

    /**
     *
     * @author nacho
     *
     */
    public static class NodeListIterator implements Iterator {
        NodeList list = null;

        int pos = 0;

        /**
         *
         * @param nodelist
         */
        public NodeListIterator(NodeList nodelist) {
            list = nodelist;
            pos = 0;
        }

        /**
         *
         */
        public boolean hasNext() {
            return pos < list.getLength();
        }

        /**
         *
         */
        public Object next() {
            return list.item(pos++);
        }

        public void remove() {
            // TODO Auto-generated method stub

        }

    }

    /**
     *
     * @param txt
     * @return
     */
    public static String removeMarkup(String txt,List<String> permittedTags) {
        StringBuffer buff = new StringBuffer();
        StringBuffer tmp = new StringBuffer();
        final char OPEN = '<';
        final char CLOSE = '>';
        int i = 0;
        char c;
        String tagName = null;
        // 1. quitar scripts

        // 2. quitar etiquetas.
        //while (txt.indexOf(OPEN) != -1) {
            for (i = 0; i < txt.length(); i++) {
                c = txt.charAt(i);
                if (c == OPEN) {
                    tmp.delete(0,tmp.length());
                    tmp.append(c);
                    tagName = null;
                    do {
                        c = txt.charAt(++i);
                        if(tagName == null && (c==' ' || c==CLOSE)){
                            tagName = tmp.substring(1);
                            if(tagName.charAt(0)=='/'){
                                tagName = tagName.substring(1);
                            }
                        }
                        tmp.append(c);
                    } while (c != CLOSE);
                    if(permittedTags.contains(tagName)){
                        buff.append(tmp);
                    }
                } else {
                    buff.append(c);
                }
            }
            txt = buff.toString();
        //}

        // 3. sustituir entidades
        txt = txt.replaceAll("\\&aacute\\;", "").replaceAll("\\&eacute\\;", "").replaceAll("\\&iacute\\;", "")
                .replaceAll("\\&oacute\\;", "").replaceAll("\\&uacute\\;", "").replaceAll("\\&Aacute\\;", "")
                .replaceAll("\\&Eacute\\;", "").replaceAll("\\&Iacute\\;", "").replaceAll("\\&Oacute\\;", "")
                .replaceAll("\\&Uacute\\;", "").replaceAll("\\&ntilde\\;", "").replaceAll("\\&Ntilde\\;", "")
                .replaceAll("\\&[a-z]+\\;", " ").replaceAll("\\s+", " ");
        return txt;
    }

    /**
     * guarri-prueba
     * @param args
     */
    public static void main(String[] args) {
        // String txt = "Hola Amiguetes <p> como estamos </p>. <a
        // href=\"llll\">link</a>";
        String txt = "Maana es el da en <a href=\"\">que</a> <br /> empieza <i>todo</i> el tema.";
        System.out.println("antes: " + txt);
        System.out.println("despues: " + removeMarkup(txt,Arrays.asList(new String[]{"i","p","b"})));
        System.exit(0);
    }


}
