/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
package com.imaginaworks.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;


/**
 * @author nacho
 *
 */
public class Log {

    private static final Map LOGGER_CACHE = new HashMap();
    private static Logger anonLogger = null;
    private static Handler defaultHandler = null;
    private static final DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private static int length = 1;

	/**
	 *
	 * @param txt
	 * @param author
	 */
	public static void trace(Class c,String txt){
        if (c != null) {
            StringBuffer sb = new StringBuffer();
            String n = c.getName();
            n = n.substring(1 + n.lastIndexOf('.'));
            sb.append('[');
            sb.append(n);
            sb.append(']');
            for(int i = 0; i<calculateSpaces(n.length()); i++){
                sb.append(' ');
            }
            sb.append(txt);
            getLogger(c).log(Level.INFO,sb.toString());
        }
        else{
            getAnonLogger().log(Level.INFO,txt);
        }
	}

    /**
     *
     * @param i
     * @return
     */
    private static int calculateSpaces(int i) {
        if(i>=length){
            length = i + 1;
        }
        return length - i;
    }

    /**
     *
     * @return
     */
    private static Logger getAnonLogger() {
        if(anonLogger == null){
            anonLogger = Logger.getAnonymousLogger();
            anonLogger.setUseParentHandlers(false);
            anonLogger.setLevel(Level.ALL);
            anonLogger.addHandler(getDefaultHandler());
        }
        return anonLogger;
    }

    /**
     *
     * @param c
     * @return
     */
    private static Logger getLogger(Class c){
        Logger l = (Logger) LOGGER_CACHE.get(c);
        if(l == null){
            String n = c.getName();
            l = Logger.getLogger(n);
            l.setUseParentHandlers(false);
            l.setLevel(Level.ALL);
            l.addHandler(getDefaultHandler());
            LOGGER_CACHE.put(c,l);
        }
        return l;
    }


    /**
     *
     * @return
     */
    private static Handler getDefaultHandler() {
        if(defaultHandler == null){
            defaultHandler = new ConsoleHandler();
            defaultHandler.setFormatter(new Formatter(){
                private static final String SEP = " - ";
                public String format(LogRecord record) {
                    StringBuffer sb = new StringBuffer(df.format(new Date(record.getMillis())));
                    sb.append(SEP);
                    sb.append(record.getMessage());
                    sb.append('\n');
                    return sb.toString();
                }});
        }
        return defaultHandler;
    }

    /**
     *
     * @param class1
     * @param string
     */
    public static void traceStackTrace(Class c, String string) {
        trace(c,string);
        trace(c,"Traza de pila:");
        Throwable t = new Throwable();
        t.fillInStackTrace();
        t.printStackTrace();
    }
}
