/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
package com.imaginaworks.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author nacho
 *
 */
public class DateUtils {

    private static final Map<String,DateFormat> DF_CACHE = new HashMap<String,DateFormat>();

    /**
     *
     * @param date
     * @param format
     * @return
     */
    public static Date parse(String date, String format){
        try {
            return getFormatter(format).parse(date);
        } catch (ParseException e) {
            //esto es cutre no?? :-P
            System.err.println(e.getMessage());
            return null;
        }
    }

    /**
     *
     * @param date
     * @param format
     * @return
     */
    public static String format(Date date, String format){
        return getFormatter(format).format(date);
    }

    /**
     *
     * @param format
     * @return
     */
    private static DateFormat getFormatter(String format){
       DateFormat df = DF_CACHE.get(format);
       if(df == null){
           df = new SimpleDateFormat(format,TXTUtils.getDefaultLocale());
           DF_CACHE.put(format,df);
       }
       return df;
    }

    /**
     *
     * @return
     */
    public static List<Date> getThisWeek(){
        List<Date> dias = new LinkedList<Date>();
        Calendar c = Calendar.getInstance();
        
        setTimeToZero(c);
        setToLastMonday(c);
        
        for(int i = 0; i<7; i++){
            dias.add(c.getTime());
            c.add(Calendar.DATE,1);
        }
        return dias;
    }

    /**
     * 
     * @param c
     */
    private static void setTimeToZero(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);        
    }

   public static Date getFirstDayOfMonth(){
       Calendar c = Calendar.getInstance();
       setTimeToZero(c);
       c.set(Calendar.DAY_OF_MONTH, 1);
       return c.getTime();
   }
   
   public static Date getLastDayOfMonth(){
       Calendar c = Calendar.getInstance();
       setTimeToZero(c);
       c.set(Calendar.DAY_OF_MONTH,1);
       c.add(Calendar.MONTH, 1);
       c.add(Calendar.DATE, -1);
       return c.getTime();
   }
   
   public static Date getFirstDayOfYear(){
       Calendar c = Calendar.getInstance();
       setTimeToZero(c);
       c.set(Calendar.MONTH, Calendar.JANUARY);
       c.set(Calendar.DAY_OF_MONTH,1);
       return c.getTime();
   }
   public static Date getLastDayOfYear(){
       Calendar c = Calendar.getInstance();
       setTimeToZero(c);
       c.set(Calendar.MONTH, Calendar.DECEMBER);
       c.set(Calendar.DAY_OF_MONTH,31);
       return c.getTime();
   }   
   
   /**
    * Devuelve una fecha sin informacin de hora (a las 0:0:0)
    * @return
    */
   public static Date getCleanDate(){
       Calendar c = Calendar.getInstance();
       setTimeToZero(c);
       return c.getTime();
   }
    
    /**
     * 
     * @return
     */
    public static Date getLastMonday(){
        Calendar c = Calendar.getInstance();
        setToLastMonday(c);
        return c.getTime();
    }
    
    /**
     * 
     * @param c
     */
    private static void setToLastMonday(Calendar c) {
        setTimeToZero(c);
        while(c.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY){
            c.add(Calendar.DATE, -1);
        }
    }
    
    /**
     * 
     * @param args
     */
    public static void main(String[] args){
        System.out.println(getFirstDayOfMonth());
        System.out.println(getLastDayOfMonth());
    }
}
