/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
package com.imaginaworks.util;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.classifier4J.CustomizableStopWordProvider;
import net.sf.classifier4J.DefaultTokenizer;
import net.sf.classifier4J.IStopWordProvider;
import net.sf.classifier4J.ITokenizer;
import net.sf.classifier4J.Utilities;
import net.sf.classifier4J.summariser.ISummariser;
import net.sf.classifier4J.summariser.SimpleSummariser;

/**
 * 
 * @author nacho
 * 
 */
public class TXTUtils {
	private static final Locale localeUS = new Locale("en", "US");
	private static final Locale localeES = new Locale("es", "ES");
	private static final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	private static final DateFormat shortDf = new SimpleDateFormat("dd/MM/yy");
	private static final NumberFormat percentFormatIN = NumberFormat
			.getPercentInstance(localeUS);
	private static final NumberFormat floatFormatIN = NumberFormat
			.getNumberInstance(localeUS);
	private static final NumberFormat percentFormatOUT = NumberFormat
			.getPercentInstance(localeES);
	private static final NumberFormat floatFormatOUT = NumberFormat
			.getNumberInstance(localeES);
	private static final Pattern REGEX_IMG_TAG = Pattern
			.compile("\\ssrc=\"([\\S]+)\\.(gif|png|jpg|jpeg)\"\\s");

	private static ITokenizer tokenizer = new DefaultTokenizer();
	private static IStopWordProvider swProvider = null;
	private static final boolean debug = false;

	static {
		percentFormatIN.setGroupingUsed(true);
		percentFormatIN.setMaximumFractionDigits(2);
		percentFormatIN.setMinimumFractionDigits(2);
		percentFormatIN.setParseIntegerOnly(false);
		floatFormatIN.setGroupingUsed(true);
		floatFormatIN.setMaximumFractionDigits(2);
		floatFormatIN.setMinimumFractionDigits(2);
		floatFormatIN.setParseIntegerOnly(false);
		percentFormatOUT.setGroupingUsed(true);
		percentFormatOUT.setMaximumFractionDigits(2);
		percentFormatOUT.setMinimumFractionDigits(2);
		percentFormatOUT.setParseIntegerOnly(false);
		floatFormatOUT.setGroupingUsed(true);
		floatFormatOUT.setMaximumFractionDigits(2);
		floatFormatOUT.setMinimumFractionDigits(2);
		floatFormatOUT.setParseIntegerOnly(false);
		
		try{
			Log.trace(TXTUtils.class, "Cargando lista de palabras comunes...");
			swProvider = new CustomizableStopWordProvider("iwStopWords.txt");
		}
		catch(Exception x){
			Log.trace(TXTUtils.class, "ERROR: No se pudo cargar la lista de palabras comunes:");
			x.printStackTrace();
		}
	}

	/**
	 * 
	 * @return
	 */
	public static Locale getDefaultLocale() {
		return localeES;
	}

	/**
	 * 
	 * @param f
	 * @return
	 */
	public static Number parseFloat(String f) {
		Number n = null;
		try {
			n = floatFormatIN.parse(f);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (debug) {
			System.out.println(f + "->" + n);
		}
		return n;
	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	public static String formatCurrency(Number v) {
		String s = floatFormatOUT.format(v.doubleValue());
		return s;
	}

	/**
	 * 
	 * @param p
	 * @return
	 */
	public static Number parsePercent(String p) {
		Number n = null;
		try {
			n = percentFormatIN.parse(p);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (debug) {
			System.out.println(p + "->" + n);
		}
		return n;
	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	public static boolean isInteger(String v) {
		try {
			Integer.parseInt(v);
			return true;
		} catch (Exception x) {
			return false;
		}
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Date parseDate(String date) {
		try {
			if (debug) {
				System.out.println("Convertir en fecha: " + date);
			}
			return df.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param d
	 * @return
	 */
	public static String formatDate(Date d) {
		return df.format(d);
	}

	/**
	 * 
	 * @param d
	 * @return
	 */
	public static String formatShortDate(Date d) {
		return shortDf.format(d);
	}

	/**
	 * Cambia las  por n y quita acentos de las vocales.
	 * @param input
	 * @return
	 */
//	public static String removeSpanishAccents(String input){
//		input = input
//		.replace('', 'a')
//		.replace('', 'e')
//		.replace('', 'i')
//		.replace('', 'o')
//		.replace('', 'u')
//		.replace('', 'n')
//		.replace('', 'A')
//		.replace('', 'E')
//		.replace('', 'I')
//		.replace('', 'O')
//		.replace('', 'U')
//		.replace('', 'N');
//
//		return input;
//
//	}
	/**
	 * 
	 * 
	 * @param input
	 * @return la cadena input en minusculas y caracteres distintos de letras y
	 *         numeros. Cambia espacios por '-'
	 */
	public static String normalizeString(String input) {
		StringBuilder sb = new StringBuilder();
		char c;
		int n;
//		input = removeSpanishAccents(input.toLowerCase());
		for (int i = 0; i < input.length(); i++) {
			c = input.charAt(i);
			if (c == ' ') {
				sb.append('-');
			} else {
				n = (int) c;
				if ((n > 96 && n < 123) || (n > 47 && n < 58)) {
					sb.append(c);
				}
			}
		}
		return sb.toString();
	}

	/**
	 * 
	 * 
	 * @param input
	 * @return
	 */
	public static String toNumEntities(String input) {
		StringBuilder sb = new StringBuilder();
		char c;
		int n;
		for (int i = 0; i < input.length(); i++) {
			c = input.charAt(i);
			n = (int) c;
			if (n < 128) {
				sb.append(c);
			} else {
				sb.append("&#" + n + ";");
			}
		}
		return sb.toString();
	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	public static String capitalize(String input) {
		/*
		 * tratamos el caso de 'lava'
		 */
		if (input.startsWith("&")) {
			int pos = input.indexOf(';');
			String ent = input.substring(0, pos + 1);
			int c = Integer.parseInt(ent.substring(2, ent.length() - 1));
			input = String.valueOf((char) c) + input.substring(pos + 1);
		}
		/*
		 * compruebo que no este formateado ya
		 */
		int c0 = input.charAt(0);
		int c1 = input.charAt(1);
		if (c0 > 64 && c0 < 91 && c1 > 96 && c1 < 123) {
			return input;
		} else {
			return input.substring(0, 1).toUpperCase()
					+ input.substring(1).toLowerCase();
		}
	}

	/**
	 * Encuentra cadenas del tipo "src=http://.../fichero.jpg|gif|jpeg|png
	 * 
	 * @param html
	 * @return
	 */
	public static String[] getImageUrls(String html) {
		Matcher m = REGEX_IMG_TAG.matcher(html);
		List<String> urls = new ArrayList<String>();
		// grp == src="http://host.com/img.jpg"
		String grp;
		while (m.find()) {
			grp = html.substring(m.start(), m.end());
			urls.add(grp.substring(6, grp.lastIndexOf('"')));
		}
		return urls.toArray(new String[urls.size()]);
	}

	/**
	 * Realiza una limpieza en un texto html:
	 * 
	 * 1. sustituye &lt; por '<' y &gt; por '>' 2. convierte los caracteres no
	 * ascii a entidades numericas.
	 * 
	 * @param input
	 * @return
	 */
	public static String cleanHTMLText(String input) {
		input = input.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
		StringBuilder sb = new StringBuilder();
		int n = 0;
		char c;
		for (int i = 0; i < input.length(); i++) {
			c = input.charAt(i);
			if ((int) c < 128) {
				sb.append(c);
			} else {
				sb.append("&#" + (int) c + ";");
			}
		}
		return sb.toString();
	}

	public static String getRandomString(int size) {
		String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder sb = new StringBuilder();
		Random r = new Random();
		int charsSize = chars.length();
		do {
			sb.append(chars.charAt(r.nextInt(charsSize)));
		} while (sb.length() < size);
		return sb.toString();
	}

	@SuppressWarnings("unchecked")
	public static String getKeywords(String input, int maxKW) {
//		input = removeSpanishAccents(input);
		// get the frequency of each word in the input
		Map wordFrequencies = Utilities.getWordFrequency(input,false,tokenizer, swProvider);
		// now create a set of the X most frequent words
		Set mostFrequentWords = Utilities.getMostFrequentWords(maxKW, wordFrequencies);
		StringBuilder sb = new StringBuilder();
		int i = 0;
		for (Object o : mostFrequentWords) {
			sb.append(o.toString() + ' ');	
			i++;
			if(i>=maxKW){
				break;
			}
			
		}
		return sb.toString();
	}

	
	public static String summarise(String input, int sentences) {
		ISummariser is = new SimpleSummariser();
		return is.summarise(input, sentences);
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		// System.out.println(capitalize(toNumEntities("LAVA")));
		//System.out.println(capitalize("MADRID"));
		//System.out.println(capitalize("madrid"));
		//System.out.println(capitalize("Madrid"));
		//System.out.println(getRandomString(10));
	}
}
