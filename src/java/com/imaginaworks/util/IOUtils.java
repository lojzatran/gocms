/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */
package com.imaginaworks.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.activation.FileDataSource;

/**
 * @author nacho
 *
 */
public class IOUtils {
    
    
    public static String getMimeType(File f){
        return new FileDataSource(f).getContentType();
    }
    
    /**
     *
     * @param path
     * @return
     */
    public static boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    /**
     *
     * @param origFolder
     * @param destFolder
     * @return
     */
    public static boolean copyContents(File origFolder, File destFolder) {
        boolean b = true;
        if (!destFolder.exists()) {
            destFolder.mkdirs();
        }
        File[] cont = origFolder.listFiles();
        File f = null;
        File nf = null;
        for (int i = 0; i < cont.length; i++) {
            f = cont[i];
            nf = new File(destFolder, f.getName());
            if (f.isDirectory()) {
                copyContents(f, nf);
            } else {
                b = copyFile(f, nf);
            }
        }
        return b;
    }

    /**
     *
     * @param fromFile
     * @param toFile
     * @return
     */
    public static boolean copyFile(File fromFile, File toFile) {
        boolean result = false;
        try {
            FileInputStream fis = new FileInputStream(fromFile);
            File tmp = File.createTempFile("tempFile", "tmp");
            FileOutputStream fos = new FileOutputStream(tmp);
            byte[] buff = new byte[1024];
            while (fis.read(buff) != -1) {
                fos.write(buff);
            }
            fos.close();
            fis.close();
            result = true;
            result = tmp.renameTo(toFile);
        } catch (Exception x1) {
            x1.printStackTrace();
        }
        return result;
    }

    /**
     *
     * @param is
     * @return
     * @throws IOException
     */
    public static File copyToTMPFile(InputStream is) throws IOException {
        File file = File.createTempFile("mister-i", ".tmp");
        OutputStream os = new FileOutputStream(file);
        int b;
        while ((b = is.read()) != -1) {
            os.write(b);
        }
        os.close();
        return file;
    }

    /**
     *
     * @param f
     * @return
     */
    public static String readFile(File f) {
        FileReader fr = null;
        StringBuffer sb = new StringBuffer();
        try {
            int c = 0;
            fr = new FileReader(f);
            while ((c = fr.read()) != -1) {
                sb.append((char) c);
            }
        }
        catch(Exception x){
            x.printStackTrace();
        }
        finally {
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


    /**
     *
     * @param content
     * @param f
     */
    public static void saveToFile(String content, File f){
        PrintWriter pw = null;
        try{
            pw = new PrintWriter(new FileWriter(f));
            pw.print(content);
        }
        catch(Exception x){
            x.printStackTrace();
        }
        finally{
            pw.close();
        }
    }

}
