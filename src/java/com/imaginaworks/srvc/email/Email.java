/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

package com.imaginaworks.srvc.email;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.mail.HtmlEmail;

import com.imaginaworks.util.Log;


/**
 * @author Nacho
 */
public class Email {
    private static final String DEFAULT_ALTERNATE_TEXT = "Tu cliente de correo no soporta mensajes en html...";
    private static final String XML_HTML_DOCTIPE = "<!doctype html";
    private static final String HTML_HEADER = "<meta http-equiv=\"Content-Type\" content=\"text/html";
    private static final Pattern REGEX_IMG_TAG = Pattern.compile("\\ssrc=\"http\\:\\/\\/([\\S]+)\\.(gif|png|jpg|jpeg)\"\\s");
    private String smtp;
    private String smtp_usr = null;
    private String smtp_pwd = null;
    private List to;
    private String from;
    private String subject;
    private String body;
    private File[] attachments;
    private static final boolean debug = false;

    /**
     * 
     * @param smtpServer
     * @param user
     * @param pwd
     * @param fromAddr
     * @param toAddr
     * @param subject
     * @param body
     * @param attachments
     * @throws EmailException
     */
    public static void sendEmail(String smtpServer, String user, String pwd, String fromAddr, String toAddr,
            String subject, String body) throws EmailException {

        try {
            Email emilio = new Email();
            emilio.setSmtp(smtpServer);
            if (user != null && pwd != null) {
                emilio.setSmtp_usr(user);
                emilio.setSmtp_pwd(pwd);
            }
            emilio.setFrom(fromAddr);
            emilio.setBody(body);
            emilio.setSubject(subject);
            emilio.addRecipient(toAddr);
            emilio.send();
        } catch (Exception ex) {
            throw new EmailException(ex);
        }

    }
    /**
     * 
     * @param smtpServer
     * @param user
     * @param pwd
     * @param fromAddr
     * @param toAddr
     * @param subject
     * @param body
     * @throws EmailException
     */
    public static void sendHTMLEmail(String smtpServer, String user, String pwd, String fromAddr, String toAddr,
            String subject, String body) throws EmailException {

        try {
            Email emilio = new Email();
            emilio.setSmtp(smtpServer);
            if (user != null && pwd != null) {
                emilio.setSmtp_usr(user);
                emilio.setSmtp_pwd(pwd);
            }
            emilio.setFrom(fromAddr);
            emilio.setBody(body);
            emilio.setSubject(subject);
            emilio.addRecipient(toAddr);
            emilio.sendWithEmbeddedImages();
        } catch (Exception ex) {
            throw new EmailException(ex);
        }

    }
    /**
     * 
     * @throws Exception
     */
    public void send() throws Exception {
        // 1. configuramos
        boolean useAuth = smtp_pwd != null && smtp_usr != null;
        Authenticator auth = null;
        Properties props = System.getProperties();
        props.put("mail.smtp.host", smtp);
        if (useAuth) {
            props.put("mail.smtp.auth", "true");
            auth = new SMTPAuthenticator(smtp_usr, smtp_pwd);
        }
        Session session = Session.getDefaultInstance(props, auth);
        Transport tr = session.getTransport("smtp");

        // 2. creamos el mensaje
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.setSubject(subject);
        message.setText(body);
        for (Iterator it = to.iterator(); it.hasNext();)
            message.addRecipient(Message.RecipientType.TO, new InternetAddress((String) it.next()));

        // 3. Aadimos los ficheros adjuntos:
        DataSource source = null;
        if (attachments != null) {
            // El contenido es un Multipart:
            Multipart multipart = new MimeMultipart();
            // Una parte para el texto:
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(body, getMime(body));
            multipart.addBodyPart(messageBodyPart);
            for (int i = 0; i < attachments.length; i++) {
                messageBodyPart = new MimeBodyPart();
                source = new FileDataSource(attachments[i]);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(attachments[i].getName());
                multipart.addBodyPart(messageBodyPart);
            }
            message.setContent(multipart);
        } else {
            message.setContent(body, getMime(body));
        }

        // 5. Enviamos
        tr.connect();
        tr.sendMessage(message, message.getAllRecipients());
        tr.close();
    }

    /**
     * 
     * @throws Exception
     */
    public void sendWithEmbeddedImages() throws Exception {
        sendWithEmbeddedImages(DEFAULT_ALTERNATE_TEXT);
    }

    /**
     * Usa commons mail.
     * 
     */
    public void sendWithEmbeddedImages(String alternateText) throws Exception {
        boolean useAuth = smtp_pwd != null && smtp_usr != null;
        // Create the email message
        HtmlEmail email = new HtmlEmail();
        email.setDebug(debug);
        email.setHostName(this.smtp);
        for (Iterator it = to.iterator(); it.hasNext();) {
            email.addTo((String) it.next());
        }
        email.setFrom(this.from);
        email.setSubject(this.subject);

        embedImages(email);

        // set the alternative message
        email.setTextMsg(alternateText);

        if (useAuth) {
            email.setAuthentication(smtp_usr, smtp_pwd);
        }
        // send the email
        email.send();
    }

    /**
     * 
     * @param email
     * @throws Exception
     */
    private void embedImages(HtmlEmail email) throws Exception {
        Matcher m = REGEX_IMG_TAG.matcher(this.body);
        Map replacements = new HashMap();
        // grp == src="http://host.com/img.jpg"
        String grp = null;
        String sUrl = null;
        URL url = null;
        String cid = null;
        int count = 0;
        while (m.find()) {
            grp = this.body.substring(m.start(), m.end());
            sUrl = grp.substring(6, grp.lastIndexOf('"'));
            Log.trace(getClass(),"Tratando imagen: " + sUrl);
            url = new URL(sUrl);
            cid = email.embed(url, "img" + count++);
            replacements.put(grp, " src=\"cid:" + cid + "\" ");
        }
        Entry entry = null;
        for (Iterator it = replacements.entrySet().iterator(); it.hasNext();) {
            entry = (Entry) it.next();
            this.body = this.body.replaceAll((String) entry.getKey(), (String) entry.getValue());
        }
        // set the html message
        email.setHtmlMsg(this.body);
    }

    /**
     * @param text
     * @return
     */
    private String getMime(String text) {
        String s = text.toLowerCase();
        if (s.startsWith(XML_HTML_DOCTIPE))
            return "text/html";
        if (s.indexOf(HTML_HEADER) != -1)
            return "text/html";
        if (s.indexOf("<html") != -1)
            return "text/html";
        return "text/plain";
    }

    /**
     * 
     */
    public Email() {
        to = new ArrayList();
    }

    /**
     * @return
     */
    public String getBody() {
        return body;
    }

    /**
     * @return
     */
    public String getFrom() {
        return from;
    }

    /**
     * @return
     */
    public String getSmtp() {
        return smtp;
    }

    /**
     * @return
     */
    public List getTo() {
        return to;
    }

    /**
     * @param string
     */
    public void setBody(String string) {
        body = string;
    }

    /**
     * @param string
     */
    public void setFrom(String string) {
        from = string;
    }

    /**
     * @param string
     */
    public void setSmtp(String string) {
        smtp = string;
    }

    public void addRecipient(String email) {
        to.add(email);
    }

    /**
     * @return
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param string
     */
    public void setSubject(String string) {
        subject = string;
    }

    /**
     * @return Returns the attachments.
     */
    public File[] getAttachments() {
        return attachments;
    }

    /**
     * @param attachments
     *            The attachments to set.
     */
    public void setAttachments(File[] attachments) {
        this.attachments = attachments;
    }

    /**
     * @return Returns the smtp_pwd.
     */
    public String getSmtp_pwd() {
        return smtp_pwd;
    }

    /**
     * @param smtp_pwd
     *            The smtp_pwd to set.
     */
    public void setSmtp_pwd(String smtp_pwd) {
        this.smtp_pwd = smtp_pwd;
    }

    /**
     * @return Returns the smtp_usr.
     */
    public String getSmtp_usr() {
        return smtp_usr;
    }

    /**
     * @param smtp_usr
     *            The smtp_usr to set.
     */
    public void setSmtp_usr(String smtp_usr) {
        this.smtp_usr = smtp_usr;
    }

    /**
     * 
     * @author nacho
     * 
     */
    private class SMTPAuthenticator extends javax.mail.Authenticator {
        String usr;

        String pwd;

        public SMTPAuthenticator(String usr, String pwd) {
            this.usr = usr;
            this.pwd = pwd;
        }

        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(usr, pwd);
        }
    }

    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer()
                .append(
                        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">")
                .append("<html xmlns=\"http://www.w3.org/1999/xhtml\">")
                .append("<head>")
                .append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />")
                .append("<title>Konecta!</title>")
                .append("</head>")
                .append("")
                .append("<body>")
                .append("<div align=\"center\">")
                .append("<table width=\"691\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">")
                .append("<tr>")
                .append(
                        "<td><img src=\"http://movistar.mister-i.com/contactos/mms/KonectaMail/nkonecta01.gif\" width=\"691\" height=\"106\" /></td>")
                .append("</tr>")
                .append("<tr>")
                .append("<td><table width=\"691\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">")
                .append("<tr>")
                .append(
                        "<td width=\"92\"><img src=\"http://movistar.mister-i.com/contactos/mms/KonectaMail/nkonecta02.gif\" width=\"92\" height=\"217\" /></td>")
                .append(
                        "<td width=\"226\" bgcolor=\"#80c1e8\"><div align=\"left\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><strong><font color=\"#FFFFFF\" size=\"4\">Lucia</font><font size=\"4\"> te invita a la mayor comunidad movil en Espa&ntilde;a.</font><br />")
                .append("<br />")
                .append(
                        "<font size=\"3\">Te esperamos en <font color=\"#FFFFFF\">Konecta!</font></font></strong></font></div></td>")
                .append(
                        "<td width=\"164\"><img src=\"http://movistar.mister-i.com/contactos/mms/KonectaMail/nkonecta03_1.gif\" width=\"164\" height=\"217\" /></td>")
                .append(
                        "<td width=\"166\" valign=\"top\" bgcolor=\"#c7106d\"><div align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><strong><font color=\"#FFFFFF\" size=\"2\"><img src=\"http://movistar.mister-i.com/contactos/mms/KonectaMail/foto.jpg\" width=\"120\" height=\"150\" /><br />")
                .append(
                        "Lucia</font><font size=\"2\"> te invita a la mayor comunidad movil en Espa&ntilde;a.</font></strong></font></div></td>")
                .append(
                        "<td width=\"43\"><img src=\"http://movistar.mister-i.com/contactos/mms/KonectaMail/nkonecta04.gif\" width=\"43\" height=\"217\" /></td>")
                .append("</tr>")
                .append("</table></td>")
                .append("</tr>")
                .append("<tr>")
                .append("<td><table width=\"691\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">")
                .append("<tr>")
                .append(
                        "<td width=\"443\"><img src=\"http://movistar.mister-i.com/contactos/mms/KonectaMail/m_telefonica.jpg\" width=\"443\" height=\"189\" /></td>")
                .append(
                        "<td width=\"248\"><img src=\"http://movistar.mister-i.com/contactos/mms/KonectaMail/nkonecta05.gif\" width=\"248\" height=\"190\" /></td>")
                .append("</tr>")
                .append("</table></td>")
                .append("</tr>")
                .append("<tr>")
                .append(
                        "<td><img src=\"http://movistar.mister-i.com/contactos/mms/KonectaMail/nkonecta06.gif\" width=\"691\" height=\"159\" /></td>")
                .append("</tr>").append("</table>").append("</div>").append("</body>").append("</html>");

        String smtp = "smtp.europaactiveclub.com";
        String usr = "lbq587c";
        String pwd = "madrid123";
        String sbj = "Test";
        String rcp = "nacho@imaginaworks.com";
        String frm = "desarrollo@europaactiveclub.com";
        String txt = sb.toString();

        Email m = new Email();
        m.smtp = smtp;
        m.body = txt;
        m.from = frm;
        m.to = Arrays.asList(new String[] { rcp });
        m.subject = sbj;
        m.smtp_usr = usr;
        m.smtp_pwd = pwd;
        try {
            m.sendWithEmbeddedImages();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}