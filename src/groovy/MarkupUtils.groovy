/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */

class MarkupUtils {

  static String removeMarkup(String original) {
      def matcher = original =~ "</?\\w+((\\s+\\w+(\\s*=\\s*(?:\".*?\"|'.*?'|[^\'\">\\s]+))?)+\\s*|\\s*)/?>";
      def result = matcher.replaceAll("");
      return result;
  }

  static String formatCode(String original){
      def keywordsMatcher = original =~ "[abstract|boolean|break|byte|case|catch|char|class|const|continue|def|default|do|double|else|extends|false|final|finally|float|for|goto|if|implements|import|in|instanceof|int|interface|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|void|volatile|while]";
      StringBuilder sb = new StringBuilder(original);
      while(keywordsMatcher.find()){
          sb.replace(keywordsMatcher.start(),keywordsMatcher.end(),beautifyKeyword(keywordsMatcher.group()));
      }
      //TODO: cambiar color a las cadenas de texto.
  }

  private static String beautifyKeyword(String kw){
      return "<span style='font-weight:bold;color:#036;'>${kw}</span>"
  }

}