/* =============================================================================
		   Copyright 2008 ImaginaWorks Software Factory, S.L.

	   Licensed under the Apache License, Version 2.0 (the "License");
	   you may not use this file except in compliance with the License.
	   You may obtain a copy of the License at

	       http://www.apache.org/licenses/LICENSE-2.0

	   Unless required by applicable law or agreed to in writing, software
	   distributed under the License is distributed on an "AS IS" BASIS,
	   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	   See the License for the specific language governing permissions and
	   limitations under the License.
================================================================================= */


class Weather {
    private static navalcarnero;
    private static lastUpdateNavalcarnero = 0
    private static updateRate = 1000*60*60*6 //6h
    
    static String tempNavalcarnero(){
        //estacin de Navalcarnero
        def rss = loadNavalcarnero()
        def desc = "${rss.channel.item.description}"
        def p0 = desc.indexOf("Temperatura")
        def p1 = desc.indexOf("</li",p0)
        return desc.substring(p0,p1);
        return desc
    }
    
    private static loadNavalcarnero(){
        def t = System.currentTimeMillis() - lastUpdateNavalcarnero
        if (!navalcarnero || t > updateRate){
            println("Actualizando informacin metereolgica de Navalcarnero...")
            navalcarnero = new XmlSlurper().parse("http://meteoclimatic.com/feed/rss/ESMAD2800000028600A");
            lastUpdateNavalcarnero = System.currentTimeMillis()
        }
        return navalcarnero
    }
    
  static void main(args) {
    println tempNavalcarnero();
    println tempNavalcarnero();
  }

}